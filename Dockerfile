FROM python:3.12.1-alpine3.19

LABEL com.berzibot.author="berzi"

ENV PYTHONBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

COPY .env ./
COPY pyproject.toml ./
COPY src ./src
COPY migrate_db.py ./

RUN python -m pip install --upgrade pip && \
    apk update && \
    apk add --no-cache -u  \
    "gcc>11.2.1"  \
    "linux-headers>5.16.7" \
    "musl-dev>1.2.3" \
    "postgresql-bdr-client>9.4.14" \
    "zlib>1.2.12" \
    "zlib-dev>1.2.12"
