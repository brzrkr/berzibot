"""Modal definitions for the chat control cog."""

from typing import cast, override

from discord import (
    Interaction,
    InteractionResponse,
    Permissions,
    TextStyle,
)
from discord.ui import TextInput

from src.components.berzimodal import BerziModal


class CreateAnnouncement(BerziModal):
    """Modal to create an announcement."""

    title = "Announcement"

    @override
    def get_minimum_permissions(self) -> Permissions:
        return Permissions(manage_channels=True)

    content: TextInput = TextInput(
        label="Content",
        required=True,
        style=TextStyle.paragraph,
        max_length=4000,
    )

    @override
    async def on_submit(self, interaction: Interaction) -> None:
        resp = cast(InteractionResponse, interaction.response)

        await resp.send_message(self.content.value)
