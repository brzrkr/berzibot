"""Extension for commands and features pertaining to the text chat."""

import re
from collections import defaultdict
from datetime import UTC, datetime, timedelta
from typing import cast

import discord
from discord import (
    Interaction,
    InteractionResponse,
    Member,
    Message,
    NotFound,
    TextChannel,
)
from discord.ext.commands import CheckFailure, Cog, UserInputError, has_permissions
from lingua import Language, LanguageDetectorBuilder

from src.berzibot import Berzibot
from src.components.berzicommand import berzi_command, berzi_group
from src.components.context import BerziContext as Context
from src.components.localisation import fetch_line
from src.components.log import prepare_logger
from src.components.utils import BerzibotCog
from src.config import DEFAULT_DELETE_AFTER, DEFAULT_GUILDS_FOR_APP_COMMANDS

from .constants import (
    LANG_WARN_CHANNEL_LAST_WARNING,
    LANG_WARN_CHANNELS_TO_MONITOR,
    LANG_WARN_CHANNELS_WITH_LENIENT_RULES,
    LANG_WARN_CLEANUP_PATTERN,
    LANG_WARN_COOLDOWN,
    LANG_WARN_LENIENT_MESSAGE,
    LANG_WARN_LENIENT_PATTERN,
    LANG_WARN_REQUIRED_CONFIDENCE,
    LANG_WARN_TIME_TOLERANCE,
    LANG_WARN_TOLERANCE,
    LANG_WARN_WARNING_MESSAGE,
    MAX_EMBED_FIELD_SIZE,
    MAX_EMBED_TITLE_SIZE,
    MAX_EMBED_TOTAL_SIZE,
)
from .modals import CreateAnnouncement

logger = prepare_logger(__name__)


class ChatControl(BerzibotCog):
    """Hold all commands for controlling chat and channels."""

    def __init__(self, bot: Berzibot) -> None:
        super().__init__(bot)
        self.language_detector = (
            LanguageDetectorBuilder.from_languages(
                Language.ITALIAN,
                Language.ENGLISH,
            )
            .with_low_accuracy_mode()
            .with_preloaded_language_models()
            .build()
        )
        self.language_warning_active: defaultdict[int, bool] = defaultdict(lambda: True)
        """Whether language warnings are active for the given guild."""

    @has_permissions(manage_messages=True)
    @berzi_command(name="embed", meta={"mod_only": True})
    async def embed(self, ctx: Context, title: str, *, content: str) -> None:
        """Embed some text for better visibility."""
        embed = discord.Embed()

        quotes = "'\""
        content = (content or "").strip(quotes)
        title = (title or "").strip(quotes)

        if not (content and title):
            await ctx.send(fetch_line("user error"))
            return

        if (
            not (
                len(content) <= MAX_EMBED_FIELD_SIZE
                and len(title) <= MAX_EMBED_TITLE_SIZE
            )
            or len(embed) > MAX_EMBED_TOTAL_SIZE
        ):
            await ctx.send(fetch_line("too long"))
            return

        embed.add_field(name=title, value=content, inline=False)

        await ctx.message.delete()
        await ctx.send(embed=embed)

    @has_permissions(manage_channels=True)
    @berzi_command(name="announce", meta={"mod_only": True})
    async def announce(
        self,
        ctx: Context,
        content: str,
        *,
        roles: str | None = None,
    ) -> None:
        """Make an announcement in this channel, optionally pinging roles (as IDs or
        names, separated by comma).

        Wrap the content in quotes.
        This allows to ping roles that cannot normally be pinged.
        """
        if not ctx.guild:
            return

        to_mention: dict[discord.Role, bool] = {}
        """Store roles and whether they can usually be mentioned."""

        if roles:
            guild_roles = {role.name.lower(): role for role in ctx.guild.roles}
            for role_name_ in roles.split(","):
                role_name = role_name_.lower().strip()
                if role_name.isnumeric():
                    if role := ctx.guild.get_role(int(role_name)):
                        to_mention.update({role: role.mentionable})
                        await role.edit(mentionable=True)

                elif role_name in guild_roles:
                    role = guild_roles[role_name]
                    to_mention.update({role: role.mentionable})
                    await role.edit(mentionable=True)

        mention_string = " ".join(role.mention for role in to_mention)
        announcement = f"{content}\n{mention_string}"

        await ctx.message.delete()
        await ctx.send(announcement)

        # Restore original mentionability (if that's a word).
        for role, mentionable in to_mention.items():
            await role.edit(mentionable=mentionable)

        logger.info(
            "%s made an announcement in channel %s@%s pinging %s.",
            ctx.author,
            ctx.channel.id,
            ctx.guild.id,
            to_mention.keys(),
        )

    @berzi_group(name="channel", meta={"mod_only": True})
    async def channel(self, ctx: Context) -> None:
        """Commands for text channel administration."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "channel"))

    @has_permissions(manage_messages=True, read_message_history=True)
    @channel.berzi_command(name="flush", meta={"mod_only": True})
    async def flush(
        self,
        ctx: Context,
        channel: TextChannel,
        member: Member | None = None,
    ) -> None:
        """Remove all messages from a channel, optionally only by a specific user."""
        if not ctx.guild or not isinstance(ctx.author, Member):
            return

        if (
            member
            and not member.bot
            and member.guild_permissions <= ctx.author.guild_permissions
        ):
            raise CheckFailure

        def check(message: discord.Message) -> bool:
            return message.author == member

        async with ctx.typing():
            try:
                await channel.purge(check=check if member else lambda _: True)
            except NotFound:
                pass

        await ctx.send(fetch_line("successful"), delete_after=DEFAULT_DELETE_AFTER)

        for_member = f" for {member=}" if member else ""
        logger.info(
            "%s flushed channel %s on guild %s%s.",
            ctx.author,
            channel.name,
            ctx.guild.name,
            for_member,
        )

    @has_permissions(manage_messages=True, read_message_history=True)
    @channel.berzi_command(name="truncate", meta={"mod_only": True})
    async def truncate(
        self,
        ctx: Context,
        direction: str,
        start: discord.Message,
    ) -> None:
        """Truncate all messages `before` or `after` a certain message ID (included)."""
        direction = direction.lower()

        if direction not in ("before", "after"):
            msg = f"Invalid direction: {direction}"
            raise UserInputError(msg)

        def check(message: discord.Message) -> bool:
            return (
                message.created_at <= start.created_at
                if direction == "before"
                else message.created_at >= start.created_at
            )

        channel = ctx.channel
        if not isinstance(channel, TextChannel):
            return

        async with ctx.typing():
            await channel.purge(check=check)
        await ctx.send(fetch_line("successful"), delete_after=DEFAULT_DELETE_AFTER)

        logger.info(
            "%s truncated channel %s on guild %s %s %s.",
            ctx.author,
            channel.name,
            ctx.guild and ctx.guild.name,
            direction,
            start.created_at,
        )

    @has_permissions(manage_channels=True, manage_messages=True)
    @berzi_command(name="disablelangwarnings", meta={"mod_only": True})
    async def disablelangwarnings(self, ctx: Context) -> None:
        """Disable language warnings in this guild."""

        if not ctx.guild:
            return

        self.language_warning_active[ctx.guild.id] = False
        await ctx.send("Disabled language warnings.")

        logger.info(
            "%s disabled language warnings in guild %s.",
            ctx.author,
            ctx.guild.id,
        )

    @has_permissions(manage_channels=True, manage_messages=True)
    @berzi_command(name="enablelangwarnings", meta={"mod_only": True})
    async def enablelangwarnings(self, ctx: Context) -> None:
        """Enable language warnings in this guild."""

        if not ctx.guild:
            return

        self.language_warning_active[ctx.guild.id] = True
        await ctx.send("Enabled language warnings.")

        logger.info(
            "%s enabled language warnings in guild %s.",
            ctx.author,
            ctx.guild.id,
        )

    @has_permissions(manage_channels=True, manage_messages=True)
    @berzi_command(name="langwarningsactive", meta={"mod_only": True})
    async def langwarningsactive(self, ctx: Context) -> None:
        """Return whether language warnings are active in this guild."""

        if not ctx.guild:
            return

        await ctx.send(
            "Language warnings are currently "
            + ("enabled" if self.language_warning_active[ctx.guild.id] else "disabled")
            + ".",
        )

    @staticmethod
    def _clean_up_message(message: str, *, lenient: bool) -> str:
        # Remove emoji and mentions.
        clean_text = LANG_WARN_CLEANUP_PATTERN.sub("", message)

        if lenient:
            # Remove text between parentheses.
            clean_text = LANG_WARN_LENIENT_PATTERN.sub("", clean_text)

        # Only keep letters, hyphens, apostrophes.
        return re.sub(r"[^a-zA-Z'-]", "", clean_text).strip()

    @staticmethod
    async def _is_first_message_of_conversation(message: Message, now: datetime) -> bool:
        history = message.channel.history(limit=2)
        try:
            await anext(history)  # Skip the message being analysed.

            previous_msg = await anext(history)
        except StopAsyncIteration:
            return True

        return now - previous_msg.created_at >= LANG_WARN_TIME_TOLERANCE

    @Cog.listener()
    async def on_message(self, message: discord.Message) -> None:
        author = message.author
        channel = message.channel
        if not isinstance(author, Member) or author.bot or not channel.guild:
            return

        channel_coords = (channel.guild.id, channel.id)

        if not self.language_warning_active[channel_coords[0]]:
            logger.debug("Message not checked for language: lang warnings are disabled.")
            return

        now = datetime.now(tz=UTC)
        last_warning_for_channel = LANG_WARN_CHANNEL_LAST_WARNING.get(
            channel_coords,
            # If no last warning, inflate the default so that it's > cooldown.
            now - timedelta(days=1),
        )
        is_first_message_of_conversation = await self._is_first_message_of_conversation(
            message,
            now,
        )
        cooldown_is_in_effect = (now - last_warning_for_channel) < LANG_WARN_COOLDOWN
        if (
            channel_coords not in LANG_WARN_CHANNELS_TO_MONITOR
            or author.guild_permissions.manage_channels
            or cooldown_is_in_effect
        ):
            logger.debug(
                "Message not checked for language:\n"
                "channel_coords not in LANG_WARN_CHANNELS_TO_MONITOR: %s\n"
                "author.guild_permissions.manage_channels: %s\n"
                "cooldown_is_in_effect: %s",
                channel_coords not in LANG_WARN_CHANNELS_TO_MONITOR,
                author.guild_permissions.manage_channels,
                cooldown_is_in_effect,
            )
            return

        lenient = channel_coords in LANG_WARN_CHANNELS_WITH_LENIENT_RULES
        clean_text = self._clean_up_message(message.content, lenient=lenient)

        if not clean_text:
            logger.debug(
                "Message not checked for language: clean_text was empty.\n"
                "Message before cleanup: %s",
                message.content,
            )
            return

        confidence = self.language_detector.compute_language_confidence(
            clean_text,
            Language.ITALIAN,
        )

        if confidence >= LANG_WARN_REQUIRED_CONFIDENCE:
            logger.debug(
                "Message not checked for language: confidence low.\n"
                "%s% out of required %s%.",
                confidence,
                LANG_WARN_REQUIRED_CONFIDENCE,
            )
            return

        LANG_WARN_CHANNELS_TO_MONITOR[channel_coords] = [
            incident
            for incident in [*LANG_WARN_CHANNELS_TO_MONITOR[channel_coords], now]
            if now - incident <= LANG_WARN_TIME_TOLERANCE
        ]

        if (
            not is_first_message_of_conversation
            and len(LANG_WARN_CHANNELS_TO_MONITOR[channel_coords]) < LANG_WARN_TOLERANCE
        ):
            logger.debug(
                "Message not checked for language:\n"
                "is_first_message_of_conversation: %s"
                "warnings: %s out of tolerance %s",
                is_first_message_of_conversation,
                len(LANG_WARN_CHANNELS_TO_MONITOR[channel_coords]),
                LANG_WARN_TOLERANCE,
            )
            return

        warning = LANG_WARN_LENIENT_MESSAGE if lenient else LANG_WARN_WARNING_MESSAGE

        await message.reply(warning, mention_author=False)
        LANG_WARN_CHANNELS_TO_MONITOR[channel_coords].clear()
        LANG_WARN_CHANNEL_LAST_WARNING[channel_coords] = now

    @has_permissions(manage_channels=True)
    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(
        name="announce",
        description="Send a message as Berzibot. Does not support mentions.",
    )
    async def slash_announce(self, interaction: Interaction) -> None:
        resp = cast(InteractionResponse, interaction.response)

        pool = self.bot.pool
        if not pool:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        await CreateAnnouncement(pool=pool, interaction=interaction).send()
