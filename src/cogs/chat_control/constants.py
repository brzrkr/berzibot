"""Constants for the chat control cog."""

import re
from datetime import datetime, timedelta
from typing import Final

MAX_EMBED_FIELD_SIZE: Final[int] = 1024
MAX_EMBED_TITLE_SIZE: Final[int] = 256
MAX_EMBED_TOTAL_SIZE: Final[int] = 6000


LANG_WARN_TOLERANCE: int = 5
"""Amount of messages in disallowed languages after which a warning will be issued.

Currently only supports Italian as the obligatory language.
"""
LANG_WARN_TIME_TOLERANCE: timedelta = timedelta(minutes=5)
"""Warnings will only be issued if enough disallowed messages younger than this tolerance
are collected.
"""
LANG_WARN_REQUIRED_CONFIDENCE: float = 0.02
"""Any message below this level of confidence will be considered disallowed."""
LANG_WARN_CHANNELS_TO_MONITOR: dict[tuple[int, int], list[datetime]] = {
    (257599362836856844, 624615376080076800): [],  # Test channel
    (257599362836856844, 1200775634994081830): [],  # Test channel
    # (240436732305211392, 255048290872197120): [],
    # (240436732305211392, 585747194821345280): [],
}
"""Mapping of (guild_id, channel_id) to a list of datetimes where disallowed messages
happened.

When a disallowed message is received, the log is checked and messages older than the
LANG_WARN_TIME_TOLERANCE are removed.
If the remaining messages (including the new one) is more than LANG_WARN_TOLERANCE, a
warning is issued in the channel and the log is resetted.
"""

LANG_WARN_CHANNELS_WITH_LENIENT_RULES: set[tuple[int, int]] = {
    (257599362836856844, 1200775634994081830),  # Test channel
    (240436732305211392, 585747194821345280),
}
"""Set of languages with special lenient rules.

These channels disregard text that is written within (), [], {} or || ||.
"""
LANG_WARN_CLEANUP_PATTERN: re.Pattern = re.compile(r":[^:]*:|<[^>]*>", re.MULTILINE)
LANG_WARN_LENIENT_PATTERN: re.Pattern = re.compile(
    r"\([^)]*\)|\[[^]]*]|\{[^}]*}|\|\|[^|]*\|\|", re.MULTILINE
)

LANG_WARN_COOLDOWN: timedelta = timedelta(minutes=3)
LANG_WARN_CHANNEL_LAST_WARNING: dict[tuple[int, int], datetime] = {}

LANG_WARN_WARNING_MESSAGE: str = """
Hey. Friendly reminder that this channel is for Italian-only.
If you have questions in a different language, use the questions channel.
If you want to speak English, use a different channel where it's allowed.
If you need help keeping up with the conversation, I recommend using [Deepl](https://deepl.com).
""".strip()
LANG_WARN_LENIENT_MESSAGE: str = f"""
{LANG_WARN_WARNING_MESSAGE}
Remember that in this channel, you're allowed to use a little English `||under spoiler||`.
""".strip()
