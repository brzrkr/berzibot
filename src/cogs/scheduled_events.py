"""Features for recurring events.

How to manage events with Berzibot
---
Berzibot will automatically pick up Discord events whose description ends with the exact
text:
This event is powered by Berzibot.

You can prove that Berzibot picked up the event because it will add an ID between
parentheses to that line.

To disable Berzibot's features, remove the "powered by Berzibot" line from the description
of the event.
If you then add it again, Berzibot will find the previous settings for the event, if its
ID has not changed.

If you disable Berzibot management from a running event, the cleanup actions will be
executed immediately.

Once Berzibot picked up an event, a datatase entry for it (and its series, if it's a
repeating event) will be created.
Currently, all additional features must be accessed by editing the database directly.
"""

import asyncio
import re
from collections import defaultdict
from collections.abc import Awaitable, Callable, Iterator
from dataclasses import dataclass
from datetime import datetime
from typing import TYPE_CHECKING, Final, Literal, cast
from unittest.mock import MagicMock
from zoneinfo import ZoneInfo

from discord import (
    EventStatus,
    Forbidden,
    Guild,
    ScheduledEvent,
)
from discord.ext import tasks
from discord.ext.commands import Cog, is_owner

import src.cogs.scheduled_events_actions as actions
from src.berzibot import Berzibot
from src.components.berzicommand import berzi_command
from src.components.context import BerziContext
from src.components.log import prepare_logger
from src.components.utils import BerzibotCog

if TYPE_CHECKING:
    from asyncpg import Pool, Record

logger = prepare_logger(__name__)

EventAction = Callable[
    [Berzibot, Guild, ScheduledEvent, actions.EventActionParams],
    Awaitable[None],
]
"""Function callable by Berzibot when starting or ending an event."""
EventActionName = str


async def setup(bot: Berzibot) -> None:  # noqa: D103
    logger.info("Loading cog: %s.", __name__)
    await bot.add_cog(ScheduledEvents(bot))


RAW_EVENT_DESCRIPTION_TEMPLATE: Final[str] = "This event is powered by Berzibot."
EVENT_DESCRIPTION_TEMPLATE: Final[str] = RAW_EVENT_DESCRIPTION_TEMPLATE + " {}"
EVENT_DESCRIPTION_PATTERN: Final[re.Pattern] = re.compile(
    EVENT_DESCRIPTION_TEMPLATE.format(
        # language=regexp # noqa: ERA001
        r"\(([0-9]+)\)",
    ),
)
FINISHED_EVENT_STATUSES: Final[frozenset[EventStatus]] = frozenset(
    {
        EventStatus.ended,
        EventStatus.cancelled,
        EventStatus.completed,
    }
)

ACTION_CALL_PATTERN: Final[re.Pattern] = re.compile(
    r"^(?P<name>[a-zA-Z0-9_]+)\((?P<args>.*?)\)$",
    re.DOTALL,
)
ACTION_PARAMS_SEPARATOR: Final[str] = "|"
ACTIONS_SEPARATOR: Final[str] = "\n\n\n"

EVENT_END_ADVANCE: Final[int] = 3
"""Amount of minutes in advance to end events compared to their official end time.

Discord deletes events automatically as soon as their end time is reached, preventing
Berzibot from detecting them for cleanup. Ending them in advance allows Berzibot to
retain control over them while displaying an end time that is approximately accurate.

A lower number makes the end time appear more accurate to users, but runs the risk of
Berzibot missing a window for cleaning up.
A higher number makes cleanup less likely to fail, but makes the displayed end time of the
event less accurate.
"""
EVENT_START_ADVANCE: Final[int] = 5
"""Amount of minutes to add to the default start time of events to give Berzibot time to
schedule them and execute actions on them. Also acts as minimum duration for events.
"""


ONLY_MANUAL_EVENTS: Final[bool] = True
"""Whether the regular watch for events should be blocked.

This feature has been added because the system tends to misbehave, perhaps due to Discord
API inconsistencies. At the moment, only one event needs to be supported.
"""
FRIDAY = 4
SATURDAY = 5


@dataclass
class EventActionCollection:
    """Tuple of a supported event action and its reverse."""

    name: EventActionName
    start: EventAction
    end: EventAction


@dataclass
class EventActionCollectionWithArguments(EventActionCollection):
    """Tuple of a supported event action with supplied arguments."""

    args: actions.EventActionParams

    @property
    def signature(self) -> str:
        """Return a string with the action name and parameters."""
        return f"{self.name}({"\n".join([str(arg) for arg in self.args])})"


SUPPORTED_ACTIONS: Final[dict[EventActionName, EventActionCollection]] = {
    "send_message": EventActionCollection(
        name="send_message",
        start=actions.send_message_start,
        end=actions.noop,
    ),
    "close_text_channel": EventActionCollection(
        name="close_text_channel",
        start=actions.close_text_channel_start,
        end=actions.close_text_channel_end,
    ),
    "close_voice_channel": EventActionCollection(
        name="close_voice_channel",
        start=actions.close_voice_channel_start,
        end=actions.close_voice_channel_end,
    ),
}
"""Actions supported on event start and end.

In the db, actions are saved as strings of action names to execute when the event starts
and reverse when it ends, separated by **three** newlines (= two empty lines).
Arguments are separated by pipes (no quotes for text, white space is stripped).

Currently supported:
close_text_channel(CHANNEL_ID)
close_voice_channel(CHANNEL_ID)
send_message(CHANNEL_ID|CONTENT)
"""


class ScheduledEvents(BerzibotCog):
    """Hold all commands for controlling events and their effects."""

    def __init__(self, bot: Berzibot) -> None:
        super().__init__(bot)

    @tasks.loop(minutes=1.0)
    async def manual_italian_server_venerdita(self) -> None:
        """Manually start and end the venerdita event on the italian server."""
        if not self.bot.pool:
            return

        italian_guild = self.bot.get_guild(240436732305211392)
        if italian_guild is None:
            logger.critical("Italian guild not found.")
            return

        # Currently the discord event is only used to extract its name for logging.
        venerdita_event = cast(ScheduledEvent, MagicMock())
        venerdita_event.name = "Venerdita"

        now = datetime.now(tz=ZoneInfo("Europe/Rome"))
        if now.hour == 0:
            async with self.bot.pool.acquire() as conn:
                event_record = await conn.fetchrow(
                    """
SELECT server, actions, started
FROM scheduled_events
WHERE id = $1;
                    """,
                    2,
                )

            if not event_record:
                logger.info("No existing record found for venerdita with id 2.")
                return

            started: bool = event_record["started"]

            if now.weekday() == FRIDAY:
                if started:
                    logger.info("Venerdita already started.")
                    return

                await self.execute_event_start_actions(
                    event_id=2,
                    actions_string=event_record["actions"],
                    guild=italian_guild,
                    event=venerdita_event,
                    pool=self.bot.pool,
                )

            elif now.weekday() == SATURDAY:
                if not started:
                    logger.info("Venerdita already stopped.")
                    return

                await self.execute_event_end_actions(
                    event_id=2,
                    actions_string=event_record["actions"],
                    guild=italian_guild,
                    event=venerdita_event,
                    pool=self.bot.pool,
                )

    async def cog_unload(self) -> None:  # noqa: D102
        self.manual_italian_server_venerdita.cancel()

    @staticmethod
    def _parse_actions(
        actions_string: str,
        event_db_id: int | None = None,
    ) -> Iterator[EventActionCollectionWithArguments]:
        """Extract all valid event actions found in the given database string."""
        logger.debug(
            "Parsing actions string for event id %s: %s",
            event_db_id,
            actions_string,
        )
        action_names = (
            actions_string.strip().split(ACTIONS_SEPARATOR) if actions_string else []
        )
        logger.debug("Action names found: %s", action_names)
        for action_call in action_names:
            action_call_match = ACTION_CALL_PATTERN.match(action_call.strip())
            if action_call_match is None:
                if event_db_id:
                    logger.critical(
                        "Could not parse action for event %s: %s",
                        event_db_id,
                        action_call.strip(),
                    )
                continue

            action_name = action_call_match["name"]
            action_args = [
                arg.strip()
                for arg in action_call_match["args"]
                .strip()
                .split(ACTION_PARAMS_SEPARATOR)
            ]

            if action_name not in SUPPORTED_ACTIONS:
                if event_db_id:
                    logger.error(
                        "Unrecognised action %s on event with ID %s",
                        action_name,
                        event_db_id,
                    )
                continue

            action = SUPPORTED_ACTIONS[action_name]
            yield EventActionCollectionWithArguments(
                name=action.name,
                start=action.start,
                end=action.end,
                args=tuple(action_args),
            )

    @staticmethod
    def _build_event_description(description: str | None, event_db_id: int) -> str:
        """Add Berzibot's DB ID template to an event description."""
        description = "" if description is None else description.strip()
        description = description.removesuffix(RAW_EVENT_DESCRIPTION_TEMPLATE)
        description = EVENT_DESCRIPTION_PATTERN.sub("", description)
        description = f"{description}\n" if description else ""

        return description + EVENT_DESCRIPTION_TEMPLATE.format(f"({event_db_id})")

    async def _register_new_event_in_db(
        self,
        event: ScheduledEvent,
        *,
        pool: "Pool",
    ) -> tuple[ScheduledEvent, int]:
        """Register a new event entry in the database, update its description, and return
        it and its ID.
        """

        async with pool.acquire() as conn:
            event_id = await conn.fetchval(
                """
INSERT INTO scheduled_events (server) VALUES ($1) RETURNING id;
                """,
                event.guild_id,
            )

        new_description = self._build_event_description(event.description, event_id)
        edited_event = await event.edit(description=new_description)

        return edited_event, event_id

    def _extract_event_id(self, event: ScheduledEvent) -> int | None:
        """Return the event ID from its description if available, else None."""
        if not (m := re.search(EVENT_DESCRIPTION_PATTERN, event.description or "")):
            logger.debug("No event description pattern found for event %s", event)
            return None

        try:
            event_id = int(m.group(1))
        except ValueError:
            logger.exception(
                "Invalid event ID on event %s: %s.",
                event,
                m.group(1),
            )
            return None

        if event_id == 0:
            logger.error("Invalid event ID on event %s: 0", event)
            return None

        logger.info("Event %s has assigned ID %s.", event, event_id)
        return event_id

    @Cog.listener()
    async def on_ready(self) -> None:
        """Load existing events in connected guilds and check if any should be tracked.

        Events matching the raw description template are picked up and assigned an ID
        based on a new database entry or an existing one (if a matching one exists).

        Newly registered events and events already containing an ID are then checked: if
        they have already started and starting actions have not been executed yet, they
        are executed now.
        """

        if ONLY_MANUAL_EVENTS:
            self.manual_italian_server_venerdita.start()
            return

        while not self.bot.pool:
            logger.warning("Pool unexpectedly not ready. Waiting.")
            await asyncio.sleep(1)

        async with self.bot.pool.acquire() as conn:
            raw_fetch = await conn.fetch(
                """
SELECT id, server, actions, started
FROM scheduled_events
                """,
            )

            event_data: defaultdict[int, dict[int, Record]] = defaultdict(dict)
            for row in raw_fetch:
                event_data[row["server"]][row["id"]] = row

        logger.debug("Found event data: %s", event_data)

        for guild in self.bot.guilds:
            logger.info("Processing events for guild %s", guild)
            try:
                for event in guild.scheduled_events:
                    if event.description is None:
                        continue

                    if event.description.endswith(RAW_EVENT_DESCRIPTION_TEMPLATE):
                        logger.info(
                            "Event %s matches pattern but has no ID: registering it.",
                            event,
                        )
            except Forbidden:
                logger.warning(
                    "Could not fetch events from guild %s due to a permission error.",
                    guild,
                    exc_info=True,
                )
                continue

    @Cog.listener()
    async def on_scheduled_event_update(
        self,
        before: ScheduledEvent,
        after: ScheduledEvent,
    ) -> None:
        """Check if the edited event should be picked up by Berzibot.

        If the event started being managed by Berzibot, execute create setup.

        If the event status changed to ACTIVE, execute starting actions.
        If the event status changed away from ACTIVE, execute ending actions.
        """
        if ONLY_MANUAL_EVENTS:
            return

        while not self.bot.pool:
            logger.warning("Pool unexpectedly not ready. Waiting.")
            await asyncio.sleep(1)

        logger.debug("Detected edited event.")
        logger.debug("Before: %s %s", before, before.status)
        logger.debug("After: %s %s", after, after.status)

        before_event_id = self._extract_event_id(before)
        after_event_id = self._extract_event_id(after)

        # Check if the description was edited to enable Berzibot management.
        if (
            not before_event_id
            and after.description
            and after.description.endswith(RAW_EVENT_DESCRIPTION_TEMPLATE)
        ):
            logger.info(
                "Event %s matches pattern but has no ID: registering it.",
                after,
            )

            after, after_event_id = await self._register_new_event_in_db(
                after,
                pool=self.bot.pool,
            )

        if not before_event_id and not after_event_id:
            # Not a Berzibot event.
            logger.debug("Event %s is not a Berzibot event.", after)
            return

        if before.guild is None or after.guild is None:
            logger.warning("Event %s has no guild.", after)
            return

        if before_event_id and not after_event_id:
            # Berzibot management was disabled: execute end event actions (even if the
            #  event hasn't finished) to prevent missing cleanup later.
            logger.info(
                "Berzibot management for event %s was disabled. Running end actions.",
                before_event_id,
            )
            await self.execute_event_end_actions(
                event_id=before_event_id,
                guild=before.guild,
                event=before,
                actions_string=None,
                pool=self.bot.pool,
            )
            return

        # If the ID is present after but not before, or is present in both cases, Berzibot
        #  management for the event was already or just enabled: check if the event just
        #  finished or started.

        if before.status == after.status:
            # Nothing more to do.
            # Note that this will explicitly ignore the case where Berzibot management was
            #  just enabled and the event was already started, as executing start event
            #  actions could be unexpected at this time.
            logger.debug("Event %s has not changed status.", after)
            return

        if after_event_id and after.status == EventStatus.active:
            # Event just started.
            logger.info(
                "Event %s has just started. Running start actions.",
                after_event_id,
            )
            await self.execute_event_start_actions(
                event_id=after_event_id,
                guild=after.guild,
                event=after,
                actions_string=None,
                pool=self.bot.pool,
            )
            return

        if after_event_id and after.status in FINISHED_EVENT_STATUSES:
            # Event just ended.
            logger.info(
                "Event %s has just ended. Running end actions.",
                after_event_id,
            )
            await self.execute_event_end_actions(
                event_id=after_event_id,
                guild=after.guild,
                event=after,
                actions_string=None,
                pool=self.bot.pool,
            )

    @Cog.listener()
    async def on_scheduled_event_create(self, event: ScheduledEvent) -> None:  # noqa: PLR0911
        """Check if the newly created event should be or was picked up by Berzibot."""
        if ONLY_MANUAL_EVENTS:
            return

        while not self.bot.pool:
            logger.warning("Pool unexpectedly not ready. Waiting.")
            await asyncio.sleep(1)

        logger.debug("Detected created event: %s", event)

        guild = event.guild
        if not guild:
            return

        if not event.description:
            return

        event_id = self._extract_event_id(event)

        if event.description.endswith(RAW_EVENT_DESCRIPTION_TEMPLATE):
            logger.info(
                "Event %s matches pattern but has no ID: registering it.",
                event,
            )

            event, event_id = await self._register_new_event_in_db(
                event,
                pool=self.bot.pool,
            )

        if event_id is None:
            # Not a Berzibot event.
            logger.debug(
                "Event %s is not a Berzibot event: skipping.",
                event,
            )
            return

        if event.status is not EventStatus.active:
            # Nothing to do.
            logger.info("Newly registered event %s is not active.", event_id)
            return

        async with self.bot.pool.acquire() as conn:
            event_record = await conn.fetchrow(
                """
SELECT server, actions, started
FROM scheduled_events
WHERE id = $1;
                """,
                event_id,
            )

        if not event_record:
            logger.info(
                "No existing record found for newly registered event %s.",
                event_id,
            )
            return

        started = event_record["started"]

        if started:
            logger.info(
                "Newly registered event %s already ran starting actions.",
                event_id,
            )
            return

        logger.info(
            "Executing start event actions for event %s.",
            event,
        )

        await self.execute_event_start_actions(
            event_id=event_id,
            actions_string=event_record["actions"],
            guild=guild,
            event=event,
            pool=self.bot.pool,
        )

        logger.info(
            "Started event %s in guild %s.",
            event,
            guild,
        )

    @Cog.listener()
    async def on_scheduled_event_delete(self, event: ScheduledEvent) -> None:
        """Check if the deleted event was powered by Berzibot.

        If the event was managed by Berzibot and it had run any starting actions, run any
        ending actions.
        Finally, delete the database entry for the event.
        """
        if ONLY_MANUAL_EVENTS:
            return

        while not self.bot.pool:
            logger.warning("Pool unexpectedly not ready. Waiting.")
            await asyncio.sleep(1)

        logger.debug("Detected deleted event: %s", event)

        if not event.guild:
            return

        if not (event_id := self._extract_event_id(event)):
            logger.debug("Event was not a Berzibot event.")
            return

        if event.status not in FINISHED_EVENT_STATUSES:
            # Run end actions just in case.
            logger.info(
                "Deleted event %s wasn't finished. Running end actions before deleting.",
                event_id,
            )
            await self.execute_event_end_actions(
                event_id=event_id,
                guild=event.guild,
                event=event,
                actions_string=None,
                pool=self.bot.pool,
            )

        # Delete event from DB.
        async with self.bot.pool.acquire() as conn:
            await conn.execute("""DELETE FROM scheduled_events WHERE id=$1;""", event_id)

        logger.info("Deleted event %s from DB.", event_id)

    async def execute_event_start_actions(
        self,
        *,
        event_id: int,
        actions_string: str | None,
        guild: Guild,
        event: ScheduledEvent,
        pool: "Pool",
    ) -> None:
        """Execute the start part of the event's actions and register that its setup has
        run.
        """

        if actions_string is None:
            async with pool.acquire() as conn:
                actions_string = await conn.fetchval(
                    """
SELECT actions FROM scheduled_events WHERE id = $1;
                    """,
                    event_id,
                )

            if actions_string is None:
                logger.info(
                    "Did not execute event start actions for event %s: could not find "
                    "actions in DB.",
                    event_id,
                )
                return

        for action in self._parse_actions(actions_string, event_id):
            logger.info(
                "Executing start action %s%s for event %s in guild %s.",
                action.name,
                action.args if action.name != "send_message" else "(...)",
                event,
                guild,
            )
            # noinspection PyBroadException
            try:
                await action.start(
                    self.bot,
                    guild,
                    event,
                    action.args,
                )
            except Exception:
                logger.exception("Could not execute start action for event %s", event)
                await self.bot.notify_mods(
                    f'I could not execute the action "{action.name}" for event '
                    f'"{event.name}", please do it manually.\n'
                    f"```\n"
                    f"{action.signature}\n"
                    f"```"
                )

        async with pool.acquire() as conn:
            await conn.execute(
                """
UPDATE scheduled_events SET started = true WHERE id=$1;
                """,
                event_id,
            )

    async def execute_event_end_actions(
        self,
        *,
        event_id: int,
        actions_string: str | None,
        guild: Guild,
        event: ScheduledEvent,
        pool: "Pool",
    ) -> None:
        """Execute the end part of the event's actions and reset its setup flag."""
        if actions_string is None:
            async with pool.acquire() as conn:
                actions_string = await conn.fetchval(
                    """
SELECT actions FROM scheduled_events WHERE id = $1;
                    """,
                    event_id,
                )

            if actions_string is None:
                logger.error(
                    "Did not execute event end actions for event %s: could not find "
                    "actions in DB.",
                    event_id,
                )
                return

        found_actions = self._parse_actions(actions_string, event_id)
        logger.info("Found actions: %s", found_actions)

        for action in found_actions:
            logger.info(
                "Executing end action %s%s for event %s in guild %s.",
                action.name,
                action.args if action.name != "send_message" else "(...)",
                event,
                guild,
            )
            # noinspection PyBroadException
            try:
                await action.end(
                    self.bot,
                    guild,
                    event,
                    action.args,
                )
            except Exception:
                logger.exception("Could not execute end action for event %s", event)
                await self.bot.notify_mods(
                    f'I could not execute cleanup for the action "{action.name}" for '
                    f'event "{event.name}", please do it manually.\n'
                    f"```\n"
                    f"{action.signature}\n"
                    f"```"
                )

        async with pool.acquire() as conn:
            await conn.execute(
                """
UPDATE scheduled_events SET started = false WHERE id=$1;
                """,
                event_id,
            )

    @is_owner()
    @berzi_command(name="testaction", hidden=True)
    async def test_action(
        self,
        ctx: BerziContext,
        variant: Literal["start", "end"],
        *,
        actions_string: str,
    ) -> None:
        """Test an action string."""
        if not ctx.guild:
            return

        for action in self._parse_actions(actions_string):
            await ctx.send(f"Executing...\n```\n{action.signature}\n```")
            fake_event = MagicMock()

            if variant == "start":
                await action.start(self.bot, ctx.guild, fake_event, action.args)
            elif variant == "end":
                await action.end(self.bot, ctx.guild, fake_event, action.args)
