"""Extension for voicechat-related features and commands."""

import contextlib
from collections import defaultdict
from dataclasses import dataclass

import discord
from discord import VoiceChannel
from discord.ext.commands import Cog
from discord.utils import get

from src.berzibot import Berzibot
from src.components.berzicommand import berzi_command
from src.components.log import prepare_logger
from src.components.utils import BerzibotCog, BerziContext

logger = prepare_logger(__name__)


@dataclass
class TriggerChannel:
    """A voice channel which, when entered, creates a new temporary voice channel."""

    guild: int
    channel: discord.VoiceChannel | None
    category_id: int | None
    name: str
    child_name: str
    size: int

    def __hash__(self) -> int:
        return hash(str(self.guild) + str(self.category_id) + self.name)


async def setup(bot: Berzibot) -> None:  # noqa: D103
    logger.info("Loading cog: %s.", __name__)
    await bot.add_cog(VoicechatControl(bot))


class VoicechatControl(BerzibotCog):
    """Hold all commands for controlling voicechat and channels."""

    def __init__(self, bot: Berzibot) -> None:
        self.trigger_channels: dict[int, TriggerChannel] = {}
        """Mapping of trigger channel IDs to TriggerChannel objects."""

        self.parents_children: defaultdict[int, set[int]] = defaultdict(set)
        """Mapping of trigger channel IDs to temporary channel IDs derived from
        them.
        """

        self.children_parents: dict[int, TriggerChannel] = {}
        """Mapping of temporary channel IDs to parent trigger channel."""

        self.ready = False

        super().__init__(bot)

    @Cog.listener()
    async def on_ready(self) -> None:  # noqa: D102
        await self.collect_trigger_channels()

        self.ready = True

    @berzi_command("clean_temp_channels", meta={"mod_only": True})
    async def clean_up_channels(self, _: BerziContext) -> None:
        """Delete all temporary channels."""
        reason = "Cleaning up temporary channels."
        parents = set()
        for child_id, parent in self.children_parents.items():
            parents.add(parent)
            channel = self.bot.get_channel(child_id)
            if isinstance(channel, VoiceChannel):
                await channel.delete(reason=reason)

        for parent in parents:
            if isinstance(parent.channel, VoiceChannel):
                await parent.channel.delete(reason=reason)

        self.children_parents.clear()
        self.parents_children.clear()

    async def collect_trigger_channels(self) -> None:
        """Load all channels which create new temporary voice channels."""
        if not self.bot.pool:
            return

        async with self.bot.pool.acquire() as conn:
            channels = await conn.fetch(
                """
            SELECT
                guild_id,
                trigger_channel_name,
                new_channel_name,
                channel_size,
                category_id
            FROM voice_trigger_channels
                """,
            )

        for (
            guild_id,
            trigger_channel_name,
            new_channel_name,
            channel_size,
            category_id,
        ) in channels:
            trigger = TriggerChannel(
                guild=guild_id,
                category_id=category_id,
                name=trigger_channel_name.strip(),
                child_name=new_channel_name.strip(),
                size=channel_size,
                channel=None,
            )

            guild = self.bot.get_guild(trigger.guild)
            if not guild:
                logger.info("No guild found for channel %s", trigger.name)
                continue

            category = get(
                guild.categories,
                id=trigger.category_id,
            )
            if not category:
                logger.info("No category found for channel %s", trigger.name)

            final_name = f"+ {trigger.name}"

            # Search if channel already exists
            for channel in guild.voice_channels:
                if channel.name == final_name:
                    await channel.edit(
                        user_limit=1,
                        category=category,
                        reason="Updating trigger channel.",
                    )
                    trigger.channel = channel

                    logger.info("Edited channel %s", trigger.name)
                    break

            else:
                trigger.channel = await guild.create_voice_channel(
                    name=final_name,
                    category=category,
                    user_limit=1,
                )
                logger.info("Created channel %s", trigger.name)

            self.trigger_channels.update({trigger.channel.id: trigger})

    @Cog.listener()
    async def on_voice_state_update(  # noqa: D102
        self,
        member: discord.Member,
        before: discord.VoiceState,
        after: discord.VoiceState,
    ) -> None:
        if member.bot or not self.ready:
            return

        if (
            isinstance(entered := after.channel, VoiceChannel)
            and before.channel != entered
        ):
            await self.on_entered_channel(member, entered)

        if isinstance(exited := before.channel, VoiceChannel) and after.channel != exited:
            await self.on_exited_channel(member, exited)

    async def on_entered_channel(  # noqa: D102
        self,
        member: discord.Member,
        channel: discord.VoiceChannel,
    ) -> None:
        if channel.id in self.trigger_channels:
            await self.make_new_temporary_channel(member, channel)

    async def make_new_temporary_channel(
        self,
        member: discord.Member,
        channel: discord.VoiceChannel,
    ) -> None:
        """Create a temporary voicechat channel."""
        parent = self.trigger_channels[channel.id]
        if not parent.channel:
            return

        existing = self.parents_children[parent.channel.id]

        guild = channel.guild
        category = get(guild.categories, id=parent.category_id)
        new_channel = await guild.create_voice_channel(
            name=parent.child_name,
            category=category,
            user_limit=parent.size,
        )

        existing.add(new_channel.id)
        self.children_parents.update({new_channel.id: parent})

        await member.move_to(
            new_channel,
            reason=f"Created new temporary channel from {parent.name}",
        )

    async def on_exited_channel(  # noqa: D102
        self,
        _: discord.Member,
        channel: discord.VoiceChannel,
    ) -> None:
        if not (channel.id in self.children_parents and len(channel.members) == 0):
            return

        with contextlib.suppress(discord.errors.NotFound):
            await channel.delete(reason="Temporary channel was empty.")

        with contextlib.suppress(KeyError, AttributeError):
            parent = self.children_parents[channel.id]

            self.parents_children[parent.channel.id].remove(  # type: ignore[union-attr]
                channel.id,
            )

            del self.children_parents[channel.id]
