"""Format definitions for Thing of the Day posts."""

import string

import discord

from src.cogs.thing_of_the_day.types import TotdEntry
from src.components.localisation import fetch_line

PUNCTUATION = set(string.punctuation).difference(set("[]{}()<>"))
"""Punctuation characters except parentheses."""


class ThingFormatter:
    """Format thing of the day data as either a regular message or an embed."""

    __slots__: list[str] = []

    @staticmethod
    def format_as_text(*, entry: TotdEntry) -> str:
        """Format a message to publish an entry given its parts."""
        # Keep acronyms as-is, force title case otherwise.
        qualifier_string = entry.qualifier
        if not qualifier_string.isupper():
            qualifier_string = qualifier_string.title() or "Word"

        extra_string = entry.extra
        if extra_string and extra_string[-1] not in PUNCTUATION:
            extra_string += "."

        return (
            f"{qualifier_string} of the day: "
            f"**{entry.content}**"
            f"{', ' + extra_string if extra_string else '.'}\n\n"
            f"ℹ️ {fetch_line('totd footer')}"  # noqa: RUF001
        )

    @staticmethod
    def format_as_embed(
        *,
        entry: TotdEntry,
        guild: discord.Guild,
    ) -> discord.Embed:
        """Build an embed with the entry's content."""
        embed = discord.Embed()

        # Keep acronyms as-is, force title case otherwise.
        qualifier_string = entry.qualifier
        if not qualifier_string.isupper():
            qualifier_string = qualifier_string.title() or "Word"
        embed.title = f"{qualifier_string} of the day"

        if guild.icon:
            embed.set_thumbnail(url=guild.icon.url)

        extra_string = entry.extra
        if extra_string:
            if extra_string[-1] not in PUNCTUATION:
                extra_string += "."
        else:
            extra_string = "\N{ZERO WIDTH SPACE}"  # Circumvent empty field error.

        embed.add_field(
            name=f"> **{entry.content}**",
            value=f"{extra_string}",
            inline=False,
        )

        embed.set_footer(
            text=fetch_line("totd footer"),
            # Information icon from twitter's emoji.
            icon_url="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com"
            "/thumbs/120/twitter/233/information-source_2139.png",
        )

        return embed
