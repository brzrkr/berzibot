"""Extension for the Thing of the Day service."""

import asyncio
import re
from collections.abc import Callable, Mapping, Sequence
from datetime import datetime
from functools import reduce
from itertools import islice
from typing import Final, Literal, cast

import discord
from discord import Interaction, InteractionResponse, TextChannel, Webhook
from discord.app_commands import Choice, Namespace
from discord.ext import tasks
from discord.ext.commands import (
    Cog,
    UserInputError,
    clean_content,
    has_permissions,
    is_owner,
)

from src.berzibot import Berzibot
from src.components.berzicommand import berzi_group
from src.components.localisation import fetch_line
from src.components.log import prepare_logger
from src.components.utils import (
    BerzibotCog,
    BerziContext,
    current_timeofday,
    paginate_for_interaction_response,
    time_converter,
)
from src.config import (
    API_STRING_LIMIT,
    DEFAULT_GUILDS_FOR_APP_COMMANDS,
    ITEMS_LOAD_LIMIT,
    MINIMUM_AUTOCOMPLETE_LENGTH,
)

from .exceptions import (
    DuplicateEntryError,
    EntryDoesNotExistError,
    LanguageDoesNotExistError,
)
from .formatter import ThingFormatter
from .helpers import add_new_totd_entry, build_where_query, get_existing_totd_entry
from .modals import AddTotd, EditTotd
from .types import Language, TotdEntry

logger = prepare_logger(__name__)


_TOTD_PARAMS_DESCRIPTIONS: Final[dict[str, str]] = {
    "language": "The language to which the entry belongs.",
    "entry": "The TotD entry to edit. Case sensitive. Type more for autocomplete.",
}


class _TotdTextCommands(BerzibotCog):
    @berzi_group(name="totd", hidden=True, meta={"mod_only": True})
    async def thing_of_the_day(self, ctx: BerziContext) -> None:
        """Commands for the "thing of the day" service."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd"))

    @thing_of_the_day.berzi_command(name="remaining", meta={"mod_only": True})
    async def count_remaining(
        self,
        ctx: BerziContext,
        *,
        language: str = "",
    ) -> None:
        """Count entries that have not been published yet."""
        if not ctx.guild or not self.bot.pool:
            return

        if not language:
            await ctx.send("You didn't specify for which language.")
            return

        async with self.bot.pool.acquire() as conn:
            count = await conn.fetchval(
                """
SELECT COUNT(*) AS count
FROM totd_mentions m
JOIN totd_entries e ON e.id=m.entry_id
WHERE m.server=$2
AND m.entry_id=e.id
AND e.language_id=(
    SELECT id FROM totd_languages WHERE name=$1 LIMIT 1
)
                """,
                language.lower(),
                ctx.guild.id,
            )

        await ctx.send(f"There are {count} remaining entries.")

    @thing_of_the_day.berzi_command(name="sample", meta={"mod_only": True})
    async def sample(
        self,
        ctx: BerziContext,
        language: int | str | None = None,
        format_type: Literal["embed", "text"] = "embed",
    ) -> None:
        """Sample a random entry. `format_type` can be `embed` or `text`."""
        if not ctx.guild:
            logger.warning("No guild found on invoked totd sample command.")
            return

        if not self.bot.pool:
            logger.warning("Pool is not ready.")
            return

        if format_type not in ("text", "embed"):
            await ctx.send(
                "The `format_type` parameter must be either `embed` or `text`.",
            )
            return

        logger.debug(
            "About to query. Language: %s (%s)",
            language,
            type(language).__name__,
        )
        async with self.bot.pool.acquire() as conn:
            match language:
                case str():
                    entry = await conn.fetch(
                        """
SELECT
 qualifier,
 content,
 extra
FROM totd_entries
WHERE language_id=(
    SELECT id FROM totd_languages WHERE name=$1
)
ORDER BY random()
LIMIT 1;
                        """,
                        language,
                    )
                case int():
                    entry = await conn.fetch(
                        """
SELECT
 qualifier,
 content,
 extra
FROM totd_entries
WHERE language_id=$1
ORDER BY random()
LIMIT 1;
                        """,
                        language,
                    )
                case _:
                    entry = await conn.fetch(
                        """
SELECT
 qualifier,
 content,
 extra
FROM totd_entries
ORDER BY random()
LIMIT 1;
                        """,
                    )

        if not entry:
            await ctx.send(fetch_line("fail find", "any entry"))
            return

        for qualifier, content, extra in entry:
            guild = self.bot.get_guild(ctx.guild.id)
            if not guild:
                logger.error("Couldn't find guild %s.", ctx.guild.id)
                continue

            location = guild.get_channel(ctx.channel.id)
            if not location or not isinstance(location, TextChannel):
                logger.error(
                    "Couldn't find channel %s on guild %s.",
                    ctx.channel.id,
                    guild,
                )
                continue

            entry_obj = TotdEntry(qualifier=qualifier, content=content, extra=extra)
            if format_type == "text":
                await location.send(ThingFormatter.format_as_text(entry=entry_obj))
            else:
                await location.send(
                    embed=ThingFormatter.format_as_embed(entry=entry_obj, guild=guild),
                )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_command(name="setup", meta={"mod_only": True})
    async def setup(
        self,
        ctx: BerziContext,
        language: str,
        channel: TextChannel | str,
        *,
        send_at: str | None = None,
    ) -> None:
        """Set up a task for issuing things of the day in a channel.

        `language` can be the name for a new one to create.
        `channel` can be `here`.
        `send_at` is the time of day at which to send the next issue; if empty, uses the
        current time.
        """

        if not ctx.guild or not self.bot.pool:
            return

        language = await clean_content().convert(ctx, language)

        send_at_datetime = time_converter(send_at) if send_at else current_timeofday()

        channel = (
            ctx.channel
            if channel == "here" and isinstance(ctx.channel, TextChannel)
            else channel
        )
        if not isinstance(channel, TextChannel):
            raise UserInputError

        # Fetch the language
        language = cast(str, language).lower()
        language_id = await self.bot.pool.fetchval(
            "SELECT id FROM totd_languages WHERE name=$1;",
            language,
        )

        # If not exists: ask confirmation to create (if owner)
        new_language = False
        if not language_id:
            if not await self.bot.is_owner(ctx.author):
                await ctx.send(fetch_line("unknown", language.capitalize()))
                return

            message = fetch_line("unknown should", language.capitalize())
            async with ctx.ask_bool(message) as confirm:
                if not confirm:
                    await ctx.send(fetch_line("cancelled"))
                    return

            language_id = await self.bot.pool.fetchval(
                "INSERT INTO totd_languages (name) VALUES($1) RETURNING id;",
                language,
            )

            new_language = True

            await ctx.send(fetch_line("successful"))
            logger.info(
                "New language %s created by %s in channel %s@%s.",
                language,
                ctx.author,
                channel,
                ctx.guild,
            )

        # Check whether language is already set up in this channel.
        if not new_language:
            existing_tasks = await self.bot.pool.fetch(
                """
SELECT id, send_at, is_active
FROM totd_tasks
WHERE language_id=$1 AND channel=$2 AND server=$3;
                """,
                language_id,
                channel.id,
                ctx.guild.id,
            )

            if existing_tasks:
                for task in existing_tasks:
                    if task["send_at"] == send_at_datetime:
                        # If set up but inactive, prompt to activate.
                        if task["is_active"] is True:
                            await ctx.send(fetch_line("task active"))
                            return

                        message = fetch_line("task at for", send_at_datetime, language)
                        async with ctx.ask_bool(message) as confirm:
                            if not confirm:
                                await ctx.send(fetch_line("cancelled"))
                                return

                            await self.bot.pool.execute(
                                """
UPDATE totd_tasks SET is_active=true WHERE id=$1;
                                """,
                                task["id"],
                            )

                            await ctx.send(fetch_line("successful"))

                            logger.info(
                                "Task %s for language %s enabled "
                                "by %s in channel %s@%s.",
                                task["id"],
                                language,
                                ctx.author,
                                channel,
                                ctx.guild,
                            )

                        return

        # Set up task in this channel.
        await self.bot.pool.execute(
            """
INSERT INTO totd_tasks (language_id, send_at, channel, server) VALUES($1, $2, $3, $4);
            """,
            language_id,
            send_at_datetime,
            channel.id,
            ctx.guild.id,
        )

        await ctx.send(fetch_line("successful"))
        logger.info(
            "Task for language %s set up by %s in channel %s@%s for %s.",
            language,
            ctx.author,
            channel,
            ctx.guild,
            send_at_datetime,
        )

    async def _toggle_task_for_language(
        self,
        ctx: BerziContext,
        *,
        enable: bool,
        language_id: int,
        channel: TextChannel,
        send_at: datetime | None,
    ) -> None:
        if not self.bot.pool:
            return

        query_string, query_values = build_where_query(
            ctx,
            language_id=language_id,
            channel=channel,
            send_at=send_at,
        )
        query_values.append(enable)

        query = f"""
UPDATE totd_tasks
SET is_active=${len(query_values)}
WHERE {query_string}
        """

        logger.debug("About to run query %s", query)
        logger.debug("Parameters: %s", query_values)
        count = int(
            (
                await self.bot.pool.execute(
                    query,
                    *query_values,
                )
            )[-1],
        )

        mode = "Enabled" if enable else "Disabled"
        await ctx.send(
            f"{mode} {count} task{'s' if count != 1 else ''}!"
            f" {':D' if count > 0 else ':0'}",
        )

        if count:
            logger.info(
                "%s %s %s task%s with query string: %s and values: %s.",
                ctx.author,
                mode.lower(),
                count,
                "s" if count != 1 else "",
                query_string,
                query_values,
            )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_command(name="enable", meta={"mod_only": True})
    async def enable(
        self,
        ctx: BerziContext,
        language_id: int,
        send_at: str | None = None,
        *,
        channel: TextChannel | None = None,
    ) -> None:
        """Enable one or more tasks for the specified language, time, or channel."""
        if not channel:
            if ctx.channel and isinstance(ctx.channel, TextChannel):
                channel = ctx.channel
            else:
                return

        send_at_datetime = time_converter(send_at) if send_at is not None else None

        await self._toggle_task_for_language(
            ctx,
            enable=True,
            language_id=language_id,
            channel=channel,
            send_at=send_at_datetime,
        )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_command(name="disable", meta={"mod_only": True})
    async def disable(
        self,
        ctx: BerziContext,
        language_id: int,
        send_at: str | None = None,
        *,
        channel: TextChannel | None = None,
    ) -> None:
        """Disable one or more tasks for the specified language, time, or channel."""
        if not channel:
            if ctx.channel and isinstance(ctx.channel, TextChannel):
                channel = ctx.channel
            else:
                return

        send_at_datetime = time_converter(send_at) if send_at is not None else None

        await self._toggle_task_for_language(
            ctx,
            enable=False,
            language_id=language_id,
            channel=channel,
            send_at=send_at_datetime,
        )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_group(name="list", hidden=True)
    async def list_(self, ctx: BerziContext) -> None:
        """Commands to list existing elements such as languages or tasks."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd list"))

    @list_.berzi_command(name="languages", meta={"mod_only": True})
    async def list_languages(
        self,
        ctx: BerziContext,
        *,
        search: str | None = None,
    ) -> None:
        """List existing languages for Thing of the Day.

        Optionally `search` for a language name.
        """
        if not self.bot.pool:
            return

        async with ctx.typing():
            if search:
                search = search.lower()

                results = await self.bot.pool.fetch(
                    """
SELECT name FROM totd_languages
WHERE name ILIKE '%'|| $1 || '%';
                    """,
                    search,
                )
            else:
                results = await self.bot.pool.fetch("SELECT name FROM totd_languages;")

        if not results:
            extra = " matching" if search else ""
            await ctx.send(fetch_line("fail find language", extra))
            return

        result_string = "\n".join(result["name"] for result in results)

        await ctx.send(result_string)

    @staticmethod
    def _get_task_list_converters(
        ctx: BerziContext,
    ) -> dict[str, Callable[[object], str]]:
        def process_language(value: object) -> str:
            if not isinstance(value, str):
                return ""

            return value.capitalize()

        def process_time(value: object) -> str:
            if not isinstance(value, datetime):
                return "00:00"

            return value.strftime("%H:%M")

        def process_channel(value: object) -> str:
            if not isinstance(value, int) or not ctx.guild:
                return ""

            if not ((ch := ctx.guild.get_channel(value)) and isinstance(ch, TextChannel)):
                return ""

            return ch.name

        def process_active(value: object) -> str:
            return "Yes" if value else "No"

        def process_created(value: object) -> str:
            if not isinstance(value, datetime):
                return "0000-00-00 00:00 UTC"

            return value.strftime("%Y-%m-%d %H:%M %Z")

        return {
            "language": process_language,
            "time": process_time,
            "channel": process_channel,
            "active": process_active,
            "created": process_created,
        }

    @staticmethod
    def _render_task_list(
        headers: dict[str, str],
        records: Sequence[Mapping[str, str]],
    ) -> str:
        widths = {}
        for field, header in headers.items():
            widths[field] = max(
                reduce(
                    lambda x, y: max(x, y),
                    (len(r[field]) for r in records),
                ),
                len(header),
            )

        # Pad columns
        records = [
            {str(k): v.ljust(widths.get(k, 0)) for k, v in r.items()} for r in records
        ]

        padding_amount = 3
        padding = " " * padding_amount

        lines = [padding.join(record.values()) for record in records]
        output = "\n".join(lines)

        return f"```\n{output}```"

    @list_.berzi_command(name="tasks")
    async def list_tasks(
        self,
        ctx: BerziContext,
        channel: TextChannel | None = None,
        *,
        language: str | None = None,
    ) -> None:
        """List or `search` for "thing of the day" services."""
        if not ctx.guild or not self.bot.pool:
            return

        if not channel:
            if isinstance(ctx.channel, TextChannel):
                channel = ctx.channel
            else:
                return

        query_string, query_values = build_where_query(
            ctx,
            channel=channel,
            language_id=language,
        )

        async with ctx.typing():
            results = await self.bot.pool.fetch(
                f"""
SELECT
    l.name AS language,
    send_at AS time,
    server,
    channel,
    is_active AS active,
    t.created_at AS created
FROM totd_tasks t JOIN totd_languages l ON l.id=t.language_id
WHERE {query_string};
""",
                *query_values,
            )

        if not results:
            await ctx.send(fetch_line("no match"))
            return

        transformer = self._get_task_list_converters(ctx)

        # Determine column headers and ordering
        headers = {
            "language": "Language",
            "time": "Time (server)",
            "channel": "Channel",
            "active": "Active",
            "created": "Created",
        }

        records: list[dict[str, str]] = [headers]
        records.extend(
            [
                {
                    str(k): transformer.get(k, lambda x: str(x))(v)
                    for k, v in r.items()
                    if k in headers
                }
                for r in results
            ],
        )

        # Calculate max width of each column
        output = self._render_task_list(headers, records)

        await ctx.send(output)

    @has_permissions(manage_channels=True)
    @thing_of_the_day.berzi_group(name="add", hidden=True, meta={"mod_only": True})
    async def add(self, ctx: BerziContext) -> None:
        """Commands to add new elements such as languages or entries."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd add"))

    @is_owner()
    @add.berzi_command(name="language", hidden=True)
    async def add_language(self, ctx: BerziContext, *, language: str) -> None:
        """Add a language to which entries can be added."""
        if not self.bot.pool:
            return

        language = (await clean_content().convert(ctx, language)).lower()

        existing = await self.bot.pool.fetch(
            """
SELECT * FROM totd_languages WHERE name=$1;
            """,
            language,
        )

        if existing:
            await ctx.send(fetch_line("already exists", language.capitalize()))
            return

        await self.bot.pool.execute(
            """
INSERT INTO totd_languages(name) VALUES($1);
            """,
            language,
        )

        await ctx.send(fetch_line("successful"))

        logger.info("%s added language %s.", ctx.author, language)

    @add.berzi_command(name="entry", aliases=["entries"], meta={"mod_only": True})
    async def add_entry(
        self,
        ctx: BerziContext,
        language: str | None = None,
        *,
        input_string: str | None = None,
    ) -> None:
        """Add an entry to a language. Empty arguments, are prompted interactively."""
        pool = self.bot.pool
        if not pool:
            return

        if not language:
            try:
                async with ctx.ask_str(fetch_line("what language")) as answer:
                    language = answer
            except asyncio.exceptions.TimeoutError:
                await ctx.send(fetch_line("cancelled"))
                return

        if "|" in language:
            # User inputted entry before language.
            raise UserInputError

        if not input_string:
            message = (
                "And what do I add?\n"
                "*The format is `qualifier|content(|description)`, one per line.*"
            )
            try:
                async with ctx.ask_str(message) as answer:
                    input_string = answer
            except asyncio.exceptions.TimeoutError:
                await ctx.send(fetch_line("cancelled"))
                return

        input_string = await clean_content().convert(ctx, input_string)

        entries: list[TotdEntry] = []
        for line in input_string.split("\n"):
            if "|" not in line:
                await ctx.send(fetch_line("user error"))
                await ctx.send(fetch_line("totd add entry instructions"))
                return

            entry_input = line.split("|", maxsplit=2)
            entry = TotdEntry(
                qualifier=entry_input[0],
                content=entry_input[1],
                extra=entry_input[2] if len(entry_input) > 2 else "",  # noqa: PLR2004
            )
            if not entry.content:
                await ctx.send(fetch_line("user error"))
                await ctx.send(fetch_line("totd add entry instructions"))
                return

            entries.append(entry)

        dupes = successful = 0
        async with ctx.typing():
            for entry in entries:
                try:
                    await add_new_totd_entry(
                        db_pool=pool,
                        language=language,
                        entry=entry,
                        author_id=ctx.author.id,
                    )
                    successful += 1
                except LanguageDoesNotExistError:
                    await ctx.send(
                        fetch_line("fail find", language.capitalize()),
                    )
                    return
                except DuplicateEntryError:
                    dupes += 1
                    continue

        if len(entries) == 1 and successful:
            await ctx.send(fetch_line("successful"))
        else:
            dupe_sentence = (
                f" (I skipped {dupes} duplicate{'' if dupes == 1 else 's'})"
                if dupes
                else ""
            )
            await ctx.send(
                f"Added {successful if successful else 'no'} entries{dupe_sentence}.",
            )

        logger.info(
            "%s added %s to language %s.",
            ctx.author,
            successful,
            language,
        )

    @is_owner()
    @thing_of_the_day.berzi_group(name="remove", hidden=True)
    async def remove(self, ctx: BerziContext) -> None:
        """Commands to remove elements such as languages or entries."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd remove"))

    @remove.berzi_command(name="language", hidden=True)
    async def remove_language(
        self,
        ctx: BerziContext,
        *,
        language: str,
    ) -> None:
        """Remove a given language and all of its entries."""
        if not self.bot.pool:
            return

        language = (await clean_content().convert(ctx, language)).lower()

        language_id = await self.bot.pool.fetchval(
            """
SELECT id FROM totd_languages WHERE name=$1;
            """,
            language,
        )

        if not language_id:
            await ctx.send(fetch_line("unknown", language.capitalize()))
            return

        message = fetch_line("remove language", language.capitalize())
        async with ctx.ask_bool(message) as confirm:
            if not confirm:
                await ctx.send(fetch_line("cancelled"))
                return

        await self.bot.pool.execute(
            """
DELETE FROM totd_languages WHERE id=$1;
            """,
            language_id,
        )

        await ctx.send(fetch_line("successful"))

        logger.info("%s removed %s.", ctx.author, language)

    @remove.berzi_command(name="entry", hidden=True)
    async def remove_entry(
        self,
        ctx: BerziContext,
        *,
        entry_content: clean_content,
    ) -> None:
        """Remove an entry from its language."""
        if not self.bot.pool:
            return

        if not entry_content:
            await ctx.send(fetch_line("need entry name"))
            return

        async with ctx.typing():
            entries = await self.bot.pool.fetch(
                """
SELECT te.id, te.content, tl.name AS language FROM totd_entries te
JOIN totd_languages tl ON te.language_id=tl.id
WHERE content ILIKE '%' || $1 || '%';
                """,
                entry_content,
            )

        if not entries:
            await ctx.send(fetch_line("fail find", entry_content))
            return

        to_remove = []
        if len(entries) > 1:
            entry_list = "\n".join(
                f"{entry['id']} {entry['content']} ({entry['language']})"
                for entry in entries
            )
            message = f"I found all these entries:\n{entry_list}\nWhich do I remove?"

            try:
                async with ctx.ask_str(message) as answer:
                    if answer == "all":
                        to_remove = [entry["id"] for entry in entries]
                    else:
                        answer_parts = {s.lower().strip() for s in answer.split(",")}
                        to_remove.extend(
                            (
                                [
                                    e["id"]
                                    for e in filter(
                                        lambda e: (
                                            e["id"] in answer_parts
                                            or e["content"] in answer_parts
                                            or e["language"] in answer_parts
                                        ),
                                        entries,
                                    )
                                ]
                            ),
                        )

            except asyncio.exceptions.TimeoutError:
                await ctx.send(fetch_line("cancelled"))
                return
        else:
            to_remove = [entries[0]["id"]]

        async with ctx.ask_bool(fetch_line("sure")) as answer:
            if not answer:
                await ctx.send(fetch_line("cancelled"))
                return

        async with ctx.typing():
            await self.bot.pool.execute(
                """
DELETE FROM totd_entries WHERE id=ANY($1::INT[]);
                """,
                to_remove,
            )

        await ctx.send(fetch_line("successful"))
        logger.info(
            "%s removed entries %s.",
            ctx.author,
            to_remove,
        )

    @is_owner()
    @thing_of_the_day.berzi_group(name="edit", hidden=True)
    async def edit(self, ctx: BerziContext) -> None:
        """Commands to modify elements such as languages."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd edit"))

    @edit.berzi_command(name="language", hidden=True)
    async def edit_language(
        self,
        ctx: BerziContext,
        language: str,
        *,
        new_name: str,
    ) -> None:
        """Edit the name of a given language."""
        if not self.bot.pool:
            return

        language = language.lower()
        new_name = new_name.lower()

        await self.bot.pool.execute(
            """
UPDATE totd_languages SET name=$1 WHERE name=$2;
            """,
            new_name,
            language,
        )

        await ctx.send(fetch_line("successful"))
        logger.info(
            "%s changed the name of language %s to %s.",
            ctx.author,
            language,
            new_name,
        )

    @thing_of_the_day.berzi_group(name="count", hidden=False)
    async def count(self, ctx: BerziContext) -> None:
        """Commands to count elements such as languages or entries."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd count"))

    @count.berzi_command(name="languages")
    async def count_languages(self, ctx: BerziContext) -> None:
        """Count how many languages are configured for "thing of the day"."""
        if not self.bot.pool:
            return

        count = await self.bot.pool.fetchval(
            """
SELECT COUNT(*) FROM totd_languages;
            """
        )

        if count > 1:
            extra = " I'm pretty smart aren't I?"
        elif count < 1:
            extra = " I feel stoopid."
        else:
            extra = ""

        await ctx.send(f"I know {count} languages.{extra}")

    @count.berzi_command(name="entries")
    async def count_entries(
        self,
        ctx: BerziContext,
        *,
        languages: str | None = None,
    ) -> None:
        """Count how many entries there are for any configured language (separated by
        commas).
        """
        if not self.bot.pool:
            return

        lang_exists_doubt = ""
        language_string = "all languages"
        args = []
        languages_no = 0
        if languages is None:
            query = """
SELECT COUNT(*) FROM totd_entries;
            """
        else:
            parsed_languages = re.split(r",\s*", languages)

            if (languages_no := len(parsed_languages)) == 1:
                language_string = parsed_languages[0].capitalize()
            else:
                language_string = ", ".join(
                    lang.capitalize() for lang in parsed_languages[:-1]
                )
                language_string += f" and {parsed_languages[-1].capitalize()} together"

            query = f"""
SELECT COUNT(*)
FROM totd_entries
JOIN totd_languages l ON language_id=l.id
WHERE {" OR ".join(f"l.name=${idx}" for idx in range(1, languages_no + 1))}
            """
            args.extend(parsed_languages)

        count = await self.bot.pool.fetchval(query, *args)

        if count == 0:
            number = "it even exists" if languages_no == 1 else "if they even exist"
            lang_exists_doubt = f", if {number}"

        await ctx.send(f"{count} entries for {language_string}{lang_exists_doubt}.")


class ThingOfTheDay(_TotdTextCommands):
    """Hold all the commands for thing of the day services."""

    def __init__(self, bot: Berzibot) -> None:
        self.publish_totd.start()
        self.available_languages: list[Language] = []
        self.available_languages_by_name: dict[str, Language] = {}

        super().__init__(bot)

    @Cog.listener()
    async def on_ready(self) -> None:
        """Load existing languages."""

        while not self.bot.pool:
            logger.warning("Pool unexpectedly not ready. Waiting.")
            await asyncio.sleep(1)

        async with self.bot.pool.acquire() as conn:
            languages = await conn.fetch(
                """
SELECT id AS db_id, name from totd_languages;
                """
            )

        self.available_languages = [
            Language(db_id=int(language["db_id"]), name=language["name"])
            for language in languages
        ]
        self.available_languages_by_name = {
            language.name.lower(): language for language in self.available_languages
        }

    async def cog_unload(self) -> None:
        self.publish_totd.cancel()

    @tasks.loop(minutes=1.0)
    async def publish_totd(self) -> None:
        """Publish all Things of the Day scheduled for the current minute, if any."""
        if not self.bot.pool:
            return

        now = current_timeofday()

        async with self.bot.pool.acquire() as conn:
            entries = await conn.fetch(
                """
WITH entries AS (
    SELECT
        te.id,
        te.qualifier,
        te.content,
        te.extra,
        tt.server,
        tt.channel,
        tt.mode,
        tt.id AS task
    FROM totd_entries te
    JOIN totd_tasks tt ON te.language_id = tt.language_id
    WHERE tt.is_active=true
    AND EXTRACT(hour FROM tt.send_at at time zone 'utc')::int=$1
    AND EXTRACT(minute FROM tt.send_at at time zone 'utc')::int=$2
)
SELECT DISTINCT ON (entries.task)
    entries.id,
    entries.qualifier,
    entries.content,
    entries.extra,
    entries.server,
    entries.channel,
    entries.mode,
    (
        -- Prioritise entries that have never been mentioned, randomly select otherwise.
        SELECT CASE WHEN COUNT(*) = 0 THEN 0 ELSE 1 END
        FROM totd_mentions tm
        WHERE tm.entry_id = entries.id
    ) AS is_old
FROM entries
ORDER BY
    entries.task,
    is_old,
    RANDOM();
                """,
                now.hour,
                now.minute,
            )

        for entry in entries:
            entry_id = entry["id"]
            qualifier = entry["qualifier"]
            content = entry["content"]
            extra = entry["extra"]
            server_id = entry["server"]
            channel_id = entry["channel"]
            display_mode = entry["mode"]

            # Record mention
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    """
INSERT INTO totd_mentions
(entry_id, channel, server)
VALUES($1, $2, $3);
                    """,
                    entry_id,
                    channel_id,
                    server_id,
                )
                logger.info(
                    "Recorded mention of totd entry with id %s to channel %s@%s.",
                    entry_id,
                    channel_id,
                    server_id,
                )

            # Publish entry
            guild = self.bot.get_guild(server_id)
            if not guild:
                continue

            location = guild.get_channel(channel_id)
            if not location or not isinstance(location, TextChannel):
                continue

            entry_object = TotdEntry(qualifier=qualifier, content=content, extra=extra)
            if display_mode == "embed":
                embed = ThingFormatter.format_as_embed(entry=entry_object, guild=guild)
                embed.colour = self.bot.user.colour if self.bot.user else None
                await location.send(embed=embed)
            else:
                message = ThingFormatter.format_as_text(entry=entry_object)
                await location.send(message)

            logger.info(
                "Published totd entry with id %s to channel %s@%s.",
                entry_id,
                channel_id,
                server_id,
            )

    @publish_totd.before_loop
    async def check_ready(self) -> None:
        await self.bot.wait_until_ready()

    async def autocomplete_languages(
        self,
        _interaction: Interaction,
        current: str,
    ) -> list[Choice[str]]:
        return [
            Choice[str](name=language.name, value=language.name)
            for language in self.available_languages
            if language.name.lower().startswith(current.lower())
        ]

    async def autocomplete_totd_entries(
        self,
        interaction: Interaction,
        current: str,
    ) -> list[Choice[str]]:
        if not self.bot.pool:
            return []

        if not (MINIMUM_AUTOCOMPLETE_LENGTH <= len(current) <= API_STRING_LIMIT):
            return []

        namespace = cast(Namespace, interaction.namespace)
        language = namespace.language if isinstance(namespace.language, str) else None

        async with self.bot.pool.acquire() as conn:
            if language:
                fetched_entries = await conn.fetch(
                    """
SELECT content FROM totd_entries
JOIN totd_languages tl ON tl.id=language_id
-- Remove Discord markdown if any.
WHERE LOWER(tl.name) = LOWER($1) AND translate(content, '_*~', '') ILIKE $2 || '%'
LIMIT 25;
                    """,
                    language,
                    current,
                )
            else:
                fetched_entries = await conn.fetch(
                    """
SELECT content FROM totd_entries
-- Remove Discord markdown if any.
WHERE translate(content, '_*~', '') ILIKE $1 || '%'
LIMIT 25;
                    """,
                    current,
                )

        return [
            Choice[str](name=entry["content"], value=entry["content"])
            for entry in islice(fetched_entries, ITEMS_LOAD_LIMIT)
        ]

    @has_permissions(manage_channels=True)
    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(description="Add a new entry to the thing of the day.")
    @discord.app_commands.describe(language=_TOTD_PARAMS_DESCRIPTIONS["language"])
    @discord.app_commands.autocomplete(language=autocomplete_languages)
    async def add_totd_entry(self, interaction: Interaction, language: str) -> None:
        if not self.bot.pool:
            resp = cast(InteractionResponse, interaction.response)
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        await AddTotd(
            interaction=interaction,
            pool=self.bot.pool,
            entry_language=language,
        ).send()

    @has_permissions(manage_channels=True)
    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(description="Edit an existing thing of the day entry.")
    @discord.app_commands.describe(**_TOTD_PARAMS_DESCRIPTIONS)
    @discord.app_commands.autocomplete(
        language=autocomplete_languages,
        entry=autocomplete_totd_entries,
    )
    async def edit_totd_entry(
        self,
        interaction: Interaction,
        language: str,
        entry: str,
    ) -> None:
        resp = cast(InteractionResponse, interaction.response)
        if not self.bot.pool:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        try:
            existing_entry = await get_existing_totd_entry(
                db_pool=self.bot.pool,
                language=language,
                entry_content=entry,
            )
        except EntryDoesNotExistError:
            await resp.send_message(fetch_line("fail find", entry), ephemeral=True)
            return

        await EditTotd(
            existing_entry=existing_entry,
            entry_language=language,
            pool=self.bot.pool,
            interaction=interaction,
        ).send()

    @has_permissions(manage_channels=True)
    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(
        description="Privately find and display an existing thing of the day entry.",
    )
    @discord.app_commands.describe(**_TOTD_PARAMS_DESCRIPTIONS)
    @discord.app_commands.autocomplete(
        language=autocomplete_languages,
        entry=autocomplete_totd_entries,
    )
    async def show_totd_entry(
        self,
        interaction: Interaction,
        language: str,
        entry: str,
    ) -> None:
        resp = cast(InteractionResponse, interaction.response)
        if not self.bot.pool:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        try:
            existing_entry = await get_existing_totd_entry(
                db_pool=self.bot.pool,
                language=language,
                entry_content=entry,
            )
        except EntryDoesNotExistError:
            await resp.send_message(fetch_line("fail find", entry), ephemeral=True)
            return

        await resp.send_message(
            ThingFormatter.format_as_text(entry=existing_entry),
            ephemeral=True,
        )

    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(description="Show a list of all TotDs.")
    @discord.app_commands.autocomplete(language=autocomplete_languages)
    async def list_totds(self, interaction: Interaction, language: str) -> None:
        resp = cast(InteractionResponse, interaction.response)
        followup = cast(Webhook, interaction.followup)

        pool = self.bot.pool
        if not pool:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        async with pool.acquire() as conn:
            totds = await conn.fetch(
                """
SELECT content FROM totd_entries te
JOIN totd_languages tl on tl.id = te.language_id
WHERE LOWER(tl.name) = LOWER($1);
                """,
                language,
            )

        if not totds:
            await resp.send_message(
                fetch_line("fail find", "any Thing of the Day"),
                ephemeral=True,
            )
            return

        response_iter = paginate_for_interaction_response(
            (f"- {totd["content"]}" for totd in totds),
            header="# Available Things of the Day:\n",
        )

        await resp.send_message(next(response_iter), ephemeral=True)
        for part in response_iter:
            await followup.send(part, ephemeral=True)
