"""Exceptions for use in the thing of the day cog."""


class LanguageDoesNotExistError(ValueError):
    """The given language is not registered in TotD."""


class EntryDoesNotExistError(ValueError):
    """The given entry content is not registered in TotD."""


class DuplicateEntryError(ValueError):
    """A TotD entry is duplicated (based on its main word)."""
