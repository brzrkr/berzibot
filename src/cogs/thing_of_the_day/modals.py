"""Modal definitions for the thing of the day cog."""

from abc import ABC
from typing import cast, override

from asyncpg import Pool
from discord import Interaction, InteractionResponse, Permissions, TextStyle
from discord.ui import TextInput
from discord.utils import MISSING

from src.components.berzimodal import BerziModal
from src.components.localisation import fetch_line
from src.components.log import prepare_logger
from src.config import MODAL_TITLE_MAX_LENGTH

from .exceptions import (
    DuplicateEntryError,
    LanguageDoesNotExistError,
)
from .helpers import add_new_totd_entry, edit_existing_totd_entry
from .types import TotdEntry

logger = prepare_logger(__name__)


class _TotdModal(BerziModal, ABC):
    """Modal base with Thing of the Day fields and permissions pre-set.

    Length limits are safe values to prevent errors when displaying entries with embeds.
    """

    @override
    def get_minimum_permissions(self) -> Permissions:
        return Permissions(manage_channels=True)

    qualifier: TextInput = TextInput(
        label="Qualifier",
        placeholder="E.g. adjective",
        required=False,
        max_length=240,
    )
    extra: TextInput = TextInput(
        label="Explanation",
        placeholder="E.g. sleepy",
        required=False,
        style=TextStyle.paragraph,
        max_length=4000,
    )


class AddTotd(_TotdModal):
    """Modal to insert a new TotD entry."""

    title = "Add Thing of the Day"

    content: TextInput = TextInput(
        label="Content",
        placeholder="E.g. sonnolento",
        max_length=240,
    )

    @override
    def __init__(
        self,
        *,
        entry_language: str,
        pool: Pool,
        interaction: Interaction | None = None,
        title: str = MISSING,
        timeout: float | None = None,
        custom_id: str = MISSING,
    ) -> None:
        super().__init__(
            title=title,
            timeout=timeout,
            custom_id=custom_id,
            interaction=interaction,
            pool=pool,
        )

        self.qualifier.row = 0
        self.content.row = 1
        self.extra.row = 2

        self.entry_language = entry_language

    @override
    async def on_submit(self, interaction: Interaction) -> None:
        resp = cast(InteractionResponse, interaction.response)

        try:
            await add_new_totd_entry(
                db_pool=self.pool,
                language=self.entry_language,
                author_id=interaction.user.id,
                entry=TotdEntry(
                    qualifier=self.qualifier.value,
                    content=self.content.value,
                    extra=self.extra.value,
                ),
            )
        except LanguageDoesNotExistError:
            await resp.send_message(
                fetch_line("fail find", self.entry_language.capitalize()),
                ephemeral=True,
            )
            return
        except DuplicateEntryError:
            await resp.send_message(
                fetch_line("already exists", self.content.value),
                ephemeral=True,
            )
            return

        await resp.send_message(
            f"{interaction.user.mention} added a new Thing of the Day "
            f"{self.qualifier.value or 'entry'}: "
            f'"{self.content.value}" '
            f"to {self.entry_language.capitalize()}! ✅",
        )


class EditTotd(_TotdModal):
    """Modal to edit an existing TotD entry."""

    title = "Edit TotD entry"

    @override
    def __init__(
        self,
        *,
        existing_entry: TotdEntry,
        entry_language: str,
        pool: Pool,
        interaction: Interaction | None = None,
        title: str = MISSING,
        timeout: float | None = None,
        custom_id: str = MISSING,
    ) -> None:
        super().__init__(
            title=title,
            timeout=timeout,
            custom_id=custom_id,
            interaction=interaction,
            pool=pool,
        )

        # The title of the modal can only be 45 chars long max, so we only include the
        #  entry's name in it if we can afford it.
        if (
            len(full_title := self.title + f' "{existing_entry.content}"')
            <= MODAL_TITLE_MAX_LENGTH
        ):
            self.title = full_title

        self.qualifier.default = existing_entry.qualifier
        self.extra.default = existing_entry.extra

        self.current = existing_entry
        self.entry_language = entry_language

    @override
    async def on_submit(self, interaction: Interaction) -> None:
        resp = cast(InteractionResponse, interaction.response)

        new_entry = TotdEntry(
            qualifier=self.qualifier.value,
            content=self.current.content,
            extra=self.extra.value,
        )

        try:
            await edit_existing_totd_entry(
                db_pool=self.pool,
                language=self.entry_language,
                entry=new_entry,
            )
        except LanguageDoesNotExistError:
            await resp.send_message(
                fetch_line("fail find", self.entry_language.capitalize()),
                ephemeral=True,
            )
            return

        await resp.send_message(
            f"{interaction.user.mention} edited Thing of the Day "
            f'"{new_entry.content}" '
            f"in {self.entry_language.capitalize()}! ✅",
        )
