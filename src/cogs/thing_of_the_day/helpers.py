"""Helper functions to interact with TotD database tables."""

from asyncpg import Pool
from discord.ext.commands import UserInputError

from src.components.context import BerziContext

from .exceptions import (
    DuplicateEntryError,
    EntryDoesNotExistError,
    LanguageDoesNotExistError,
)
from .types import TotdEntry


async def _get_language_id(db_pool: Pool, language: str) -> int:
    language = language.lower()
    language_id: int = await db_pool.fetchval(
        "SELECT id FROM totd_languages WHERE name=$1;",
        language,
    )

    if not language_id:
        raise LanguageDoesNotExistError(language)

    return language_id


async def add_new_totd_entry(
    *,
    db_pool: Pool,
    language: str,
    entry: TotdEntry,
    author_id: int,
) -> None:
    """Add a new TotD entry to the database.

    :raise LanguageDoesNotExistError: If the language does not exist.
    :raise DuplicateEntryError: If the entry already exists based on its content.
    """

    language_id = await _get_language_id(db_pool, language)

    existing_entries = {
        str(r[0])
        for r in await db_pool.fetch(
            "SELECT content FROM totd_entries WHERE language_id=$1;",
            language_id,
        )
    }

    if entry.content in existing_entries:
        raise DuplicateEntryError(entry.content)

    async with db_pool.acquire() as conn:
        await conn.execute(
            """
INSERT INTO totd_entries (language_id, qualifier, content, extra, author)
VALUES($1, $2, $3, $4, $5);
            """,
            language_id,
            entry.qualifier,
            entry.content,
            entry.extra,
            author_id,
        )


async def edit_existing_totd_entry(
    *,
    db_pool: Pool,
    language: str,
    entry: TotdEntry,
) -> None:
    """Edit an existing TotD entry.

    :raise LanguageDoesNotExistError: If the language does not exist.
    """

    language_id = await _get_language_id(db_pool, language)

    async with db_pool.acquire() as conn:
        await conn.execute(
            """
UPDATE totd_entries
SET qualifier=$1, extra=$2
WHERE content=$3 AND language_id=$4;
            """,
            entry.qualifier,
            entry.extra,
            entry.content,
            language_id,
        )


async def get_existing_totd_entry(
    *,
    db_pool: Pool,
    language: str,
    entry_content: str,
) -> TotdEntry:
    """Get an existing TotD entry.

    :raise LanguageDoesNotExistError: If the language does not exist.
    :raise EntryDoesNotExistError: If the given entry does not exist.
    """

    language_id = await _get_language_id(db_pool, language)

    async with db_pool.acquire() as conn:
        entry_from_db = await conn.fetchrow(
            """
SELECT qualifier, content, extra FROM totd_entries
WHERE content=$1 AND language_id=$2;
            """,
            entry_content,
            language_id,
        )

        if entry_from_db is None:
            raise EntryDoesNotExistError(entry_content)

        return TotdEntry(
            qualifier=entry_from_db["qualifier"],
            content=entry_from_db["content"],
            extra=entry_from_db["extra"],
        )


def build_where_query(
    ctx: BerziContext,
    **kwargs: object,
) -> tuple[str, list[object]]:
    """Assemble a WHERE clause for a query with the provided columns and values.

    Context is used to add the server (guild) to the query.
    Argument names remain unchanged, thus they are case-sensitive.
    "channel" and other Discord entities with an id should be provided as instances
     of said entities, the id is automatically extracted.

    :return: a tuple of the query string and a list of the respective values.
    """

    if not ctx.guild:
        message = "No guild found."
        raise UserInputError(message)

    query_names = ["server"]
    query_values: list[object] = [ctx.guild.id]

    transformers = {
        "channel": lambda x: x.id,
        "author": lambda x: x.id,
    }

    for column, value in kwargs.items():
        if not value:
            continue

        query_names.append(column)
        transformer = transformers.get(column, lambda x: x)
        query_values.append(transformer(value))

    query_string = " AND ".join(f"{name}=${i}" for i, name in enumerate(query_names, 1))

    return query_string, query_values
