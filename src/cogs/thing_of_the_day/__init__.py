from src.berzibot import Berzibot
from src.cogs.thing_of_the_day.cog import ThingOfTheDay
from src.components.log import prepare_logger

logger = prepare_logger(__name__)


async def setup(bot: Berzibot) -> None:  # noqa: D103
    logger.info("Loading cog: %s.", __name__)
    await bot.add_cog(ThingOfTheDay(bot))
