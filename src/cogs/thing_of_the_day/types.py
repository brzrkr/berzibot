"""Type definitions and dataclasses for the TotD cog."""

from dataclasses import dataclass


@dataclass
class Language:
    """An available TotD language from the database."""

    db_id: int
    name: str


@dataclass
class TotdEntry:
    """A single TotD entry."""

    qualifier: str
    content: str
    extra: str
