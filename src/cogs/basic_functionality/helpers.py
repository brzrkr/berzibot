"""Helper functions for the basic functionality cog."""

from typing import TypeGuard

from .types import CogOperation


def is_valid_cog_operation(op_name: str) -> TypeGuard[CogOperation]:  # noqa: D103
    return op_name in {"load", "unload", "reload"}
