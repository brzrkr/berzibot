"""Cog for all generic, basic commands and features."""

from datetime import datetime, timezone
from inspect import Parameter
from pathlib import Path
from typing import Literal, cast

import discord
from discord import Emoji, Member
from discord.ext.commands import (
    Cog,
    ExtensionAlreadyLoaded,
    ExtensionFailed,
    ExtensionNotFound,
    ExtensionNotLoaded,
    NoEntryPointError,
    UserInputError,
    has_permissions,
    is_owner,
)

from src.components.berzicommand import BerziCommand, berzi_command, berzi_group
from src.components.context import BerziContext as Context
from src.components.localisation import fetch_line
from src.components.log import prepare_logger
from src.components.utils import (
    BerzibotCog,
    EmbedPaginator,
    from_snake_case,
    paginate_string,
    to_snake_case,
)
from src.config import DEFAULT_DELETE_AFTER

from .helpers import is_valid_cog_operation
from .types import CogOperation

logger = prepare_logger(__name__)


class BasicFunctionality(BerzibotCog, command_attrs={"hidden": True}):
    """Hold all basic functionality."""

    @Cog.listener()
    async def on_ready(self) -> None:
        logger.info("Connected.")

    @Cog.listener()
    async def on_disconnect(self) -> None:
        logger.info("Disconnected.")

    async def _make_help_fields(
        self,
        for_command: str,
        ctx: Context,
    ) -> set[tuple[str, str]]:
        no_help = fetch_line("no help")

        fields = set()
        for cmd in self.bot.walk_commands():
            cmd = cast(BerziCommand, cmd)

            if for_command and cmd.name != for_command:
                continue

            if not for_command and (
                cmd.hidden or isinstance(cmd, discord.ext.commands.Group)
            ):
                continue

            # Hide mod-only commands from users who don't have appropriate permissions.
            is_owner_ = await self.bot.is_owner(ctx.author)
            is_member = isinstance(ctx.author, Member)
            if not is_owner_:
                if not is_member:
                    continue

                permissions = cast(Member, ctx.author).guild_permissions
                if (
                    cmd.get_meta("mod_only")
                    and not permissions.manage_channels
                    or cmd.get_meta("admin_only")
                    and not permissions.administrator
                ):
                    continue

            try:
                params = cmd.clean_params
            except ValueError:
                logger.exception(
                    "Command %s is missing context param and won't be shown in list.",
                    cmd,
                )
                continue

            signature_bits = []
            for param in params.values():
                if param.default != Parameter.empty:
                    paren_open = "("
                    paren_closed = ")"
                else:
                    paren_open = ""
                    paren_closed = ""

                signature_bits.append(f"`{paren_open}{param.name}{paren_closed}`")

            signature = " ".join(signature_bits)

            name = f"{'🚫 ' if not cmd.enabled else ''}{cmd.qualified_name} {signature}"

            help_text = (cmd.help if for_command else cmd.short_doc) or no_help

            fields.add((name, help_text))

        return fields

    @berzi_command(name="help", hidden=False)
    async def help_(
        self,
        ctx: Context,
        *,
        for_command: str | None = None,
    ) -> None:
        """Display commands and their usage."""
        if for_command:
            for_command = for_command.lower().strip()

        embed = discord.Embed()
        embed.description = fetch_line("instructions")
        embed.title = for_command or "Help"
        if self.bot.user:
            if self.bot.user.avatar:
                embed.set_thumbnail(url=self.bot.user.avatar.url)
            embed.colour = self.bot.user.colour

        fields = await self._make_help_fields(for_command or "", ctx)
        if not fields and for_command:
            await ctx.send(fetch_line("fail find", for_command))
            return

        fields_list = sorted(fields, key=lambda tp: tp[0])

        thumbnail = (
            self.bot.user.avatar.url if self.bot.user and self.bot.user.avatar else None
        )

        paginator = EmbedPaginator(
            ctx,
            fields_list,
            thumbnail=thumbnail,
            title=f"Help for {for_command or 'my commands'}",
            description=(
                "*Ping me with a command!* Parameters in (parentheses) are optional."
            ),
            page_size=5,
        )
        await paginator.paginate()

    @berzi_command(name="ping", hidden=False)
    async def ping(self, ctx: Context) -> None:
        """Ping the bot to check response time."""
        delay = round(
            (datetime.now(tz=timezone.utc) - ctx.message.created_at).total_seconds()
            * 1000,
        )

        await ctx.send(f"Pong! 🏓 {delay}ms.")

    @berzi_command(name="release_name", hidden=False)
    async def release_name(self, ctx: Context) -> None:
        """Print the name of the current release name."""

        await ctx.send(self.bot.release)

    @is_owner()
    @berzi_command(name="die", hidden=True)
    async def die(self, ctx: Context) -> None:
        """Kill me. :(."""
        await ctx.send(fetch_line("logout"))
        await self.bot.close()

    @is_owner()
    @berzi_command(name="sleep", hidden=True)
    async def sleep(self, ctx: Context) -> None:
        """Go to sleep, ignoring all commands except the wake up call."""
        if not self.bot.sleeping:
            await self.bot.set_sleeping(value=True)
            await ctx.send(fetch_line("goodnight"))

    @is_owner()
    @berzi_command(name="wake", hidden=True)
    async def wake(self, ctx: Context, up: str = "") -> None:
        """Wake up after being put to sleep."""
        # Memeily force correct English, lul.
        if self.bot.sleeping and up.lower().strip() == "up":
            await self.bot.set_sleeping(value=False)
            await ctx.send(fetch_line("wake up"))

    @is_owner()
    @berzi_group(name="errors", aliases=["error"], hidden=True)
    async def errors(self, ctx: Context) -> None:
        """Commands for error reporting and handling."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "errors"))

    @is_owner()
    @errors.berzi_command(name="send", hidden=True)
    async def errors_send(self, ctx: Context) -> None:
        """Toggle sending errors."""
        currently_sending = self.bot.sending_errors
        self.bot.set_sending_errors(value=not currently_sending)
        await ctx.send(
            f"Ok,{'' if currently_sending else ' not'} sending errors from now on.",
            delete_after=DEFAULT_DELETE_AFTER,
        )

    @is_owner()
    @errors.berzi_command(name="logs", aliases=["log"], hidden=True)
    async def errors_log(self, ctx: Context) -> None:
        """Send the current logs."""
        try:
            with Path("../discord.log").open("r", encoding="utf-8") as fd:  # noqa: ASYNC230
                message = fd.read()
        except FileNotFoundError:
            message = "Couldn't find the file!"

        dm_channel = await ctx.author.create_dm()
        for part in paginate_string(
            message,
            2000,
            separator="\n",
            enforce_length=True,
        ):
            await dm_channel.send(part)

    @is_owner()
    @berzi_group(name="cog", hidden=True)
    async def cog(self, ctx: Context) -> None:
        """Commands for cog administration."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "cog"))

    def _list_cogs(self) -> tuple[set[str], set[str]]:
        """Return a set of already loaded cog names and one of available cogs."""
        already_loaded = {to_snake_case(cog_name) for cog_name in self.bot.loaded_cogs}

        available = {
            cog.name[:-3]
            for cog in Path("cogs/").glob("*.py")
            if not cog.name.startswith("_")
        }

        return already_loaded, available

    def _get_cogs_to_process(
        self,
        cog_names: str,
        operation: Literal["load", "unload", "reload"],
    ) -> set[str]:
        already_loaded, available = self._list_cogs()
        this_cog = {__name__.split(".")[-1]}

        if cog_names.lower() == "all":
            to_process = available
        else:
            to_process = {to_snake_case(name) for name in cog_names.split(",")}

        if operation == "load":
            to_process = to_process.difference(already_loaded)
        elif operation == "reload":
            to_process = to_process.intersection(already_loaded)
        elif operation == "unload":
            to_process = to_process.intersection(already_loaded).difference(this_cog)

        return to_process

    async def _manage_cogs(
        self,
        ctx: Context,
        cog_names: str,
        operation: CogOperation,
    ) -> None:
        operation = cast(CogOperation, operation.lower())
        if not is_valid_cog_operation(operation):
            message = f"Unknown operation: {operation}."
            raise UserInputError(message)

        to_process = self._get_cogs_to_process(cog_names, operation)

        if not to_process:
            await ctx.send(fetch_line("successful"))
            return

        processed = 0
        not_found = 0
        for cog_name in to_process:
            try:
                await getattr(self.bot, f"{operation}_extension")(f"cogs.{cog_name}")
                logger.info("%sed cog %s.", operation.capitalize(), cog_name)
                processed += 1
            except (ExtensionAlreadyLoaded, NoEntryPointError, ExtensionFailed):
                logger.exception("Failed to load cog: %s.", cog_name)
            except (ExtensionNotFound, ExtensionNotLoaded):
                not_found += 1
                logger.exception("Failed to find cog: %s.", cog_name)

        if processed:
            await ctx.send(fetch_line("successful"))
        else:
            await ctx.send(fetch_line("fail"))

        if not_found:
            await ctx.send(fetch_line("fail find", f"{not_found} of them"))

    @cog.berzi_command(name="load", hidden=True)
    async def load(self, ctx: Context, *, cog_names: str) -> None:
        """Load a cog."""
        await self._manage_cogs(ctx, cog_names, "load")

    @cog.berzi_command(name="unload", hidden=True)
    async def unload(self, ctx: Context, *, cog_names: str) -> None:
        """Unload a cog."""
        await self._manage_cogs(ctx, cog_names, "unload")

    @cog.berzi_command(name="reload", hidden=True)
    async def reload(self, ctx: Context, *, cog_names: str) -> None:
        """Reload a cog."""
        await self._manage_cogs(ctx, cog_names, "reload")

    @cog.berzi_command(name="list", hidden=True)
    async def list_(self, ctx: Context) -> None:
        """List all available and loaded cogs."""
        loaded, available = self._list_cogs()

        def format_cog(cog_name: str) -> str:
            icon = "🏳️" if cog_name in loaded else "🏴"
            cog_name = from_snake_case(cog_name)

            return f"{icon} {cog_name}"

        message = "Available cogs:\n"
        message += "\n".join(format_cog(cog) for cog in available)

        await ctx.send(message)

    @has_permissions(manage_emojis=True)
    @berzi_group(name="emoji")
    async def emoji(self, ctx: Context) -> None:
        """Commands for emoji management."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "emoji"))

    @emoji.berzi_command(name="sort")
    async def sort_emoji(self, ctx: Context, *, order: str) -> None:
        """Sort the server's emoji. Do `emoji sort help` for instructions."""
        order = order.strip()
        if not (guild := ctx.guild):
            logger.error("Could not find guild in context (%s).", ctx.message.content)
            return

        all_emoji = guild.emojis

        sort_commands = ("alphadesc", "alphaasc", "reverse")
        is_emoji_list = "[" in order and "]" in order
        is_sort_command = order.lower() in sort_commands
        if order.lower() == "help":
            message_parts = (
                "**Usage:** provide one of "
                + ", ".join(f"`{c}`" for c in sort_commands)
                + " or give me a list of emoji in the order you want.\n"
                "Here are all the emoji of the server; if you don't write all of them,"
                " I'm going to put all missing ones at the end. Do include the brackets."
                " Names are case-sensitive.",
                "```\n[" + ",".join(emoji.name for emoji in all_emoji) + "]\n```",
                "**Please backup your emoji before running this command.**\n"
                "If you use this on more than a handful of emoji, Discord's API limit"
                " will be reached, so it will take several hours to finish the job"
                " completely (even after I say I'm done). Some emoji may be missing"
                " during that time.",
            )
            for part in message_parts:
                await ctx.send(part)
            return

        emoji_list = []

        if is_sort_command:

            def emoji_sort_key(emoji: Emoji) -> str:
                return emoji.name

            emoji_list = list(all_emoji)
            order = order.lower()

            if order == "reverse":
                emoji_list.reverse()
            else:
                emoji_list.sort(
                    key=emoji_sort_key,
                    reverse=order.endswith("desc"),
                )

        if is_emoji_list:
            emoji_by_name = {emoji.name: emoji for emoji in all_emoji}

            names = [
                name
                for emoji_name in order.strip("[]").split(",")
                if (name := emoji_name.strip()) in emoji_by_name
            ]

            final_emoji = []
            for name in names:
                final_emoji.append(emoji_by_name[name])
                del emoji_by_name[name]

            emoji_list = final_emoji + list(emoji_by_name.values())

        await ctx.send(
            f"Ok. Let me collect the {len(emoji_list)} emoji.\n"
            f"This will take a bit. Don't touch the emoji until I'm finished.",
        )

        emoji_data = []
        for emoji in emoji_list:
            emoji_data.append((emoji.name, await emoji.read()))
            await emoji.delete(
                reason=f"Emoji sort command on Berzibot by {ctx.author}.",
            )

        await ctx.send("Uploading new order...")

        if not (guild := ctx.guild):
            await ctx.send(fetch_line("unsuccessful"))
            return

        for name, image in reversed(emoji_data):
            await guild.create_custom_emoji(name=name, image=image)

        await ctx.send(fetch_line("successful"))
