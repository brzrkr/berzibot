"""Types for the basic functionality cog."""

from typing import Literal

CogOperation = Literal["load", "unload", "reload"]
