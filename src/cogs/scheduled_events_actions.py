"""Action callables for scheduled events."""

from discord import (
    CategoryChannel,
    ForumChannel,
    Guild,
    Role,
    ScheduledEvent,
    StageChannel,
    TextChannel,
    Thread,
    VoiceChannel,
)

from src.berzibot import Berzibot
from src.components.log import prepare_logger

logger = prepare_logger(__name__)

EventActionParams = tuple[object, ...]


def _validate_args_length(args: EventActionParams, expected_length: int) -> bool:
    if len(args) != expected_length:
        logger.critical(
            "Action send_message_start called with wrong number of arguments "
            "(should be 2): %s",
            args,
        )
        return False

    return True


def _validate_channel(
    arg: object, guild: Guild
) -> Thread | VoiceChannel | StageChannel | TextChannel | None:
    try:
        channel = guild.get_channel_or_thread(int(str(arg)))
    except ValueError:
        logger.critical(
            "Action send_message_start called with wrong argument type (should be a "
            "valid int): %s",
            arg,
        )
        return None

    if channel is None:
        logger.critical(
            "Action send_message_start called with invalid argument (should be an "
            "existing channel or thread on guild %s): %s",
            guild,
            arg,
        )
    elif isinstance(channel, (CategoryChannel | ForumChannel)):
        return None

    return channel


async def noop(
    _bot: Berzibot,
    _guild: Guild,
    _event: ScheduledEvent,
    _args: EventActionParams,
) -> None:
    """Do nothing. Used for actions that require only a starting or ending action."""


async def send_message_start(
    _bot: Berzibot,
    guild: Guild,
    _event: ScheduledEvent,
    args: EventActionParams,
) -> None:
    """Send a message with the given content in the specified channel or thread.

    send_message(channel|content)
    """

    if not _validate_args_length(args, 2):
        return

    channel = _validate_channel(args[0], guild)
    if channel is None:
        return

    await channel.send(str(args[1]))


async def close_text_channel_start(
    bot: Berzibot,
    guild: Guild,
    event: ScheduledEvent,
    args: EventActionParams,
) -> None:
    """Remove permission to write inside the thread or text channel.

    close_text_channel(channel_id)
    """

    if not _validate_args_length(args, 1):
        return

    channel = _validate_channel(args[0], guild)
    if channel is None:
        return

    reason = f"Action from event {event.name}."

    if isinstance(channel, Thread):
        await channel.edit(locked=True, reason=reason)
        return

    for target, overwrites in channel.overwrites.items():
        # Skip permissions for individual people.
        if not isinstance(target, Role):
            logger.debug(
                "Skipped locking text channel %s for %s: not a role.",
                channel,
                target,
            )
            continue

        if target.is_bot_managed() or target.is_integration():
            logger.debug("Skipped role %s: is a bot or integration role.", target)
            continue

        # Skip roles for mods.
        if overwrites.manage_channels or overwrites.administrator:
            logger.debug(
                "Skipped locking text channel %s for %s: is mod role.",
                channel,
                target,
            )
            continue

        if not bot.pool:
            logger.critical(
                "Database not available while executing %s",
                reason.lower(),
            )
            return

        overwrite = channel.overwrites_for(target)

        # Save previous overrides so they can be restored when the channel is reopened.
        async with bot.pool.acquire() as pool:
            await pool.execute(
                """
INSERT INTO public.se_original_perms (
    server,
    role,
    send_messages,
    create_public_threads,
    create_private_threads,
    send_messages_in_threads,
    send_tts_messages,
    send_voice_messages
)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
ON CONFLICT (server, role) DO UPDATE
SET send_messages = EXCLUDED.send_messages;
                """,
                guild.id,
                target.id,
                overwrites.send_messages,
                overwrites.create_public_threads,
                overwrites.create_private_threads,
                overwrites.send_messages_in_threads,
                overwrites.send_tts_messages,
                overwrites.send_voice_messages,
            )

        overwrite.update(
            send_messages=False,
            create_public_threads=False,
            create_private_threads=False,
            send_messages_in_threads=False,
            send_tts_messages=False,
            send_voice_messages=False,
        )

        await channel.set_permissions(target, overwrite=overwrite, reason=reason)
        logger.info(
            "Locked channel %s for role %s as %s",
            channel,
            target,
            reason.lower(),
        )


async def close_text_channel_end(
    bot: Berzibot,
    guild: Guild,
    event: ScheduledEvent,
    args: EventActionParams,
) -> None:
    """Restore permission to write inside the thread or text channel."""
    if not _validate_args_length(args, 1):
        return

    channel = _validate_channel(args[0], guild)
    if channel is None:
        return

    reason = f"Action from event {event.name}."

    if isinstance(channel, Thread):
        await channel.edit(locked=False, reason=reason)
        return

    if not bot.pool:
        logger.critical(
            "Database not available while executing %s",
            reason.lower(),
        )
        return

    async with bot.pool.acquire() as pool:
        original_perms = await pool.fetch(
            """
SELECT
    role,
    send_messages,
    create_public_threads,
    create_private_threads,
    send_messages_in_threads,
    send_tts_messages,
    send_voice_messages
FROM se_original_perms
WHERE server = $1;
            """,
            guild.id,
        )
    perms_per_role: dict[int, dict[str, bool]] = {
        record["role"]: {
            "send_messages": record["send_messages"],
            "create_public_threads": record["create_public_threads"],
            "create_private_threads": record["create_private_threads"],
            "send_messages_in_threads": record["send_messages_in_threads"],
            "send_tts_messages": record["send_tts_messages"],
            "send_voice_messages": record["send_voice_messages"],
        }
        for record in original_perms
    }

    for target, overwrites in channel.overwrites.items():
        # Skip permissions for individual people.
        if not isinstance(target, Role):
            logger.debug(
                "Skipped unlocking text channel %s for %s: not a role.",
                channel,
                target,
            )
            continue

        if target.is_bot_managed() or target.is_integration():
            logger.info("Skipped role %s: is a bot or integration role.", target)
            continue

        # Skip roles for mods.
        if overwrites.manage_channels or overwrites.administrator:
            logger.info(
                "Skipped unlocking text channel %s for %s: is mod role.",
                channel,
                target,
            )
            continue

        overwrite = channel.overwrites_for(target)
        for perm_name in (
            "send_messages",
            "create_public_threads",
            "create_private_threads",
            "send_messages_in_threads",
            "send_tts_messages",
            "send_voice_messages",
        ):
            if not perms_per_role.get(target.id, {}).get(perm_name, True):
                logger.info(
                    (
                        "Skipped unlocking permission %s in channel %s for %s: did not "
                        "have permission originally."
                    ),
                    perm_name,
                    channel,
                    target,
                )
                continue

            logger.info(
                "Unlocking permission %s in channel %s for %s.",
                perm_name,
                channel,
                target,
            )
            overwrite.update(**{perm_name: True})

        await channel.set_permissions(target, overwrite=overwrite, reason=reason)
        logger.info(
            "Unlocked channel %s for role %s as %s",
            channel,
            target,
            reason.lower(),
        )


async def close_voice_channel_start(
    bot: Berzibot,
    guild: Guild,
    event: ScheduledEvent,
    args: EventActionParams,
) -> None:
    """Remove permission to connect to the voice channel.

    close_voice_channel(channel_id)
    """

    if not _validate_args_length(args, 1):
        return

    channel = _validate_channel(args[0], guild)
    if channel is None or not isinstance(channel, VoiceChannel):
        return

    reason = f"Action from event {event.name}."

    for target, overwrites in channel.overwrites.items():
        # Skip permissions for individual people.
        if not isinstance(target, Role):
            logger.debug(
                "Skipped locking vc %s for %s: not a role.",
                channel,
                target,
            )
            continue

        if target.is_bot_managed() or target.is_integration():
            logger.debug("Skipped role %s: is a bot or integration role.", target)
            continue

        # Skip roles for mods.
        if overwrites.manage_channels or overwrites.administrator:
            logger.debug(
                "Skipped locking vc %s for %s: is mod role.",
                channel,
                target,
            )
            continue

        # Skip if the role already doesn't have connect permissions.
        if not overwrites.connect:
            logger.debug(
                (
                    "Skipped locking vc %s for %s: already lacks permissions. "
                    "Saving to database."
                ),
                channel,
                target,
            )

            if not bot.pool:
                logger.critical(
                    "Database not available while executing %s",
                    reason.lower(),
                )
                return

            async with bot.pool.acquire() as pool:
                await pool.execute(
                    """
INSERT INTO public.se_original_perms (server, role, connect)
VALUES ($1, $2, $3)
ON CONFLICT (server, role) DO UPDATE
SET connect = EXCLUDED.connect;
                    """,
                    guild.id,
                    target.id,
                    False,  # noqa: FBT003
                )
            continue

        # Remove connect permissions.
        overwrite = channel.overwrites_for(target)
        overwrite.update(connect=False)

        await channel.set_permissions(target, overwrite=overwrite, reason=reason)
        logger.info(
            "Locked channel %s for role %s as %s",
            channel,
            target,
            reason.lower(),
        )


async def close_voice_channel_end(
    bot: Berzibot,
    guild: Guild,
    event: ScheduledEvent,
    args: EventActionParams,
) -> None:
    """Restore permission to connect to the voice channel."""
    if not _validate_args_length(args, 1):
        return

    channel = _validate_channel(args[0], guild)
    if channel is None or not isinstance(channel, VoiceChannel):
        return

    reason = f"Action from event {event.name}."

    if not bot.pool:
        logger.critical(
            "Database not available while executing %s",
            reason.lower(),
        )
        return

    async with bot.pool.acquire() as pool:
        original_perms = await pool.fetch(
            """
SELECT role, connect
FROM se_original_perms
WHERE server = $1;
            """,
            guild.id,
        )
    perms_per_role: dict[int, bool] = {
        record["role"]: record["connect"] for record in original_perms
    }

    for target, overwrites in channel.overwrites.items():
        # Skip permissions for individual people.
        if not isinstance(target, Role):
            logger.info("Skipped unlocking vc %s for %s: not a role.", channel, target)
            continue

        if target.is_bot_managed() or target.is_integration():
            logger.info("Skipped role %s: is a bot or integration role.", target)
            continue

        # Skip roles for mods.
        if overwrites.manage_channels or overwrites.administrator:
            logger.info("Skipped unlocking vc %s for %s: is mod role.", channel, target)
            continue

        # Skip if role didn't have permissions before locking the channel.
        if not perms_per_role.get(target.id, True):
            logger.info(
                "Skipped unlocking vc %s for %s: did not have permissions originally.",
                channel,
                target,
            )
            continue

        # Remove connect permissions.
        overwrite = channel.overwrites_for(target)
        overwrite.update(connect=True)

        await channel.set_permissions(target, overwrite=overwrite, reason=reason)
        logger.info(
            "Unlocked channel %s for role %s as %s",
            channel,
            target,
            reason.lower(),
        )
