"""Modals for the FAQ cog."""

from typing import cast, override

from asyncpg import Pool
from discord import Guild, Interaction, InteractionResponse, Permissions, TextStyle
from discord.ui import TextInput
from discord.utils import MISSING

from src.components.berzimodal import BerziModal
from src.config import MODAL_TITLE_MAX_LENGTH

from .helpers import add_new_faq, edit_existing_faq
from .types import FaqEntry


class CreateFaq(BerziModal):
    """Modal to create a new FAQ."""

    title = "Add new FAQ"

    @override
    def get_minimum_permissions(self) -> Permissions:
        return Permissions(manage_channels=True)

    name: TextInput = TextInput(
        label="Name",
        required=True,
        style=TextStyle.short,
        min_length=3,
        max_length=240,
    )
    content: TextInput = TextInput(
        label="Content",
        required=True,
        style=TextStyle.paragraph,
        max_length=4000,
    )

    @override
    def __init__(
        self,
        *,
        pool: Pool,
        interaction: Interaction | None = None,
        title: str = MISSING,
        timeout: float | None = None,
        custom_id: str = MISSING,
        guild: Guild,
        starting_content: str | None = None,
    ) -> None:
        super().__init__(
            title=title,
            timeout=timeout,
            custom_id=custom_id,
            interaction=interaction,
            pool=pool,
        )

        self.guild_id = guild.id

        if starting_content:
            self.content.default = starting_content

    @override
    async def on_submit(self, interaction: Interaction) -> None:
        resp = cast(InteractionResponse, interaction.response)

        await add_new_faq(
            db_pool=self.pool,
            guild_id=self.guild_id,
            name=self.name.value,
            content=self.content.value,
            author_id=interaction.user.id,
        )

        await resp.send_message(
            f"{interaction.user.mention} added a new FAQ: " f'"{self.name.value}".'
        )


class EditFaq(BerziModal):
    """Modal to edit an existing FAQ."""

    title = "Edit FAQ"

    @override
    def get_minimum_permissions(self) -> Permissions:
        return Permissions(manage_channels=True)

    content: TextInput = TextInput(
        label="Content",
        required=True,
        style=TextStyle.paragraph,
        min_length=10,
        max_length=4000,
    )

    @override
    def __init__(
        self,
        *,
        existing_entry: FaqEntry,
        pool: Pool,
        interaction: Interaction | None = None,
        title: str = MISSING,
        timeout: float | None = None,
        custom_id: str = MISSING,
    ) -> None:
        super().__init__(
            title=title,
            timeout=timeout,
            custom_id=custom_id,
            interaction=interaction,
            pool=pool,
        )

        # The title of the modal can only be 45 chars long max, so we only include the
        #  entry's name in it if we can afford it.
        if (
            len(full_title := self.title + f' "{existing_entry.name}"')
            <= MODAL_TITLE_MAX_LENGTH
        ):
            self.title = full_title

        self.content.default = existing_entry.content

        self.current = existing_entry

    @override
    async def on_submit(self, interaction: Interaction) -> None:
        resp = cast(InteractionResponse, interaction.response)

        new_entry = FaqEntry(
            content=self.current.content,
            db_id=self.current.db_id,
            name=self.current.name,
            author_id=self.current.author_id,
        )

        await edit_existing_faq(db_pool=self.pool, entry=new_entry)

        await resp.send_message(
            f"{interaction.user.mention} edited FAQ " f'"{new_entry.name}".'
        )
