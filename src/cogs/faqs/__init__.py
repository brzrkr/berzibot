from src.berzibot import Berzibot
from src.components.log import prepare_logger

from .cog import Faqs

logger = prepare_logger(__name__)


async def setup(bot: Berzibot) -> None:  # noqa: D103
    logger.info("Loading cog: %s.", __name__)
    await bot.add_cog(Faqs(bot))
