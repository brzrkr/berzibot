"""Types and enumerations for the FAQs cog."""

from dataclasses import dataclass
from enum import Enum, auto


@dataclass
class FaqEntry:
    """An FAQ."""

    db_id: int
    content: str
    name: str
    author_id: int


class FAQPermission(Enum):
    """Permission level for FAQ management."""

    NONE = auto()
    """No permissions. Not present in db-side enum. Used to signal removal of a role."""

    ALIASES = auto()
    """Can create FAQ names."""

    CREATE = auto()
    """Can create FAQs (and edit and delete own FAQs) in addition to previous
    permissions.
    """

    MODIFY = auto()
    """Can modify any FAQ in addition to previous permissions."""

    DELETE = auto()
    """Can delete any FAQ in addition to previous permissions."""

    PERMISSIONS = auto()
    """Can assign and modify role permissions in addition to previous permissions."""

    def __ge__(self, other: "FAQPermission") -> bool:
        return other.__class__ is self.__class__ and self.value >= other.value

    def __gt__(self, other: "FAQPermission") -> bool:
        return other.__class__ is self.__class__ and self.value > other.value

    def __le__(self, other: "FAQPermission") -> bool:
        return other.__class__ is self.__class__ and self.value <= other.value

    def __lt__(self, other: "FAQPermission") -> bool:
        return other.__class__ is self.__class__ and self.value < other.value

    def __eq__(self, other: object) -> bool:
        return isinstance(other, self.__class__) and self.value is other.value

    def to_db_name(self) -> str:
        """Get the name of the permission."""
        return self.name.lower()

    @classmethod
    def from_db_name(cls, name: str | None) -> "FAQPermission":
        """Get a FAQ permission from its name."""
        if name is None:
            return cls.NONE

        return cls[name.upper()]
