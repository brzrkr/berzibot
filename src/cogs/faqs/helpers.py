"""Helper functions for the FAQs cog."""

import re
from collections.abc import Callable, Coroutine

from asyncpg import Pool, UniqueViolationError
from discord import Member

from src.berzibot import Berzibot
from src.cogs.faqs.types import FAQPermission
from src.components.context import BerziContext
from src.components.localisation import fetch_line

from .types import FaqEntry

FAQ_NAMES_SEPARATOR = "|"
ALLOWED_FAQ_NAME_CHARS = re.compile(r"[0-9a-zA-Z-_() ',.!?:;\"=+]")


def split_faq_names(names: str) -> list[str]:
    """Split the names of multiple FAQs provided into a single string."""
    return list(
        filter(
            lambda txt: (
                txt and all(re.match(ALLOWED_FAQ_NAME_CHARS, char) for char in txt)
            ),
            (txt.strip() for txt in names.split(FAQ_NAMES_SEPARATOR)),
        ),
    )


async def verify_faq_perms(
    *,
    ctx: BerziContext,
    required_level: FAQPermission,
    bot: Berzibot,
) -> bool:
    """Verify whether the context author meets the required perm level, if not, warn."""
    if not ctx.guild or not bot.pool:
        return False

    if await bot.is_owner(ctx.author):
        return True

    if not isinstance(ctx.author, Member):
        return False

    user_roles = [role.id for role in ctx.author.roles]
    async with bot.pool.acquire() as conn:
        user_perms = await conn.fetchval(
            "SELECT permissions FROM faqs_creators"
            " WHERE server=$1 AND role=ANY($2::bigint[])"
            " ORDER BY permissions DESC"
            " LIMIT 1;",
            ctx.guild.id,
            user_roles,
        )

    if FAQPermission.from_db_name(user_perms) < required_level:
        await ctx.send(fetch_line("insufficient permissions"))
        return False

    return True


async def add_new_faq_names(
    *,
    pool: Pool,
    faq_id: int,
    guild_id: int,
    names: list[str],
) -> None:
    """Write the given new FAQ names to the database."""
    if not pool:
        return

    async with pool.acquire() as conn:
        try:
            await conn.executemany(
                "INSERT INTO faqs_names (faq_id, text, server) VALUES ($1, $2, $3);",
                [(faq_id, name, guild_id) for name in names],
            )
        finally:
            await conn.executemany(
                "DELETE FROM faqs_suggestions WHERE server=$2 AND name=$1;",
                [(name, guild_id) for name in names],
            )


def get_duplicate_faq_name_from_exception(exc: UniqueViolationError) -> str:
    """Get the name of the duplicated FAQ name from a UniqueViolationError exception."""
    dupe_name = getattr(exc, "detail", "").partition("=")[2].partition(",")[0][1:]

    return f'"{dupe_name}"'


async def get_faq(name: str, *, pool: Pool, guild_id: int) -> FaqEntry | None:
    """Fetch an FAQ and related data from the database for the given name and guild or
    return None if not found.
    """

    async with pool.acquire() as conn:
        entry = await conn.fetchrow(
            """
SELECT
e.id AS id,
e.content AS content,
n.faq_id AS faq_id,
n.text AS name,
e.created_by AS author
FROM faqs_entries e
JOIN faqs_names n ON n.faq_id = e.id
WHERE LOWER(n.text)=$1 AND e.is_draft IS false AND e.server=$2
LIMIT 1;
             """,
            name.lower(),
            guild_id,
        )

    if not entry:
        return None

    return FaqEntry(
        author_id=entry["author"],
        name=entry["name"],
        db_id=entry["id"],
        content=entry["content"],
    )


async def get_similar_faqs(name: str, *, pool: Pool, guild_id: int) -> list[str]:
    """Fetch FAQ names similar to the given one."""
    async with pool.acquire() as conn:
        similar = await conn.fetch(
            """
SELECT n.text AS name FROM faqs_names n
JOIN faqs_entries e ON n.faq_id = e.id
WHERE e.server=$1 AND n.text % $2 AND e.is_draft IS false
ORDER BY n.text % $2 DESC
LIMIT 3;
            """,
            guild_id,
            name,
        )

    return [faq["name"] for faq in similar]


async def send_similar_faqs_message(
    *,
    searched_name: str,
    similar_names: list[str],
    mechanism: Callable[[str], Coroutine[None, None, None]],
) -> None:
    """Based on the given list of similar FAQ names found (which may be empty), send a
    message advising the command caller using the given mechanism, which must be a
    function that takes a string and sends a message on Discord.
    """

    if not similar_names:
        message = fetch_line("fail find", f"a FAQ called `{searched_name}`")
    else:
        if len(similar_names) > 1:
            similar_options = ", ".join([f'"{n}"' for n in similar_names[:-1]])
            similar_options += f' or "{similar_names[-1]}"'
        else:
            similar_options = f'"{similar_names[0]}"'

        message = fetch_line("did you mean", similar_options)

    await mechanism(message)


async def get_faq_names(*, pool: Pool, guild_id: int) -> list[list[str]]:
    """Fetch all non-draft FAQs for the given guild and return a list of each's names."""
    async with pool.acquire() as conn:
        faqs = await conn.fetch(
            """
SELECT ARRAY_AGG(n.text) AS names
FROM faqs_entries e
JOIN faqs_names n ON e.id=n.faq_id
WHERE e.server=$1 AND e.is_draft IS false
GROUP BY e.id
ORDER BY names;
            """,
            guild_id,
        )

    return [list(faq["names"]) for faq in faqs]


async def edit_existing_faq(*, db_pool: Pool, entry: FaqEntry) -> None:
    """Edit an existing FAQ in the database."""
    async with db_pool.acquire() as conn:
        await conn.execute(
            """
UPDATE faqs_entries SET content=$2 WHERE id=$1;
            """,
            entry.db_id,
            entry.content,
        )


async def add_new_faq(
    *,
    db_pool: Pool,
    guild_id: int,
    name: str,
    content: str,
    author_id: int,
) -> None:
    """Add a new FAQ to the given guild."""
    async with db_pool.acquire() as conn:
        new_id: int = await conn.fetchval(
            """
INSERT INTO faqs_entries (server, content, created_by)
VALUES ($1, $2, $3)
RETURNING id;
            """,
            guild_id,
            content,
            author_id,
        )
        await conn.execute(
            """
INSERT INTO faqs_names (server, faq_id, text)
VALUES ($1, $2, $3);
            """,
            guild_id,
            new_id,
            name,
        )
