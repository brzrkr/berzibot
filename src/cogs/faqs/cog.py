"""Extension for FAQ features and commands."""

from typing import cast

import discord
from asyncpg import Record, UniqueViolationError
from discord import Interaction, InteractionResponse, Member, Message, Webhook
from discord.app_commands import Choice
from discord.ext.commands import MissingPermissions, has_permissions

from src.berzibot import Berzibot, BerziContext
from src.components.berzicommand import berzi_command, berzi_group
from src.components.localisation import EASTER_EGGS, fetch_easter_egg, fetch_line
from src.components.log import prepare_logger
from src.components.utils import (
    BerzibotCog,
    EmbedPaginator,
    paginate_for_interaction_response,
    paginate_string,
)
from src.config import (
    API_STRING_LIMIT,
    DEFAULT_GUILDS_FOR_APP_COMMANDS,
    MINIMUM_AUTOCOMPLETE_LENGTH,
)

from .helpers import (
    FAQ_NAMES_SEPARATOR,
    add_new_faq_names,
    get_duplicate_faq_name_from_exception,
    get_faq,
    get_faq_names,
    get_similar_faqs,
    send_similar_faqs_message,
    split_faq_names,
    verify_faq_perms,
)
from .modals import CreateFaq, EditFaq
from .types import FAQPermission

logger = prepare_logger(__name__)


async def add_as_faq(interaction: Interaction, message: Message) -> None:
    """Context menu to add a message as a FAQ.

    Needs to be registered in the bot's setup_hook.
    Must be defined outside of classes due to discordpy quirks.
    """

    resp = cast(InteractionResponse, interaction.response)
    bot = cast(Berzibot, interaction.client)

    pool = bot.pool
    guild = interaction.guild
    if not pool or not guild:
        await resp.send_message(fetch_line("not ready"), ephemeral=True)
        return

    member = interaction.user
    if not isinstance(member, Member) or not member.guild_permissions.manage_channels:
        await resp.send_message(fetch_line("insufficient permissions"), ephemeral=True)
        return

    await CreateFaq(
        pool=pool,
        interaction=interaction,
        guild=guild,
        starting_content=message.content,
    ).send()


class _FaqsTextCommands(BerzibotCog):
    async def _get_faq_with_perms_or_fail(
        self,
        ctx: BerziContext,
        faq_name: str,
        perms: FAQPermission,
    ) -> Record | None:
        """Get a specified FAQ by name if the caller has adequate permissions."""
        if not ctx.guild or not self.bot.pool:
            return None

        async with self.bot.pool.acquire() as conn:
            faq: Record | None = await conn.fetchrow(
                """
SELECT *
FROM faqs_entries e
JOIN faqs_names n ON n.faq_id=e.id
WHERE LOWER(n.text)=$1 AND e.server=$2
                """,
                faq_name.strip().lower(),
                ctx.guild.id,
            )

        if not faq:
            await self._get_similar_faqs(ctx, faq_name)
            return None

        if not (
            faq["created_by"] == ctx.author.id
            or await verify_faq_perms(ctx=ctx, required_level=perms, bot=self.bot)
        ):
            return None

        return faq

    async def _get_similar_faqs(
        self,
        ctx: BerziContext,
        faq_name: str,
        limit: int = 3,
    ) -> None:
        if not ctx.guild or not self.bot.pool:
            return

        async with self.bot.pool.acquire() as conn:
            similar = await conn.fetch(
                """
SELECT n.text AS name FROM faqs_names n
JOIN faqs_entries e ON n.faq_id = e.id
WHERE e.server=$1 AND n.text % $2
ORDER BY n.text % $2 DESC
LIMIT $3;
                """,
                ctx.guild.id,
                faq_name,
                limit,
            )

        if not similar:
            message = fetch_line("fail find", f"a FAQ called `{faq_name}`")
        else:
            if len(similar) > 1:
                similar_options = ", ".join([f'"{n["name"]}"' for n in similar[:-1]])
                similar_options += f' or "{similar[-1]["name"]}"'
            else:
                similar_options = f'"{similar[0]["name"]}"'

            message = fetch_line("did you mean", similar_options)

        await ctx.channel.send(message)

    @berzi_command(name="explain", aliases=["ex", "eli5", "about"])
    async def summon_faq(self, ctx: BerziContext, *, name: str) -> None:
        """Display the FAQ with the given name."""
        if not ctx.guild or not self.bot.pool:
            return

        name = name.lower()

        if name in EASTER_EGGS:
            await ctx.send(fetch_easter_egg(f"{name}"))
            return

        guild_id = ctx.guild.id

        async with self.bot.pool.acquire() as conn:
            entry = await conn.fetchrow(
                """
SELECT
    e.content AS content,
    n.faq_id AS faq_id,
    n.text AS name,
    e.created_by AS author
FROM faqs_entries e
JOIN faqs_names n ON n.faq_id = e.id
WHERE LOWER(n.text)=$1 AND e.is_draft IS false AND e.server=$2
LIMIT 1;
                 """,
                name,
                guild_id,
            )

        if not entry:
            await self._get_similar_faqs(ctx, name)
            await ctx.channel.send("I'll note this one down as a suggestion. :)")

            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    """
INSERT INTO faqs_suggestions (server, name)
VALUES ($1, $2)
ON CONFLICT ON CONSTRAINT faqs_suggestions_name_server DO
UPDATE SET times=faqs_suggestions.times+1;
                    """,
                    guild_id,
                    name,
                )
            return

        # Record summon for stats.
        async with self.bot.pool.acquire() as conn:
            await conn.execute(
                """
INSERT INTO faqs_summons (faq_id, user_id) VALUES ($1, $2);
                """,
                entry["faq_id"],
                ctx.author.id,
            )

        author = self.bot.get_user(entry["author"])

        # Split content into pages to avoid issues with embed limits
        pages = paginate_string(entry["content"], 1024, separator="\n")

        fields = [(None, p) for p in pages]

        footer = (
            (
                f"FAQ courtesy of {author.display_name}#{author.discriminator}. "
                f"Call `faq list` to see available FAQs.",
                None,
            )
            if author
            else None
        )

        paginator = EmbedPaginator(
            ctx,
            fields,
            page_size=2,  # Two fields per page, for a maximum of 2048 chars of content
            title=entry["name"],
            footer=footer,
        )
        await paginator.paginate()

    @berzi_group(name="faq", invoke_without_command=True)
    async def faq(self, ctx: BerziContext, *, entry: str | None = None) -> None:
        """Commands for FAQ management and shorthand to summon FAQs."""
        if ctx.invoked_subcommand is None:
            if entry is None:
                await ctx.send(fetch_line("invalid subcommand", "faq"))
                await ctx.send("To read a FAQ, do `faq [name]` or `faq read [name]`.")
            else:
                # noinspection PyTypeChecker
                await ctx.invoke(self.summon_faq, name=entry)

    @faq.berzi_command(name="read", aliases=["summon", "invoke", "show"])
    async def faq_read(self, ctx: BerziContext, *, name: str) -> None:
        """Alias for `explain [faq_name]`."""
        # noinspection PyTypeChecker
        await ctx.invoke(self.summon_faq, name=name)

    @faq.berzi_command(name="perms")
    async def perms(
        self,
        ctx: BerziContext,
        perm_level: str | None = None,
        *,
        role: discord.Role | None = None,
    ) -> None:
        """Show, add or modify permissions for a role.

        If `perm_level` is "none", remove all permissions.
        If neither argument is given, show the current permissions.
        """

        server = ctx.guild
        if not server or not self.bot.pool:
            return

        if not await verify_faq_perms(
            ctx=ctx, required_level=FAQPermission.PERMISSIONS, bot=self.bot
        ):
            raise MissingPermissions([FAQPermission.PERMISSIONS.to_db_name()])

        server_id = server.id

        if not perm_level and not role:
            async with self.bot.pool.acquire() as conn:
                current_perms = await conn.fetch(
                    """
SELECT role, permissions FROM faqs_creators WHERE server=$1;
                    """,
                    server_id,
                )

            paginator = EmbedPaginator(
                ctx,
                [
                    (role.name, perm["permissions"])
                    for perm in current_perms
                    if (role := server.get_role(perm["role"]))
                ],
                page_size=5,
                title=f"Permissions for {server.name}",
            )
            await paginator.paginate()

            return

        try:
            level = FAQPermission.from_db_name(perm_level)
        except KeyError:
            await ctx.send(f'There\'s no permission level called "{perm_level}".')
            return

        if not role:
            await ctx.send("No role specified.")
            return

        if level is FAQPermission.NONE:
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    """
DELETE FROM faqs_creators WHERE server=$1 AND role=$2;
                    """,
                    server_id,
                    role.id,
                )
        else:
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    """
INSERT INTO faqs_creators (server, role, permissions)
VALUES ($1, $2, $3)
ON CONFLICT (role) DO
UPDATE SET permissions=EXCLUDED.permissions;
                    """,
                    server_id,
                    role.id,
                    level.to_db_name(),
                )

        await ctx.send(fetch_line("successful"))

    @faq.berzi_command(name="info")
    async def info(self, ctx: BerziContext, *, faq_name: str) -> None:
        """Show information on a FAQ."""
        guild = ctx.guild
        if not guild or not self.bot.pool or not self.bot.user:
            return

        async with self.bot.pool.acquire() as conn:
            faq = await conn.fetchrow(
                """
WITH temp AS (
    SELECT faq_id FROM faqs_names
    WHERE LOWER(text)=$1 AND server=$2
)
SELECT
    e.created_at AS created,
    e.created_by AS author,
    ARRAY_AGG(n.text) AS names,
    (
        SELECT COUNT(*) FROM faqs_summons WHERE faq_id=temp.faq_id
    ) AS summons
FROM faqs_names n, temp
JOIN faqs_entries e ON e.id=temp.faq_id
WHERE n.faq_id=temp.faq_id AND e.server=$2
GROUP BY e.id, temp.faq_id;
                """,
                faq_name.lower(),
                guild.id,
            )

        if not faq:
            await self._get_similar_faqs(ctx, faq_name)
            return

        summons = faq["summons"]
        names = set()
        for name in faq["names"]:
            if name.lower() == faq_name.lower():
                faq_name = name
                continue

            names.add(name)

        output = (
            discord.Embed(
                colour=self.bot.user.colour,
                title=f"**{faq_name}**",
            )
            .add_field(
                name="Requested:",
                value=f"{summons} time{'' if summons == 1 else 's'}",
                inline=False,
            )
            .add_field(
                name="Written by:",
                value=guild.get_member(faq["author"]),
                inline=False,
            )
            .add_field(
                name="Written at:",
                value=faq["created"],
                inline=False,
            )
            .set_author(
                name=self.bot.user.display_name,
                icon_url=self.bot.user.avatar and self.bot.user.avatar.url,
            )
        )

        if names:
            output.description = f"a.k.a.: {', '.join(names)}"

        await ctx.send(embed=output)

    @faq.berzi_command(name="list", aliases=["all"])
    async def list_(self, ctx: BerziContext) -> None:
        """List all available FAQs."""
        if not ctx.guild or not self.bot.pool:
            return

        faqs = await get_faq_names(pool=self.bot.pool, guild_id=ctx.guild.id)

        if not faqs:
            await ctx.send(fetch_line("fail find", "any FAQ"))
            return

        paginator = EmbedPaginator(
            ctx,
            [
                (
                    f"**{faq[0]}**",
                    f"*a.k.a: {'/'.join(faq[1:])}*" if faq[1:] else None,
                )
                for faq in faqs
            ],
            page_size=10,
            title="Available FAQs",
            description="Read a FAQ by pinging me with `explain [name]`.",
        )
        await paginator.paginate()

    @faq.berzi_command(name="suggestions", aliases=["ideas", "requests"])
    async def suggestions(self, ctx: BerziContext) -> None:
        """Show suggestions for FAQs (or aliases) that have been requested."""
        if (
            not ctx.guild
            or not self.bot.pool
            or not await verify_faq_perms(
                ctx=ctx, required_level=FAQPermission.ALIASES, bot=self.bot
            )
        ):
            return

        def format_suggestion(sugg: Record) -> str:
            return (
                f"{sugg['name']}"
                f" (requested {sugg['times']} time{'' if sugg['times'] == 1 else 's'})"
            )

        limit_suggestions = 25

        async with self.bot.pool.acquire() as conn:
            suggestions = await conn.fetch(
                """
SELECT name, times FROM faqs_suggestions
WHERE server=$1
ORDER BY times DESC, random()
LIMIT $2;
                """,
                ctx.guild.id,
                limit_suggestions,
            )

        logger.debug("Suggestions found: %s", suggestions)

        if not suggestions:
            message = "There are no suggestions at the moment."
        elif len(suggestions) == 1:
            message = f"The only suggestion is {format_suggestion(suggestions[0])}."
        else:
            suggestions_string = "\n".join(
                [format_suggestion(suggestion) for suggestion in suggestions],
            )
            message = (
                f"Here are the {len(suggestions)} most frequent suggestions:\n"
                f"{suggestions_string}"
            )

        await ctx.send(message)

    @faq.berzi_group(name="new")
    async def new(self, ctx: BerziContext) -> None:
        """Commands to enter new FAQ material such as entries or alieases."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "new"))

    @new.berzi_command(name="name", aliases=["alias", "aliases", "names"])
    async def new_name(
        self,
        ctx: BerziContext,
        old_name: str,
        *,
        new_names: str,
    ) -> None:
        """Assign one or more new names to an existing FAQ. Separate names with |."""
        if not ctx.guild or not self.bot.pool:
            return

        old = await self._get_faq_with_perms_or_fail(
            ctx,
            old_name,
            FAQPermission.ALIASES,
        )

        if not old:
            return

        names = split_faq_names(new_names)

        if not names:
            await ctx.send(fetch_line("invalid entry"))
            return

        try:
            await add_new_faq_names(
                pool=self.bot.pool,
                faq_id=old["faq_id"],
                guild_id=ctx.guild.id,
                names=names,
            )
        except UniqueViolationError as e:
            await ctx.send(
                fetch_line("already exists", get_duplicate_faq_name_from_exception(e))
            )
            return

        await ctx.send(fetch_line("successful"))

    @new.berzi_command(name="faq", aliases=["entry"])
    async def new_faq(self, ctx: BerziContext, *, content: str) -> None:
        """Create a new FAQ."""
        if not ctx.guild or not self.bot.pool:
            return

        guild_id = ctx.guild.id

        if not await verify_faq_perms(
            ctx=ctx, required_level=FAQPermission.CREATE, bot=self.bot
        ):
            raise MissingPermissions([FAQPermission.CREATE.to_db_name()])

        if not content:
            await ctx.send("You're missing the content m8")
            return

        content = content.strip()

        # Require a name for the FAQ
        # noinspection PyArgumentList
        async with ctx.ask_str(
            f"Give me at least one name for this FAQ "
            f"(separate multiple names with `{FAQ_NAMES_SEPARATOR}`).",
            preserve_case=True,
        ) as raw_names:
            if not raw_names:
                await ctx.send(fetch_line("cancelled"))
                return

            names = split_faq_names(raw_names)

        if not names:
            await ctx.send(fetch_line("invalid entry"))
            return

        async with self.bot.pool.acquire() as conn:
            try:
                new_faq_id = await conn.fetchval(
                    """
INSERT INTO faqs_entries (server, created_by, content)
VALUES ($1, $2, $3)
RETURNING id;
                    """,
                    guild_id,
                    ctx.author.id,
                    content,
                )
            except UniqueViolationError:
                await ctx.send("I've got a FAQ with that exact content already.")
                return

            try:
                await add_new_faq_names(
                    pool=self.bot.pool, faq_id=new_faq_id, guild_id=guild_id, names=names
                )
            except UniqueViolationError as e:
                await ctx.send(
                    fetch_line(
                        "already exists", get_duplicate_faq_name_from_exception(e)
                    ),
                )

                # Delete orphaned faq
                await conn.execute(
                    """
DELETE FROM faqs_entries WHERE id=$1;
                    """,
                    new_faq_id,
                )
                return

        await ctx.send(fetch_line("successful"))

    @faq.berzi_group(name="edit", invoke_without_command=True)
    async def edit(self, ctx: BerziContext, *, entry: str | None = None) -> None:
        """Commands to edit FAQ material and shorthand to edit FAQ entries."""
        if ctx.invoked_subcommand is None:
            if entry is None:
                await ctx.send(fetch_line("invalid subcommand", "edit"))
                await ctx.send("To read a FAQ, do `faq [name]` or `faq read [name]`.")
            else:
                # noinspection PyTypeChecker
                await ctx.invoke(
                    self.edit_faq,
                    faq_name=entry,
                )

    @edit.berzi_command(name="faq", aliases=["entry"])
    async def edit_faq(self, ctx: BerziContext, *, faq_name: str) -> None:
        """Edit the content of an existing FAQ."""
        if not ctx.guild or not self.bot.pool:
            return

        faq = await self._get_faq_with_perms_or_fail(
            ctx,
            faq_name,
            FAQPermission.MODIFY,
        )
        if not faq:
            return

        # Send old content for convenience.
        # Segment content to fit messages.
        old_content = faq["content"]
        old_content_parts = []
        while old_content:
            old_content_parts.append(old_content[:1500])
            old_content = old_content[1500:]

        await ctx.send("Here's the old content if you need it:")
        for part in old_content_parts:
            await ctx.send(f"```markdown\n{part}```")

        done = False
        ask_msg = (
            f"Insert the new content (you have"
            f" {int(BerziContext.DEFAULT_ASK_TIMEOUT)} seconds)."
        )
        async with ctx.ask_str(ask_msg) as content:
            if content:
                async with ctx.ask_bool("Are you sure?") as answer:
                    if answer:
                        async with self.bot.pool.acquire() as conn:
                            await conn.execute(
                                """
UPDATE faqs_entries SET content=$2 WHERE id=$1;
                                """,
                                faq["faq_id"],
                                content,
                            )
                        done = True

        if done:
            await ctx.send(fetch_line("successful"))

    @faq.berzi_group(name="delete")
    async def delete(self, ctx: BerziContext) -> None:
        """Commands to remove FAQ material."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "delete"))
            await ctx.send(
                "If you want to delete a name or FAQ, tell me explicitly"
                " with `faq delete name [name]` or `faq delete entry [name]`",
            )

    @delete.berzi_command(name="faq", aliases=["entry", "content"])
    async def remove_content(self, ctx: BerziContext, *, faq_name: str) -> None:
        """Delete the content of the given FAQ and all its aliases."""
        if not self.bot.pool:
            return

        faq = await self._get_faq_with_perms_or_fail(
            ctx,
            faq_name,
            FAQPermission.DELETE,
        )
        if not faq:
            return

        ask_msg = (
            f"Are you sure you want to delete"
            f" the FAQ {faq_name} and all of its aliases?"
        )
        async with ctx.ask_bool(ask_msg) as confirm:
            if not confirm:
                await ctx.send(fetch_line("cancelled"))
                return

        async with self.bot.pool.acquire() as conn:
            await conn.execute(
                """
DELETE FROM faqs_entries WHERE id=$1;
                """,
                faq["faq_id"],
            )

        await ctx.send(fetch_line("successful"))

    @delete.berzi_command(name="name", aliases=["alias"])
    async def remove_name(self, ctx: BerziContext, *, name: str) -> None:
        """Remove an alias for a FAQ."""
        if not ctx.guild or not self.bot.pool:
            return

        name = name.strip()

        faq = await self._get_faq_with_perms_or_fail(ctx, name, FAQPermission.ALIASES)
        if not faq:
            return

        # Count remaining names for the given FAQ
        async with self.bot.pool.acquire() as conn:
            names_amount = await conn.fetchval(
                """
SELECT COUNT(*) FROM faqs_names WHERE faq_id=$1;
                """,
                faq["faq_id"],
            )

        if names_amount == 1:
            await ctx.send(
                "This is the only name of this FAQ."
                " If you want to change its name, make a new one first with"
                " `faq new name [old name] [new name]`.\n"
                "If you want to delete the whole FAQ, do it explicitly with"
                " `faq delete entry [name]`.",
            )
            return

        ask_msg = f"Are you sure you want to delete {name}?"
        async with ctx.ask_bool(ask_msg) as confirm:
            if not confirm:
                await ctx.send(fetch_line("cancelled"))
                return

        async with self.bot.pool.acquire() as conn:
            await conn.execute(
                """
DELETE FROM faqs_names WHERE lower(text)=$1 AND server=$2;
                """,
                name.lower(),
                ctx.guild.id,
            )

        await ctx.send(fetch_line("successful"))

    @delete.berzi_command(name="suggestions")
    async def flush_suggestions(self, ctx: BerziContext) -> None:
        """Forget all suggestions for FAQs so far."""
        if (
            not await verify_faq_perms(
                ctx=ctx, required_level=FAQPermission.DELETE, bot=self.bot
            )
            or not self.bot.pool
        ):
            return

        async with self.bot.pool.acquire() as conn:
            await conn.execute(
                """
DELETE FROM faqs_suggestions WHERE true;
                """,
            )

        await ctx.send(fetch_line("successful"))


class Faqs(_FaqsTextCommands):
    """Hold all commands for FAQs and explanations."""

    async def autocomplete_all_faq_names(
        self,
        interaction: Interaction,
        current: str,
    ) -> list[Choice[str]]:
        """Find autocomplete choices for FAQ names starting with the given input."""
        if (not self.bot.pool or not interaction.guild) or (
            not (MINIMUM_AUTOCOMPLETE_LENGTH <= len(current) <= API_STRING_LIMIT)
        ):
            return []

        async with self.bot.pool.acquire() as conn:
            found_names = await conn.fetch(
                """
SELECT text, server
FROM faqs_names
WHERE server=$1 AND text ILIKE $2 || '%';
                """,
                interaction.guild.id,
                current,
            )

        return [Choice(value=name["text"], name=name["text"]) for name in found_names]

    async def autocomplete_unique_faq_names(
        self,
        interaction: Interaction,
        current: str,
    ) -> list[Choice[str]]:
        """Find autocomplete choices for FAQs starting with the given input."""
        if (not self.bot.pool or not interaction.guild) or (
            not (MINIMUM_AUTOCOMPLETE_LENGTH <= len(current) <= API_STRING_LIMIT)
        ):
            return []

        async with self.bot.pool.acquire() as conn:
            found_names = await conn.fetch(
                """
SELECT DISTINCT ON (faq_id) text, server
FROM faqs_names
WHERE server=$1 AND text ILIKE $2 || '%';
                """,
                interaction.guild.id,
                current,
            )

        return [Choice(value=name["text"], name=name["text"]) for name in found_names]

    @has_permissions(manage_channels=True)
    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(description="Delete a FAQ name. Cannot be undone.")
    @discord.app_commands.describe(
        entry="The last remaining name for a FAQ must be deleted with /delete_faq.",
    )
    @discord.app_commands.autocomplete(entry=autocomplete_all_faq_names)
    async def delete_faq_name(
        self,
        interaction: Interaction,
        entry: str,
    ) -> None:
        resp = cast(InteractionResponse, interaction.response)

        pool = self.bot.pool
        guild = interaction.guild
        if not pool or not guild:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        entry = entry.lower()

        faq = await get_faq(entry, pool=pool, guild_id=guild.id)
        if not faq:
            similar_faqs = await get_similar_faqs(entry, pool=pool, guild_id=guild.id)
            await send_similar_faqs_message(
                searched_name=entry,
                similar_names=similar_faqs,
                mechanism=lambda m: resp.send_message(m, ephemeral=True),
            )
            return

        async with pool.acquire() as conn:
            remaining_names: int = await conn.fetchval(
                """
SELECT COUNT(*) FROM faqs_names WHERE faq_id=$1;
                """,
                faq.db_id,
            )

        if remaining_names == 1:
            await resp.send_message(
                "This is the last remaining name for this FAQ. Add a new name "
                "first or use `/delete_faq` to delete the FAQ entirely.",
                ephemeral=True,
            )
            return

        async with pool.acquire() as conn:
            await conn.execute(
                """
DELETE FROM faqs_names WHERE LOWER(text)=$1 AND server=$2;
                """,
                entry,
                guild.id,
            )

        await resp.send_message(fetch_line("successful"), ephemeral=True)

    @has_permissions(manage_channels=True)
    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(
        description="Delete a FAQ and all its names. Cannot be undone.",
    )
    @discord.app_commands.describe(
        entry="The name of the FAQ to delete. Not all aliases are autocompleted.",
    )
    @discord.app_commands.autocomplete(entry=autocomplete_unique_faq_names)
    async def delete_faq(
        self,
        interaction: Interaction,
        entry: str,
    ) -> None:
        resp = cast(InteractionResponse, interaction.response)

        pool = self.bot.pool
        guild = interaction.guild
        if not pool or not guild:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        entry = entry.lower()

        faq = await get_faq(entry, pool=pool, guild_id=guild.id)
        if not faq:
            similar_faqs = await get_similar_faqs(entry, pool=pool, guild_id=guild.id)
            await send_similar_faqs_message(
                searched_name=entry,
                similar_names=similar_faqs,
                mechanism=lambda m: resp.send_message(m, ephemeral=True),
            )
            return

        async with pool.acquire() as conn:
            await conn.execute(
                """
DELETE FROM faqs_entries WHERE id=$1 AND server=$2;
                """,
                faq.db_id,
                guild.id,
            )

        await resp.send_message(fetch_line("successful"), ephemeral=True)

    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(description="Show a list of all FAQs.")
    async def list_faqs(self, interaction: Interaction) -> None:
        resp = cast(InteractionResponse, interaction.response)
        followup = cast(Webhook, interaction.followup)

        pool = self.bot.pool
        guild = interaction.guild
        if not pool or not guild:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        faqs = await get_faq_names(pool=pool, guild_id=guild.id)

        if not faqs:
            await resp.send_message(fetch_line("fail find", "any FAQ"), ephemeral=True)
            return

        # A bullet point per FAQ, with a small-text list of alternative names for each.
        formatted_lines_iter = (
            f"- {faq[0]}" + (f"\n-# or: {', '.join(faq[1:])}" if faq[1:] else "")
            for faq in faqs
        )

        response_iter = paginate_for_interaction_response(
            formatted_lines_iter,
            header="""
# Available FAQs:\n
-# Type `/explain NAME` to read a FAQ.
            """.strip(),
        )

        await resp.send_message(next(response_iter), ephemeral=True)
        for part in response_iter:
            await followup.send(part, ephemeral=True)

    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(description="Show the answer to a FAQ.")
    @discord.app_commands.describe(
        entry="The name of the FAQ to show.",
        private="If true, only you will see the FAQ.",
    )
    @discord.app_commands.autocomplete(entry=autocomplete_unique_faq_names)
    async def explain(
        self,
        interaction: Interaction,
        entry: str,
        private: bool = False,
    ) -> None:
        resp = cast(InteractionResponse, interaction.response)

        pool = self.bot.pool
        guild = interaction.guild
        if not pool or not guild:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        entry = entry.lower()

        if entry in EASTER_EGGS:
            await resp.send_message(fetch_easter_egg(entry), ephemeral=private)
            return

        faq = await get_faq(entry, pool=pool, guild_id=guild.id)
        if not faq:
            similar_faqs = await get_similar_faqs(entry, pool=pool, guild_id=guild.id)
            await send_similar_faqs_message(
                searched_name=entry,
                similar_names=similar_faqs,
                mechanism=lambda m: resp.send_message(m, ephemeral=True),
            )
            return

        # Record summon for stats.
        async with pool.acquire() as conn:
            await conn.execute(
                """
INSERT INTO faqs_summons (faq_id, user_id) VALUES ($1, $2);
                """,
                faq.db_id,
                interaction.user.id,
            )

        author = self.bot.get_user(faq.author_id)
        author_name_string = f"-# By {author.display_name}" if author else ""

        formatted_faq = f"""
## {faq.name}\n
{author_name_string}\n
{faq.content}
        """

        await resp.send_message(formatted_faq, ephemeral=private)

    @has_permissions(manage_channels=True)
    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(description="Edit an existing FAQ.")
    @discord.app_commands.describe(
        entry="The name of the FAQ to edit. Not all aliases are autocompleted.",
    )
    @discord.app_commands.autocomplete(entry=autocomplete_unique_faq_names)
    async def edit_faq(
        self,
        interaction: Interaction,
        entry: str,
    ) -> None:
        resp = cast(InteractionResponse, interaction.response)

        pool = self.bot.pool
        guild = interaction.guild
        if not pool or not guild:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        faq = await get_faq(entry, pool=pool, guild_id=guild.id)
        if not faq:
            similar_faqs = await get_similar_faqs(entry, pool=pool, guild_id=guild.id)
            await send_similar_faqs_message(
                searched_name=entry,
                similar_names=similar_faqs,
                mechanism=lambda m: resp.send_message(m, ephemeral=True),
            )
            return

        await EditFaq(
            existing_entry=faq,
            pool=pool,
            interaction=interaction,
        ).send()

    @has_permissions(manage_channels=True)
    @discord.app_commands.guilds(*DEFAULT_GUILDS_FOR_APP_COMMANDS)
    @discord.app_commands.command(description="Create a new FAQ.")
    async def add_new_faq(self, interaction: Interaction) -> None:
        resp = cast(InteractionResponse, interaction.response)

        pool = self.bot.pool
        guild = interaction.guild
        if not pool or not guild:
            await resp.send_message(fetch_line("not ready"), ephemeral=True)
            return

        await CreateFaq(
            pool=pool,
            interaction=interaction,
            guild=guild,
        ).send()
