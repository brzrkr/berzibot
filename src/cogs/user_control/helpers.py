"""Helper functions for the user control cog."""

from collections.abc import Callable, Coroutine
from datetime import datetime

from asyncpg import Pool
from discord import Guild, Member, Role

available_roles: dict[
    Guild,
    dict[Role | None, set[Role]],
] = {}
"""Mapping of guilds to a mapping of each role and set of roles that it can claim.
Roles mapped to None can be claimed by anyone.
Effectively, roles mapped to themselves will allow one-time removal.

Populated in the cog's on_ready method.
"""
role_aliases: dict[Guild, dict[Role, list[str]]] = {}
aliased_roles: dict[Guild, dict[str, Role]] = {}
"""Case-insensitive aliases of roles to expand role searches by name, sorted by
guild.

Populated in the cog's on_ready method.
"""


async def get_guild_mute_role(*, pool: Pool, guild: Guild) -> Role | None:
    """Fetch the role configured as role for muted users in the given guild."""
    async with pool.acquire() as conn:
        role_id: int = await conn.fetchval(
            """
SELECT role_id FROM uc_mute_roles WHERE guild_id = $1;
        """,
            guild.id,
        )

    return guild.get_role(role_id)


async def mute_member(
    *,
    pool: Pool,
    member: Member,
    unmute_at: datetime,
    author_display_name: str,
    error_msg_send_func: Callable[[str], Coroutine[None, None, object]],
) -> None:
    """Mute the given member until the specified timestamp, temporarily replacing their
    roles with the guild's configured mute role.
    """

    guild_id = member.guild.id

    if not (guild_mute_role := await get_guild_mute_role(pool=pool, guild=member.guild)):
        await error_msg_send_func("No mute role is configured for this server.")
        return

    if removable_roles := member.roles[1:]:
        await member.remove_roles(*removable_roles)

    await member.add_roles(
        guild_mute_role,
        reason=f"Muted by {author_display_name}",
    )

    async with pool.acquire() as conn:
        await conn.execute(
            """
INSERT INTO uc_mutes (guild_id, user_id, unmute_at, is_muted)
VALUES ($1, $2, $3, true);
            """,
            guild_id,
            member.id,
            unmute_at,
        )

        if removable_roles:
            # Save removed roles to put them back later
            roles_as_string = ",".join(str(role.id) for role in removable_roles)

            await conn.execute(
                """
INSERT INTO uc_muted_users_roles (guild_id, user_id, roles)
VALUES ($1, $2, $3);
                """,
                guild_id,
                member.id,
                roles_as_string,
            )
