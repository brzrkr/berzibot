"""Extensions for user- and role-related features."""

from collections.abc import Iterable
from typing import TYPE_CHECKING

import discord
from discord import Role
from discord.ext import tasks
from discord.ext.commands import Cog, has_permissions

from src.components.berzicommand import berzi_command
from src.components.context import BerziContext
from src.components.context import BerziContext as Context
from src.components.localisation import fetch_line
from src.components.log import prepare_logger
from src.components.utils import BerzibotCog, time_converter, time_difference
from src.config import (
    ITALIAN_LEARNING_AND_DISCUSSION_GUILD_ID,
    SICILIAN_LEARNING_AND_DISCUSSION_GUILD_ID,
)

from .constants import (
    BAN_FOREVER_ARGUMENT,
    ITA_LEVEL_A_ROLE_ID,
    ITA_LEVEL_B_ROLE_ID,
    ITA_LEVEL_C_ROLE_ID,
    LEARNING_ITALIAN_ROLE_ID,
    LESSON_HOST_ROLE_ID,
    SCN_LEVEL_A_ROLE_ID,
    SCN_LEVEL_B_ROLE_ID,
    SCN_LEVEL_C_ROLE_ID,
    SPEAK_ENGLISH_ROLE_ID,
    SPEAK_ITALIAN_ROLE_ID,
    TUTOR_ROLE_ID,
    VOICECHATTER_ROLE_ID,
)
from .helpers import (
    aliased_roles,
    available_roles,
    get_guild_mute_role,
    mute_member,
    role_aliases,
)

if TYPE_CHECKING:
    from src.berzibot import Berzibot

logger = prepare_logger(__name__)


class UserControl(BerzibotCog):
    """Hold all functionality related to moderating users."""

    def __init__(self, bot: "Berzibot") -> None:
        available_roles.clear()
        role_aliases.clear()
        aliased_roles.clear()

        super().__init__(bot)

    async def cog_unload(self) -> None:
        self.unban.cancel()
        self.unmute_loop.cancel()

    @Cog.listener()
    async def on_ready(self) -> None:
        self._populate_assignable_roles()

        self.unmute_loop.start()

        self.unban.start()

    def _populate_italian_learning_server_roles(self) -> None:
        if italian_learning := self.bot.get_guild(
            ITALIAN_LEARNING_AND_DISCUSSION_GUILD_ID,
        ):
            italian_learning_roles: dict[Role | None, set[Role]] = {None: set()}

            lesson_host = italian_learning.get_role(LESSON_HOST_ROLE_ID)
            if lesson_host:
                italian_learning_roles[lesson_host] = {lesson_host}

            learning_italian = italian_learning.get_role(LEARNING_ITALIAN_ROLE_ID)

            for role in (
                lesson_host,
                learning_italian,
                italian_learning.get_role(VOICECHATTER_ROLE_ID),
                italian_learning.get_role(TUTOR_ROLE_ID),
                italian_learning.get_role(ITA_LEVEL_A_ROLE_ID),
                italian_learning.get_role(ITA_LEVEL_B_ROLE_ID),
                italian_learning.get_role(ITA_LEVEL_C_ROLE_ID),
            ):
                if not role:
                    continue

                italian_learning_roles[None].add(role)

            available_roles[italian_learning] = dict(italian_learning_roles)

            if learning_italian:
                aliased_roles[italian_learning] = {"learning": learning_italian}
        else:
            logger.error("Could not find italian learning guild.")

    def _populate_sicilian_learning_server_roles(self) -> None:
        if sicilian_learning := self.bot.get_guild(
            SICILIAN_LEARNING_AND_DISCUSSION_GUILD_ID,
        ):
            sicilian_learning_roles: dict[Role | None, set[Role]] = {None: set()}

            scn_speak_english = sicilian_learning.get_role(SPEAK_ENGLISH_ROLE_ID)
            scn_speak_italian = sicilian_learning.get_role(SPEAK_ITALIAN_ROLE_ID)

            for role in (
                scn_speak_english,
                scn_speak_italian,
                sicilian_learning.get_role(SCN_LEVEL_A_ROLE_ID),
                sicilian_learning.get_role(SCN_LEVEL_B_ROLE_ID),
                sicilian_learning.get_role(SCN_LEVEL_C_ROLE_ID),
            ):
                if not role:
                    continue

                sicilian_learning_roles[None].add(role)

            available_roles[sicilian_learning] = dict(sicilian_learning_roles)

            if sicilian_learning:
                aliased_roles[sicilian_learning] = {}

                if scn_speak_italian:
                    aliased_roles[sicilian_learning]["italian"] = scn_speak_italian

                if scn_speak_english:
                    aliased_roles[sicilian_learning]["english"] = scn_speak_english
        else:
            logger.error("Could not find sicilian learning guild.")

    def _populate_assignable_roles(self) -> None:
        self._populate_italian_learning_server_roles()
        self._populate_sicilian_learning_server_roles()

        role_aliases.clear()
        for guild in aliased_roles:
            role_aliases[guild] = {}
            for alias in aliased_roles[guild]:
                try:
                    role_aliases[guild][aliased_roles[guild][alias]].append(
                        alias,
                    )
                except KeyError:
                    role_aliases[guild][aliased_roles[guild][alias]] = [alias]

    async def _unmute(self, to_unmute: Iterable[tuple[int, int]]) -> None:
        if not self.bot.pool:
            return

        for guild_id, user_id in to_unmute:
            guild = self.bot.get_guild(guild_id)
            if not guild:
                continue

            # Unmark as muted
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    """
                    UPDATE uc_mutes
                    SET is_muted=false
                    WHERE user_id=$1
                    AND guild_id=$2
                    AND is_muted=true
                    """,
                    user_id,
                    guild_id,
                )

            if not (member := guild.get_member(user_id)):
                continue

            # Remove mute role
            role = await get_guild_mute_role(pool=self.bot.pool, guild=guild)
            if not role:
                logger.error(
                    "Role not found when trying to remove mute role for "
                    "user %s (%s) in guild %s (%s).",
                    member.display_name,
                    member.id,
                    guild.name,
                    guild.id,
                )
                continue

            await member.remove_roles(role, reason="Served mute time.", atomic=True)

            # Reassign previous roles
            async with self.bot.pool.acquire() as conn:
                to_reassign: str = await conn.fetchval(
                    """
                    SELECT roles
                    FROM uc_muted_users_roles
                    WHERE guild_id=$1 AND user_id=$2
                    """,
                    guild_id,
                    user_id,
                )

                await conn.execute(
                    """
                    DELETE FROM uc_muted_users_roles
                    WHERE guild_id=$1 AND user_id=$2
                    """,
                    guild_id,
                    user_id,
                )

            if to_reassign:
                roles = [
                    found_role
                    for role_id in to_reassign.split(",")
                    if (found_role := guild.get_role(int(role_id)))
                ]

                await member.add_roles(
                    *roles,
                    reason="Reassigning roles from before the mute.",
                    atomic=True,
                )

            logger.info(
                "Unmuted %s from guild %s.",
                member.display_name,
                guild.name,
            )

    @tasks.loop(minutes=1.0)
    async def unmute_loop(self) -> None:
        """Unmute all users scheduled to be unmuted."""
        if not self.bot.pool:
            return

        async with self.bot.pool.acquire() as conn:
            unmute = await conn.fetch(
                """
            SELECT guild_id, user_id
            FROM uc_mutes
            WHERE is_muted=true
            AND unmute_at<=NOW()
                """,
            )

        await self._unmute((guild_id, user_id) for guild_id, user_id in unmute)

    @tasks.loop(minutes=5.0)
    async def unban(self) -> None:
        """Unban all users scheduled to be unbanned."""
        if not self.bot.pool:
            return

        async with self.bot.pool.acquire() as conn:
            unban_list = await conn.fetch(
                """
SELECT server, banned_user
FROM uc_bans
WHERE ban_end IS NOT NULL AND ban_end <= now()
ORDER BY server;
                """,
            )

            for server_id, user_id in unban_list:
                guild = self.bot.get_guild(server_id)
                user = self.bot.get_user(user_id)

                await conn.execute("DELETE FROM uc_bans WHERE banned_user=$1", user_id)

                if not guild or not user:
                    continue

                try:
                    await guild.unban(user, reason="Ban time elapsed.")
                except discord.NotFound:
                    continue

                logger.info("%s has been automatically unbanned from %s.", user, guild)

    @unban.before_loop
    async def pre_unban(self) -> None:
        await self.bot.wait_until_ready()

    @has_permissions(ban_members=True)
    @berzi_command(name="ban", meta={"mod_only": True})
    async def ban(
        self,
        ctx: BerziContext,
        user: discord.Member,
        *,
        time: str = BAN_FOREVER_ARGUMENT,
    ) -> None:
        """Ban a user for a certain amount of time, after which they will be
        automatically unbanned.
        """

        if not self.bot.pool:
            return

        # Ban user immediately.
        guild = user.guild
        await guild.ban(user)
        await ctx.send(fetch_line("successful"))

        for_time = time
        if time != BAN_FOREVER_ARGUMENT:
            time_dt = time_converter(time)

            if time:
                async with self.bot.pool.acquire() as conn:
                    await conn.execute(
                        "INSERT INTO uc_bans"
                        " (server, banning_user, banned_user, ban_end)"
                        " VALUES ($1, $2, $3, $4);",
                        guild.id,
                        ctx.author.id,
                        user.id,
                        time_dt,
                    )
                time_diff = time_difference(to=time_dt)
                for_time = (
                    "for " + (", ".join(time_diff[:-1]) + f" and {time_diff[-1]}")
                    if len(time_diff) > 1
                    else time_diff[-1]
                )

        removed_messages = ""
        async with ctx.ask_bool(
            "Should I also remove their messages from today?",
        ) as confirm:
            if confirm:
                await guild.unban(user)
                await guild.ban(user, delete_message_days=1)
                removed_messages = " and removed all their messages for the day."

        await ctx.send(fetch_line("successful"))

        logger.info("%s banned %s %s%s.", ctx.author, user, for_time, removed_messages)

    @has_permissions(manage_roles=True)
    @berzi_command(name="mute", meta={"mod_only": True})
    async def mute(
        self,
        ctx: Context,
        user: discord.Member,
        *,
        duration: str,
    ) -> None:
        """Mute a user, specifying a duration after which they will be automatically
        unmuted.
        """

        if not self.bot.pool:
            return

        guild = user.guild
        member = guild.get_member(user.id)
        if not member:
            return

        duration_time = time_converter(duration)

        await mute_member(
            pool=self.bot.pool,
            member=member,
            unmute_at=duration_time,
            author_display_name=ctx.author.display_name,
            error_msg_send_func=lambda msg: ctx.send(msg),
        )

        await ctx.send(fetch_line("successful"))

        logger.info(
            "%s has been muted from %s by %s %s.",
            member.name,
            guild.name,
            ctx.author.display_name,
            f" until {duration_time}" if duration_time else "",
        )

    @has_permissions(manage_roles=True)
    @berzi_command(name="unmute", meta={"mod_only": True})
    async def unmute(self, ctx: Context, user: discord.Member) -> None:
        """Unmute a user, if currently muted."""
        guild = user.guild

        await self._unmute(((guild.id, user.id),))

        await ctx.send(fetch_line("successful"))
        await ctx.send(fetch_line("might take time"))

        logger.info(
            "%s has been unmuted from %s by %s (if they were muted).",
            user.name,
            guild.name,
            ctx.author.display_name,
        )
