"""Private features for testing purposes."""

import io
import time
import traceback
from typing import Final, cast

from discord import TextChannel
from discord.ext.commands import is_owner

from src.berzibot import Berzibot
from src.cogs.thing_of_the_day.formatter import ThingFormatter
from src.cogs.thing_of_the_day.types import TotdEntry
from src.components.berzicommand import berzi_command
from src.components.context import BerziContext as Context
from src.components.log import prepare_logger
from src.components.utils import (
    BerzibotCog,
    cleanup_code,
    current_timeofday,
    format_table,
    time_converter,
)

logger = prepare_logger(__name__)

MAX_RESULT_LENGTH: Final[int] = 2_000


async def setup(bot: Berzibot) -> None:  # noqa: D103
    logger.info("Loading cog: %s.", __name__)
    await bot.add_cog(Testing(bot))


class Testing(BerzibotCog, command_attrs={"hidden": True}):
    """Hold all commands for testing purposes."""

    @is_owner()
    @berzi_command(name="rolepost")
    async def rolepost(self, ctx: Context) -> None:  # noqa: D102
        message = await ctx.send("Here, react to this.")
        await message.add_reaction("✔")
        await message.add_reaction("❌")

    @is_owner()
    @berzi_command(name="echo", hidden=True)
    async def echo(self, ctx: Context, content: str = "") -> None:
        """Repeat what was sent as argument."""
        await ctx.send(content)

    @is_owner()
    @berzi_command(name="server_time")
    async def server_time(self, ctx: Context) -> None:
        """Return server time as used by any of the bot's time-sensitive operations."""
        await ctx.send(current_timeofday().isoformat())

    @is_owner()
    @berzi_command(name="time")
    async def time(
        self,
        ctx: Context,
        *,
        content: time_converter,  # type: ignore[valid-type]
    ) -> None:
        """Debug the time_converter."""
        await ctx.send(content)

    @is_owner()
    @berzi_command(name="publish_totd")
    async def publish_totd(self, ctx: Context) -> None:
        """Publish all Things of the Day scheduled for the current minute, if any."""
        current_timeofday_ = current_timeofday()

        if not (pool := self.bot.pool):
            return

        async with pool.acquire() as conn:
            entries = await conn.fetch(
                """
SELECT
    e.id,
    qualifier,
    content,
    extra,
    t.server,
    t.channel,
    (
        SELECT COUNT(*) FROM totd_mentions m
        WHERE m.entry_id=e.id
        AND m.channel=t.channel
        AND m.server=t.server
    ) AS appeared
FROM totd_entries e JOIN totd_tasks t ON t.language_id=e.language_id
WHERE is_active=true
AND extract(hour FROM send_at at time zone 'utc')::int=$1
AND extract(minute FROM send_at at time zone 'utc')::int=$2
ORDER BY e.language_id, appeared, random();
                """,
                current_timeofday_.hour,
                current_timeofday_.minute,
            )

        for entry_id, qualifier, content, extra, server, channel in entries:
            await conn.execute(
                """
INSERT INTO totd_mentions
(entry_id, channel, server)
VALUES($1, $2, $3);
                """,
                entry_id,
                channel,
                server,
            )

            if not (guild := self.bot.get_guild(server)):
                return

            if not isinstance((location := guild.get_channel(channel)), TextChannel):
                return

            entry = TotdEntry(qualifier=qualifier, content=content, extra=extra)
            embed = ThingFormatter.format_as_embed(entry=entry, guild=guild)
            embed.colour = self.bot.user.colour if self.bot.user else None

            await location.send(embed=embed)
            await ctx.send(embed=embed)

    @is_owner()
    @berzi_command(name="sql", hidden=True)
    async def sql(self, ctx: Context, *, query: str) -> None:
        """Run some SQL."""
        query = cleanup_code(query)

        is_multistatement = query.count(";") > 1

        if not ctx.db:
            return

        # noinspection PyBroadException
        try:
            start = time.perf_counter()

            if is_multistatement:
                results = cast(list[dict[str, object]], await ctx.db.execute(query))
            else:
                results = cast(list[dict[str, object]], await ctx.db.fetch(query))

            dt = (time.perf_counter() - start) * 1000.0
        except Exception:  # noqa: BLE001
            await ctx.send(f"```py\n{traceback.format_exc()}\n```")
            return

        if is_multistatement or len(results) == 0:
            await ctx.send(f"`{dt:.2f}ms: {results}`")
            return

        render = format_table(results)

        fmt = f"```\n{render}\n```\n*Returned row(s) in {dt:.2f}ms*"
        if len(fmt) > MAX_RESULT_LENGTH:
            async with ctx.bot.session.post(
                "http://0x0.st",
                data={"file": io.StringIO(fmt)},
            ) as r:
                await ctx.send(f"Too many results: {await r.text()}")
                return

        await ctx.send(fmt)
