# noqa: D100
import asyncio
import logging
import os
import sys
import traceback
from collections.abc import Iterable
from datetime import datetime, timezone
from typing import TYPE_CHECKING, NotRequired, ParamSpec, TypedDict, Unpack

import aiohttp
import asyncpg
import discord
import sentry_sdk
from discord import DiscordException, Intents, NotFound
from discord.abc import Snowflake
from discord.app_commands import CommandTree
from discord.app_commands.commands import ContextMenuCallback
from discord.ext.commands import (
    Bot,
    CommandInvokeError,
    CommandNotFound,
    Context,
    HelpCommand,
    MissingRequiredArgument,
    NotOwner,
    UserInputError,
)
from discord.ext.commands import (
    CheckFailure as TextCommandCheckFailure,
)
from discord.utils import MISSING
from sentry_sdk.integrations.logging import LoggingIntegration

from src.components.context import BerziContext
from src.components.expiring_cache import ExpiringCache
from src.components.localisation import fetch_line
from src.components.log import discord_handler, prepare_logger
from src.config import (
    DB_STRING,
    DEBUG,
    DEFAULT_GUILDS_FOR_APP_COMMANDS,
    RECENT_COMMAND_TIME,
    TEST_GUILD_ID,
    TOKEN,
)

if TYPE_CHECKING:
    from discord.ext.commands._types import ContextT
    from discord.ext.commands.bot import PrefixType

    # noinspection PyUnresolvedReferences
    from discord.interactions import Interaction
    from discord.message import Message

P_OnError = ParamSpec("P_OnError")

logger = prepare_logger(__name__)


async def _prefix_callable(bot: "Berzibot", msg: discord.Message) -> Iterable[str]:
    if not await bot.should_receive_message(msg):
        return []

    prefixes = ["Berzibot, ", "Berzibot! ", "Berzibot? "]
    prefixes.extend(s.lower() for s in prefixes.copy())

    if bot.user and (user_id := bot.user.id):
        prefixes.extend([f"<@!{user_id}> ", f"<@{user_id}> "])

    if DEBUG:
        # List longer versions of prefixes first.
        prefixes.extend(["dbg,", "dbg", "debug,", "debug"])

    if msg.guild is None:
        # Allow prefixless invocation in DMs.
        # The empty string must be the last prefix added.
        prefixes.append("")

    return prefixes


class _BerzibotKwargs(TypedDict):
    intents: Intents
    help_command: NotRequired[HelpCommand | None]
    tree_cls: NotRequired[type[CommandTree]]
    description: NotRequired[str | None]
    case_insensitive: bool
    strip_after_prefix: bool


class Berzibot(Bot):  # noqa: D101
    default_activity = discord.Activity(
        type=discord.ActivityType.listening,
        name=fetch_line("listening to"),
    )

    _sleeping: bool = False
    _sending_errors: bool = False

    def __init__(
        self,
        command_prefix: "PrefixType[Berzibot]",
        release: str,
        **options: Unpack[_BerzibotKwargs],
    ) -> None:
        super().__init__(command_prefix, **options)

        self.release = release

        self.pool: asyncpg.pool.Pool | None = None

        self.loaded_cogs: set[str] = set()
        """Set of loaded cog names in snake_case."""

        self.session: aiohttp.ClientSession | None = None

        self._latest_commands: ExpiringCache[int, BerziContext] = ExpiringCache(
            RECENT_COMMAND_TIME,
        )
        """Dict of callers to invocation context."""

    async def _populate_db_pool(self) -> None:
        self.pool = await asyncpg.create_pool(DB_STRING)

    async def _populate_aiohttp_session(self) -> None:
        self.session = aiohttp.ClientSession()

    def _register_context_menu(
        self,
        *,
        func: ContextMenuCallback,
        name: str,
        guild: Snowflake | None,
    ) -> None:
        self.tree.context_menu(name=name, guilds=[guild] if guild else MISSING)(func)

    async def setup_hook(self) -> None:  # noqa: D102
        await self.loop.create_task(self._populate_db_pool())
        await self.loop.create_task(self._populate_aiohttp_session())

        logger.info("Loading extensions.")
        await self.load_extension("cogs.basic_functionality")
        await self.load_extension("cogs.chat_control")
        await self.load_extension("cogs.user_control")
        await self.load_extension("cogs.thing_of_the_day")
        await self.load_extension("cogs.faqs")
        await self.load_extension("cogs.voicechat_control")
        await self.load_extension("cogs.scheduled_events")

        from src.cogs.faqs.cog import add_as_faq

        context_menus: dict[str, tuple[set[int], ContextMenuCallback]] = {
            "Add as FAQ": (set(DEFAULT_GUILDS_FOR_APP_COMMANDS), add_as_faq),
        }

        if DEBUG:
            await self.load_extension("cogs.testing")

            # Only sync to the test guild while debugging.
            try:
                test_guild = await self.fetch_guild(TEST_GUILD_ID)
            except NotFound:
                logger.exception(
                    "Could not find test guild %s: will not sync command tree.",
                    TEST_GUILD_ID,
                )
            else:
                for name, menu in context_menus.items():
                    if TEST_GUILD_ID not in menu[0]:
                        continue

                    self._register_context_menu(name=name, guild=test_guild, func=menu[1])

                logger.info("Syncing command tree for DEBUG guild.")
                await self.tree.sync(guild=test_guild)
        else:
            # Sync to all guilds when not debugging.
            for guild_id in DEFAULT_GUILDS_FOR_APP_COMMANDS:
                # noinspection PyBroadException
                try:
                    guild = await self.fetch_guild(guild_id)
                except Exception:
                    logger.exception("Could not fetch guild for ID %s", guild_id)
                    continue

                for name, menu in context_menus.items():
                    if guild_id not in menu[0]:
                        continue

                    self._register_context_menu(name=name, guild=guild, func=menu[1])

                logger.info("Syncing command tree for %s.", guild.name)
                await self.tree.sync(guild=guild)

            # Only sync global commands when not debugging.
            for name, menu in context_menus.items():
                if menu[0]:
                    continue

                self._register_context_menu(name=name, guild=None, func=menu[1])

            logger.info("Syncing command tree for global commands.")
            await self.tree.sync()

    async def should_receive_message(self, msg: discord.Message) -> bool:
        """Return whether the given message should be read by the bot."""
        is_owner = await self.is_owner(msg.author)

        return not (DEBUG and msg.guild != TEST_GUILD_ID and not is_owner)

    @property
    def sleeping(self) -> bool:
        """Whether the bot is currently sleeping."""
        return self._sleeping

    @property
    def sending_errors(self) -> bool:
        """Whether errors are currently being sent to the owner as they happen."""
        return self._sending_errors

    async def set_sleeping(self, *, value: bool) -> None:
        """Set sleeping mode."""
        if value is True:
            self._sleeping = True
            activity = discord.Game(fetch_line("playing dead"))
            await self.change_presence(
                activity=activity,
                status=discord.Status.idle,
            )
        else:
            self._sleeping = False
            await self.change_presence(
                activity=self.default_activity,
                status=discord.Status.online,
            )

    def set_sending_errors(self, *, value: bool) -> None:
        """Set whether to send errors to the owner as they happen."""
        if isinstance(value, bool):
            self._sending_errors = value
            return

        raise TypeError

    async def get_context(  # type: ignore[override] # noqa: D102
        self,
        origin: "Message | Interaction",
        /,
        *,
        cls: type["ContextT"] = MISSING,  # noqa: ARG002
    ) -> BerziContext:
        return await super().get_context(origin, cls=BerziContext)

    async def process_commands(self, message: discord.Message) -> None:  # noqa: D102
        ctx = await self.get_context(message)

        await super().process_commands(message)

        if ctx.command is None:
            logger.debug("Command is None.")
            if (
                ctx.guild
                and ctx.guild.me in message.mentions
                and message.reference is None
            ):
                await ctx.send(fetch_line("hello"))

            return

        self._latest_commands[message.author.id] = ctx

    async def on_ready(self) -> None:  # noqa: D102
        await self.change_presence(
            activity=self.default_activity,
            status=discord.Status.online,
        )

    async def on_command_error(  # noqa: D102
        self,
        ctx: Context,
        error: DiscordException,
    ) -> None:
        if isinstance(error, CommandInvokeError):
            err = error.original
            exc = "".join(
                traceback.format_exception(
                    type(err),
                    value=err,
                    tb=err.__traceback__,
                )
            )
            logger.error(exc)

        if isinstance(error, NotOwner):
            await ctx.send(fetch_line("only owner"))
            return

        if isinstance(error, TextCommandCheckFailure):
            await ctx.send(fetch_line("insufficient permissions"))
            return

        if isinstance(error, UserInputError):
            if isinstance(error, MissingRequiredArgument):
                param_name = error.param.displayed_name or error.param.name
                message = f"You need to give me a {param_name}."
            else:
                message = "\n".join(error.args)

            await ctx.send(f"{fetch_line('user error')}\n{message}".strip())
            return

        if isinstance(error, CommandNotFound):
            await ctx.send(fetch_line("unknown", "this command"))
            return

        logger.critical(
            "Uncaught command error: %s",
            type(error),
            extra={
                "error": error,
                "error_args": error.args,
            },
        )

        await ctx.send(fetch_line("fail"))

    EXCEPTIONS_TO_SKIP: tuple[type[Exception], ...] = (
        asyncio.exceptions.TimeoutError,
        CommandNotFound,
    )

    async def on_error(  # type: ignore[override] # noqa: D102
        self,
        _event: str,
        *_args: P_OnError.args,
        **_kwargs: P_OnError.kwargs,
    ) -> None:
        exception_info = sys.exc_info()
        if not isinstance(exception_info[0], self.EXCEPTIONS_TO_SKIP):
            traceback.print_exc()
            logger.exception(exception_info)

            if self.sending_errors and self.owner_id:
                owner = self.get_user(self.owner_id)
                if not owner:
                    return

                await owner.send(str(exception_info))

    async def on_message(self, message: discord.Message) -> None:  # noqa: D102
        if message.author.bot or not await self.should_receive_message(message):
            return

        if self.sleeping and not await self.is_owner(message.author):
            logger.debug("Command ignored because of sleep mode.")
            return

        logger.debug("Processing commands for message: %s", message.content)
        await self.process_commands(message)

    async def notify_mods(self, message: str, /) -> None:
        """Notify mods about a problem. Takes care of mentioning the mods automatically.

        Ad-hoc method currently only implemented for ILAD.
        """

        guild = self.get_guild(240436732305211392)
        if not guild:
            logger.error("Could not find ILAD under ID 240436732305211392.")
            return

        destination_channel = guild.get_channel_or_thread(1213093867503755294)
        if not destination_channel or not hasattr(destination_channel, "send"):
            logger.error("Could not find mod log in ILAD under ID 1213093867503755294.")
            return

        mod_role = guild.get_role(261245054306353152)
        if not mod_role:
            logger.error("Could not find mod role in ILAD under ID 261245054306353152.")
            return

        await destination_channel.send(f"{mod_role.mention} {message}")


async def run_berzibot(release: str) -> None:
    """Load all cogs and start the bot."""
    logger.info("Initialising bot instance.")
    intents = discord.Intents.all()

    # noinspection PyTypeChecker
    berzibot = Berzibot(
        command_prefix=_prefix_callable,
        intents=intents,
        case_insensitive=True,  # Note: won't affect command groups automatically.
        strip_after_prefix=True,
        release=release,
    )

    # Remove default help to override with custom one in basic_functionality.
    berzibot.remove_command("help")

    logger.info("Starting bot (release=%s).", release)
    async with berzibot:
        await berzibot.start(TOKEN)

    logger.info("Stopping bot (release=%s).", release)


def initialise_sentry(release: str) -> None:
    """Initialise Sentry to collect runtime errors."""
    if sentry_dsn := os.environ.get("BERZIBOT_SENTRY_DSN"):
        logger.info("Initialising Sentry.")
        sentry_sdk.init(
            dsn=sentry_dsn,
            traces_sample_rate=0.2,
            profiles_sample_rate=0.1,
            environment=os.environ.get("BERZIBOT_ENV", "production"),
            release=release,
            integrations=[LoggingIntegration(level=logging.ERROR)],
        )
    else:
        logger.warning("Sentry DSN not specified.")


def main() -> None:  # noqa: D103
    release = os.environ.get("BERZIBOT_RELEASE", str(datetime.now(timezone.utc)))

    initialise_sentry(release)

    if DEBUG:
        discord_logger = logging.getLogger("discord")
        discord_logger.setLevel(logging.INFO)
        discord_logger.addHandler(discord_handler)

    asyncio.run(run_berzibot(release))


if __name__ == "__main__":
    main()
