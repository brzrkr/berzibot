"""Configuration variables for Berzibot."""

import os
from typing import Final

TOKEN: Final[str] = os.environ["BERZIBOT_TOKEN"]

DEBUG: Final[bool] = bool(os.environ.get("BERZIBOT_DEBUG"))
TEST_GUILD_ID: Final[int] = int(os.environ.get("BERZIBOT_TEST_GUILD", ""))

ITALIAN_LEARNING_AND_DISCUSSION_GUILD_ID: Final[int] = 240436732305211392
SICILIAN_LEARNING_AND_DISCUSSION_GUILD_ID: Final[int] = 496761859462922240

DEFAULT_GUILDS_FOR_APP_COMMANDS: Final[tuple[int, ...]] = (
    TEST_GUILD_ID,
    ITALIAN_LEARNING_AND_DISCUSSION_GUILD_ID,
    SICILIAN_LEARNING_AND_DISCUSSION_GUILD_ID,
)

DB_STRING: Final[str] = os.environ["BERZIBOT_DB_STRING"]

LANGUAGE: Final[str] = "eng"

DEFAULT_DELETE_AFTER: Final[float | int] = 3.0
"""Default time to keep the bot's response after commands where it's desirable to
delete it.
"""

PAGE_SIZE: Final[int] = 4
"""Default number of elements per page for pagination purposes."""

DEFAULT_TIMEOUT: Final[float | int] = 30.0
"""Default timeout in seconds for interactive elements."""

RECENT_COMMAND_TIME: Final[float | int] = DEFAULT_TIMEOUT
"""Number of seconds to keep recent commands cached for use with chain keywords."""

MINIMUM_AUTOCOMPLETE_LENGTH: Final[int] = 2
"""Minimum length of input strings to trigger autocomplete, to limit db calls."""

ITEMS_LOAD_LIMIT: Final[int] = 25
"""Amount of maximum items some Discord parameters support."""

API_STRING_LIMIT: Final[int] = 100
"""Maximum string length some Discord parameters allow."""

MODAL_TITLE_MAX_LENGTH: Final[int] = 45

INTERACTION_RESPONSE_MESSAGE_MAX_LENGTH: Final[int] = 2000
