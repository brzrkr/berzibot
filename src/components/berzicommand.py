"""Custom command types for Berzibot."""

from collections.abc import Callable
from copy import copy
from typing import Any, ParamSpec

from discord.ext.commands import Command, Group, command
from discord.ext.commands.core import MISSING

P_BerziGroup = ParamSpec("P_BerziGroup")
P_BerziCommand = ParamSpec("P_BerziCommand")
P_HasMeta = ParamSpec("P_HasMeta")


class HasMeta:
    """Mixin for a class which has metadata."""

    def __init__(self, *args: P_HasMeta.args, **kwargs: P_HasMeta.kwargs) -> None:
        super().__init__(*args, **kwargs)

        meta = kwargs.get("meta", {})
        if not isinstance(meta, dict):
            message = "meta must be a dictionary."
            raise TypeError(message)

        self._meta: dict[str, object] = copy(meta)

    def get_meta(self, key: str) -> object | None:
        """Get the meta value for the given key if present."""
        return self._meta.get(key, None)


class BerziCommand(HasMeta, Command):
    """Command class able to store arbitrary attributes in the meta property."""

    def __init__(
        self,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        super().__init__(*args, **kwargs)


def berzi_command(
    name: str,
    *args: Any,
    **attrs: Any,
) -> Callable[..., BerziCommand]:
    """Transform a function into a :class:`BerziCommand`."""

    def decorator(func: Callable) -> BerziCommand:
        if isinstance(func, Command):
            message = "Callback is already a command."
            raise TypeError(message)

        return BerziCommand(func, name=name, *args, **attrs)

    return decorator


class BerziGroup(HasMeta, Group):
    """Group class able to store arbitrary attributes in the meta property."""

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

    def berzi_command(
        self,
        *args: Any,
        **kwargs: Any,
    ) -> Callable:
        """Invoke :func:`.berzi_command` and add it to the internal command list via
        :meth:`~.GroupMixin.add_command`.
        """

        def decorator(func: Callable) -> Callable:
            kwargs.setdefault("parent", self)
            result = berzi_command(*args, **kwargs)(func)
            self.add_command(result)
            return result

        return decorator

    def berzi_group(
        self,
        *args: Any,
        **kwargs: Any,
    ) -> Callable:
        """Invoke :func:`.berzi_group` and add it to the internal command list via
        :meth:`~.GroupMixin.add_command`.
        """

        def decorator(func: Callable) -> Callable:
            kwargs.setdefault("parent", self)
            result = berzi_group(*args, **kwargs)(func)
            self.add_command(result)
            return result

        return decorator


def berzi_group(
    name: str | None = None,
    **attrs: Any,
) -> Callable[..., BerziGroup]:
    """Transform a function into a :class:`BerziGroup`."""
    return command(  # type: ignore[return-value]
        name=name or MISSING,
        cls=BerziGroup,
        **attrs,
    )
