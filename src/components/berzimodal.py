"""Modal bases and utilities."""

import traceback
from abc import abstractmethod
from typing import cast, override

from asyncpg import Pool
from discord import Interaction, InteractionResponse, Permissions
from discord.ui import Modal
from discord.utils import MISSING

from src.components.localisation import fetch_line
from src.components.log import prepare_logger

logger = prepare_logger(__name__)


class BerziModal(Modal):
    """Base class for modals.

    IMPORTANT: use .send() to send this modal instead of interaction.response.send_modal.
    `interaction` must be provided on init or on send.

    Allows requiring specific permissions to use the modal by overriding the
    get_minimum_permissions method.
    Also requires a database pool to be provided, assuming that it will be needed in
    on_submit.
    """

    @override
    def __init__(
        self,
        *,
        pool: Pool,
        interaction: Interaction | None = None,
        title: str = MISSING,
        timeout: float | None = None,
        custom_id: str = MISSING,
    ) -> None:
        self.interaction = interaction
        self.pool = pool

        super().__init__(title=title, timeout=timeout, custom_id=custom_id)

    @abstractmethod
    def get_minimum_permissions(self) -> Permissions:
        """Minimum permissions required to open the modal."""
        return Permissions.none()

    async def send(self, *, interaction: Interaction | None = None) -> None:
        """Send the modal as a response to the given interaction.

        Use this method instead of interaction.response.send_modal().
        This can be overridden to add additional checks before sending a modal: not
        calling super().send() will prevent the modal to be activated.

        `interaction` must be provided here if it was not given on init.
        """

        interaction = interaction or self.interaction
        if interaction is None:
            msg = "Interaction must be provided on BerziModal init or when sending it."
            raise ValueError(msg)

        resp = cast(InteractionResponse, interaction.response)

        if not interaction.permissions.is_superset(self.get_minimum_permissions()):
            # We handle the error here and not in on_command_error because apparently it
            #  does not receive exceptions from app commands.
            await resp.send_message(
                fetch_line("insufficient permissions"),
                ephemeral=True,
            )
            return

        await resp.send_modal(self)

    @override
    async def on_error(self, interaction: Interaction, error: Exception, /) -> None:  # type: ignore[override]
        logger.error(traceback.format_exception(type(error), error, error.__traceback__))

        resp = cast(InteractionResponse, interaction.response)
        await resp.send_message(
            f"😱 Something went wrong: {error}",
            ephemeral=True,
        )
