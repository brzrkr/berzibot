"""Copied with permission from Nils T.

The MIT License (MIT)
Copyright (c) 2019-2020 Nils T.
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
"""

import time
from typing import Generic, TypeVar, cast

T_CacheKey = TypeVar("T_CacheKey")
T_CacheValue = TypeVar("T_CacheValue")


class ExpiringCache(dict, Generic[T_CacheKey, T_CacheValue]):
    """Cache with auto-expiring content."""

    def __init__(self, seconds: float) -> None:
        self.__ttl = seconds
        super().__init__()

    def __verify_cache_integrity(self) -> None:
        current_time = time.monotonic()
        to_remove = [k for (k, (v, t)) in self.items() if current_time > (t + self.__ttl)]
        for k in to_remove:
            del self[k]

    def __getitem__(self, key: T_CacheKey) -> tuple[T_CacheValue, float]:
        """Get the value for a given key with its last-set timestamp."""
        self.__verify_cache_integrity()
        return cast(tuple[T_CacheValue, float], super().__getitem__(key))

    def __setitem__(self, key: T_CacheKey, value: T_CacheValue) -> None:
        """Set a key-value pair."""
        super().__setitem__(key, (value, time.monotonic()))

    def fetch(self, key: T_CacheKey) -> T_CacheValue | None:
        """Get the value for the given key. Returns None for missing or expired items."""
        self.__verify_cache_integrity()
        try:
            return self[key][0]
        except (KeyError, IndexError):
            return None
