"""Lines for all Berzibot messages in all supported languages."""

import random

from src.components.log import prepare_logger
from src.config import LANGUAGE

logger = prepare_logger(__name__)

LOCALES: dict[str, dict[str, set[str]]] = {
    "eng": {
        "positive response": {
            "yes",
            "yup",
            "yep",
            "y",
            "aye",
            "yeah",
            "ye",
            "yeh",
            "yh",
            "sure",
            "of course",
            "course",
            "ofc",
            "obviously",
            "obv",
            "yessir",
            "affirmative",
            "naturally",
            "certainly",
            "definitely",
            "mhm",
            *"👍👌🙏🍆💯⭕✅🆗✔☑",
        },
        "negative response": {
            "no",
            "n",
            "nope",
            "nop",
            "nah",
            "nay",
            "negative",
            "cancel",
            *"👎✋🖐💩❌🚫🚮✖",
        },
        "invalid subcommand": {"Invalid subcommand for `{}`, you baka."},
        "successful": {"Done. :>", "Alright!", "It is done. ù.ú", "👍", "Aye aye! o7"},
        "fail load": {"I couldn't load {}. :<"},
        "fail find": {"I couldn't find {}. :<"},
        "cogception": {"I can't unload the cog I use to load cogs... :/"},
        "unknown": {"I don't know {}!"},
        "unknown should": {"I don't know {}, should I?"},
        "cancelled": {
            "Nevermind then. :>",
            "Ok, I give up...",
            "Guess I'll go back to my things...",
        },
        "task active": {"There already is an identical task active here!"},
        "task at for": {
            "There is a task at {} for {} already, did you mean to enable it?",
        },
        "fail find language": {"I can't find any{} language. :<"},
        "no match": {
            "I got nothing. :<",
            "Couldn't find anything. :<",
            "Doesn't ring any bells...",
        },
        "already exists": {"{} already exists!"},
        "what language": {"For what language?"},
        "remove language": {
            "Are you sure you want to remove {} **and all its entries**?",
        },
        "need entry name": {"I need a name. Reference the entry by the main word."},
        "sure": {"Are you sure?"},
        "fail": {
            'Oops, I messed something up. >.<"',
            "I done goofed! >.<",
            "Whelp, something didn't work...",
        },
        "only owner": {"I only answer to my master. ù.u", "What if berzi finds out?"},
        "insufficient permissions": {
            "You're not cool enough. :>",
            "Make me. :>",
            "How about no. :P",
            "Nah...",
        },
        "user error": {
            "You got something wrong...",
            "Can you double-check your input?",
            "Hmm, you sure about what you wrote?",
            "Are you sure there's nothing missing?",
        },
        "listening to": {"your pings. ;)"},
        "logout": {
            "Logging out! o7",
            "RIP me. :<",
            "I'm ded.",
            "👋",
            "See you, space cowboy!",
        },
        "too long": {
            "That's too much content!",
            "Your message is too long.",
            "I can't handle so much stuff!",
        },
        "instructions": {
            """Ping me with a command. Write the (sub)command name after `help` for
            specific help.""",
        },
        "no help": {"No help available."},
        "playing dead": {
            "dead...",
        },  # Shown after "Playing..." in status when in sleep mode
        "sleeping": {
            "Shh, I'm trying to sleep here!",
            "Only my prince can wake me up.",
            "Shh, berzi said I must sleep now.",
        },
        "goodnight": {"Good night... 🌃", "Good night! 🛌", "Going to sleep now. 👋"},
        "wake up": {"Hello world!", "Fresh as a rose. 🌹", "Yaaawn! 🥱", "🥱"},
        "totd footer": {
            "You can submit your own findings and TILs here, just post them!",
            "Learned something interesting recently? Post it here!",
            "Leave a reaction if you appreciate Thing of the Day!",
            """Remember that you're free to ask about the thing of the day! (But do it
            in the right channel.)""",
            """Want to know more about this thing of the day? Ask in the questions
            channel!""",
            "Found a mistake? Tell brzrkr#4592.",
            "These entries are submitted manually. Feel free to suggest more!",
            "Not sure what it means or how to use it? Feel free to ask!",
            """It's good practice to try and make up some sentences of your own with
            the word of the day!""",
        },
        "totd add entry instructions": {
            "Write `totd add entry LANGUAGE TYPE|ENTRY|DESCRIPTION`. Like:\n"
            "> `totd add entry Weebanese interjection|uguuu|an expression of kawaiiness`"
            "\nYou can add multiple entries at a time on multiple lines.",
        },
        "hello": {
            "Hi there! You can @ me and say `help` to learn what I can do.",
            "If that was a command, I didn't get it. Try `help` first.",
        },
        "not ready": {
            "It's not time yet!",
            "I'm not ready for this. >.<",
            "Give me a moment will you?",
            "Wait a sec and retry, please.",
            "Retry in a moment.",
        },
        "muted": {
            "You have been muted from {}. Behave and the mute might be lifted. :)",
        },
        "appreciation": {  # Accepted "thank you" messages by users. Case doesn't matter
            "Thanks",
            "thx",
            "ty",
            "wow",
            "cool",
            "Good boy",
            "Good job",
            "Nice job",
            "Nice one",
            "Thank you",
            "Amazing",
            "you're amazing",
            "Nice",
            "noice",
            "marry me",
            "love it",
            "I love you",
            "love you",
            "great",
            "you're great",
            "brilliant",
            "you're brilliant",
        },
        "respond to praise": {
            "no u <3",
            "Pog",
            "Aww... :>",
            "Aw, stop it, you. ☺️",
            "You're welcome. ;>",
            "Cheers.",
            "Cheers, luv. ;)",
            *"🤗💗💓🥰😋😎😊",
            "❤️",
            "☺️",
        },
        "did you mean": {
            "Did you mean {}?",
            "Were you looking for {}?",
            "Are you looking for {}?",
            "Maybe retry with {} if that's what you're looking for.",
        },
        "might take time": {"It might take a minute to take effect."},
        "role added": {"added"},
        "role removed": {"removed"},
        "role action": {"I {} the role. 👍"},
        "invalid entry": {"That's not valid. Avoid any weird character."},
    },
    "ita": {
        "appreciation": {
            "grazie",
            "grz",
            "graz",
            "grazie mille",
            "mille grazie",
            "bella",
            "figo",
            "fico",
            "che figo",
            "che fico",
            "figata",
            "ficata",
            "che figata",
            "che ficata",
            "adoro",
        },
    },
    "scn": {
        "appreciation": {"grazi", "grazii", "grazî", "grazzi", "grazzii", "grazzî"},
    },
}

EASTER_EGGS: dict[str, set[str]] = {
    "the meaning of life": {"Maybe it's to pat me!", "42", "As if I'd tell *you*."},
    "yourself": {
        "I... I don't know what to say...",
        "T_T",
        "Gomennasai t.t",
        "Please don't hurt me. :<",
    },
    "life": {"You should ask your mum."},
    "the answer to the ultimate question of life, the universe and everything": {"42."},
    "italian": {"That's a bit too broad, don't you think? :P"},
    "how are you": {"Fine I guess.", "How are *you*? ;)"},
    "spam a wall of text": {
        """What the fuck did you just fucking say about me, you little bitch? I'll have
        you know I graduated top of my class in the Navy Seals, and I've been involved
        in numerous secret raids on Al-Quaeda, and I have over 300 confirmed kills. I
        am trained in gorilla warfare and I'm the top sniper in the entire US armed
        forces. You are nothing to me but just another target. I will wipe you the fuck
        out with precision the likes of which has never been seen before on this Earth,
        mark my fucking words. You think you can get away with saying that shit to me
        over the Internet? Think again, fucker. As we speak I am contacting my secret
        network of spies across the USA and your IP is being traced right now so you
        better prepare for the storm, maggot. The storm that wipes out the pathetic
        little thing you call your life. You're fucking dead, kid. I can be anywhere,
        anytime, and I can kill you in over seven hundred ways, and that's just with my
        bare hands. Not only am I extensively trained in unarmed combat, but I have
        access to the entire arsenal of the United States Marine Corps and I will use
        it to its full extent to wipe your miserable ass off the face of the continent,
        you little shit. If only you could have known what unholy retribution your
        little \"clever\" comment was about to bring down upon you, maybe you would
        have held your fucking tongue. But you couldn't, you didn't, and now you're
        paying the price, you goddamn idiot. I will shit fury all over you and you will
        drown in it. You're fucking dead, kiddo.""",
    },
}
"""Easter eggs users can send as FAQ names (which they usually do to mess with the bot)
mapped to possible answers to them.
"""

EASTER_EGGS["sicilian"] = EASTER_EGGS["italian"]
EASTER_EGGS["english"] = EASTER_EGGS["italian"]
EASTER_EGGS["how are you?"] = EASTER_EGGS["how are you"]


def fetch_line(code: str, *args: object) -> str:
    """Fetch a random line for the given code. If applicable, args are substituted into
    the line.

    If a code is not found, it is returned.
    """
    lines = tuple(LOCALES[LANGUAGE].get(code, code))

    if not lines:
        logger.warning("Code %s in language %s has no entries.", code, LANGUAGE)
        lines = ("",)

    line = random.choice(lines)

    if line == code:
        logger.warning("Code %s not found in language %s.", code, LANGUAGE)

    for arg in args:
        line = line.replace("{}", str(arg), 1)

    return line


def fetch_set(code: str) -> set[str]:
    """Fetch the set of possible lines for the given code. No argument substitution is
    made.
    """
    # When fetching strings intended to be read and not sent, join all languages
    if code in ("appreciation", "easter eggs"):
        all_values: set[str] = set()
        for locale in LOCALES.values():
            all_values.union(locale[code])

        return {s.lower() for s in all_values}

    return LOCALES[LANGUAGE][code].copy()


def fetch_user_message(code: str, *args: object) -> str:
    """Fetch a line meant to be read by a user: includes a notice about the message
    being automatic at the end.
    """
    return (
        f"{fetch_line(code, *args)}\n\n"
        f"i️ This is an automated message: replying to it will have no effect."
    )


def fetch_easter_egg(key: str) -> str:
    """Fetch an easter egg line."""
    try:
        response = random.choice(tuple(EASTER_EGGS[key]))
    except KeyError:
        logger.warning("Key '%s' not found in easter egg responses.", key)
        response = "Heh."

    return response
