"""Logging configuration."""

import logging
import sys

from src.config import DEBUG

FORMATTER = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s")
DISCORD_FORMATTER = logging.Formatter(
    "DISCORDPY: %(asctime)s:%(levelname)s:%(name)s: %(message)s"
)

file_handler = logging.FileHandler(
    filename="../discord.log",
    encoding="utf-8",
    mode="a",
)
file_handler.setFormatter(FORMATTER)
file_handler.setLevel(logging.DEBUG if DEBUG else logging.INFO)

console_handler = logging.StreamHandler(stream=sys.stdout)
console_handler.setFormatter(FORMATTER)
console_handler.setLevel(logging.DEBUG if DEBUG else logging.WARNING)

stderr_handler = logging.StreamHandler(stream=sys.stderr)
stderr_handler.setFormatter(FORMATTER)
stderr_handler.setLevel(logging.ERROR)

discord_handler = logging.StreamHandler(stream=sys.stderr)
discord_handler.setFormatter(DISCORD_FORMATTER)
discord_handler.setLevel(logging.INFO)


def prepare_logger(logger_name: str) -> logging.Logger:
    """Configure a logger with the given name with the default handlers."""
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG if DEBUG else logging.INFO)
    logger.addHandler(console_handler)
    logger.addHandler(stderr_handler)
    logger.addHandler(file_handler)

    return logger
