"""Custom Context class for Berzibot."""

import asyncio
from collections.abc import AsyncIterator, Generator
from contextlib import asynccontextmanager
from types import TracebackType
from typing import (
    TYPE_CHECKING,
    Any,
    NotRequired,
    TypedDict,
    Unpack,
    cast,
)

from asyncpg.pool import Pool, PoolConnectionProxy
from discord import Interaction, Message
from discord.ext import commands
from discord.ext.commands import Command, Parameter
from discord.ext.commands.view import StringView

from src.components.localisation import fetch_set

if TYPE_CHECKING:
    from src.berzibot import Berzibot


class _ContextDBAcquire:
    __slots__ = ("ctx", "timeout")

    def __init__(self, ctx: "BerziContext", timeout: int | None) -> None:
        self.ctx = ctx
        self.timeout = timeout

    def __await__(self) -> Generator[None, None, PoolConnectionProxy]:
        return self.ctx._acquire(self.timeout).__await__()  # noqa: SLF001

    async def __aenter__(self) -> PoolConnectionProxy:
        return await self.ctx._acquire(self.timeout)  # noqa: SLF001

    async def __aexit__(
        self,
        exc_type: type[BaseException] | None,
        exc_val: BaseException | None,
        exc_tb: TracebackType | None,
    ) -> None:
        await self.ctx.release()


class _BerziContextKwargs(TypedDict):
    message: Message
    bot: "Berzibot"
    view: StringView
    args: NotRequired[list]
    kwargs: NotRequired[dict[str, Any]]
    prefix: NotRequired[str | None]
    command: NotRequired[Command[Any, ..., Any] | None]
    invoked_with: NotRequired[str | None]
    invoked_parents: NotRequired[list[str]]
    invoked_subcommand: NotRequired[Command[Any, ..., Any] | None]
    subcommand_passed: NotRequired[str | None]
    command_failed: NotRequired[bool]
    current_parameter: NotRequired[Parameter | None]
    current_argument: NotRequired[str | None]
    interaction: NotRequired[Interaction | None]


class BerziContext(commands.Context):
    """Context type specific to Berzibot."""

    DEFAULT_ASK_TIMEOUT = 45

    def __init__(self, **kwargs: Unpack[_BerziContextKwargs]) -> None:
        super().__init__(**kwargs)
        self.pool: Pool = self.bot.pool
        self.db: PoolConnectionProxy | None = None

    async def _acquire(self, timeout: int | None) -> PoolConnectionProxy:
        if self.db is None:
            self.db = await self.pool.acquire(timeout=timeout)

        if not self.db:
            message = "Cannot acquire pool."
            raise RuntimeError(message)

        return self.db

    def acquire(self, *, timeout: int | None = None) -> _ContextDBAcquire:
        """Acquires a database connection from the pool. e.g. ::
            async with ctx.acquire():
                await ctx.db.execute(...)
        or: ::
            await ctx.acquire()
            try:
                await ctx.db.execute(...)
            finally:
                await ctx.release().
        """
        return _ContextDBAcquire(self, timeout)

    async def release(self) -> None:
        """Releases the database connection from the pool.
        Useful if needed for "long" interactive commands where
        we want to release the connection and re-acquire later.
        Otherwise, this is called automatically by the bot.
        """
        if self.db is not None:
            await self.bot.pool.release(self.db)
            self.db = None

    async def _ask(
        self,
        message: str,
        *,
        timeout: float,
    ) -> Message:
        await self.send(message)

        return cast(
            Message,
            await self.bot.wait_for(
                "message",
                check=lambda m: m.channel == self.channel and m.author == self.author,
                timeout=timeout,
            ),
        )

    @asynccontextmanager
    async def ask_bool(
        self,
        message: str,
        *,
        timeout: float = DEFAULT_ASK_TIMEOUT,
    ) -> AsyncIterator[bool]:
        """Ask the author of the invocation for a boolean response."""
        try:
            answer = (await self._ask(message, timeout=timeout)).content
        except asyncio.exceptions.TimeoutError:
            yield False
            return

        yield answer.strip().lower() in fetch_set("positive response")

    @asynccontextmanager
    async def ask_str(
        self,
        message: str,
        *,
        timeout: float = DEFAULT_ASK_TIMEOUT,
        preserve_case: bool = False,
    ) -> AsyncIterator[str]:
        """Ask the author of the invocation for a string response."""
        answer = (await self._ask(message, timeout=timeout)).content

        if not preserve_case:
            answer = answer.lower()

        yield answer.strip()
