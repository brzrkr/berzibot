ALTER TABLE uc_mutes DROP COLUMN IF EXISTS unmute_at;
ALTER TABLE uc_mutes ADD COLUMN IF NOT EXISTS unmute_at timestamp with time zone NOT NULL DEFAULT now();
