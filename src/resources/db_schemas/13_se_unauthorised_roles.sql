CREATE TABLE IF NOT EXISTS public.se_original_perms (
    server bigint NOT NULL,
    role bigint NOT NULL,
    send_messages bool NULL DEFAULT null,
    connect bool NULL DEFAULT null,
    PRIMARY KEY (server, role)
);
ALTER TABLE IF EXISTS public.se_original_perms OWNER TO berzibot;
COMMENT ON TABLE public.se_original_perms IS 'Permanent cache of original role '
    'permissions before locking a channel. Updated when locking a channel due to a '
    'scheduled event action and used to determine whether unlocking the channel should '
    'enable the given permission.';
