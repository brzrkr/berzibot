CREATE TABLE IF NOT EXISTS public.voice_trigger_channels (
    id integer PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY,
    guild_id bigint NOT NULL,
    category_id bigint NOT NULL,
    trigger_channel_name text NOT NULL,
    new_channel_name text NOT NULL,
    channel_size smallint NOT NULL
);
