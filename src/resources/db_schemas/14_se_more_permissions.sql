ALTER TABLE IF EXISTS public.se_original_perms
    ADD COLUMN create_public_threads bool NULL DEFAULT null;
ALTER TABLE IF EXISTS public.se_original_perms
    ADD COLUMN create_private_threads bool NULL DEFAULT null;
ALTER TABLE IF EXISTS public.se_original_perms
    ADD COLUMN send_messages_in_threads bool NULL DEFAULT null;
ALTER TABLE IF EXISTS public.se_original_perms
    ADD COLUMN send_tts_messages bool NULL DEFAULT null;
ALTER TABLE IF EXISTS public.se_original_perms
    ADD COLUMN send_voice_messages bool NULL DEFAULT null;
