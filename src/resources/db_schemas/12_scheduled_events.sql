CREATE TABLE IF NOT EXISTS public.scheduled_events (
    id INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY,
    server BIGINT NOT NULL,
    actions TEXT NULL,
    started BOOLEAN NOT NULL DEFAULT false
);
ALTER TABLE IF EXISTS public.scheduled_events OWNER TO berzibot;
COMMENT ON COLUMN public.scheduled_events.actions
    IS 'Strings of actions to execute when the event starts and reverse when it ends, separated by newlines. '
    'Arguments are separated by pipes (no quotes for text, white space is stripped).'
    'Currently supported:\n'
    'close_text_channel(CHANNEL_ID)\n'
    'close_voice_channel(CHANNEL_ID)\n'
    'send_message(CHANNEL_ID|CONTENT)';
