# Berzibot

## Configuration
### Setup
- Setup the project with `pdm`.
- Run `pdm run migrations`.
- Set the environment (see below).
- Run `pdm run start`.

### Requirements
- Python 3.12
- Postgres 16

### Enviroment
| Name                  | Description                                                                        | Example                                   | Default      |
|-----------------------|------------------------------------------------------------------------------------|-------------------------------------------|--------------|
| `BERZIBOT_TOKEN`      | Discord token to start the bot.                                                    |                                           | REQUIRED     |
| `BERZIBOT_DB_STRING`  | Full database connection string to the database.                                   | `postgresql://user:password@host:port/db` | REQUIRED     |
| `BERZIBOT_DEBUG`      | Whether the bot should start in debug mode, limiting the ways it can be activated. | `1` or any truthy value.                  | `False`      |
| `BERZIBOT_TEST_GUILD` | Test guild to use in debug mode. Must be set for the bot to work in debug mode.    | `21313213131313131`                       | Not set      |
| `BERZIBOT_SENTRY_DSN` | DSN for Sentry. If not given, Sentry will not be initialised.                      | `https://hsdajf.ingest.sentry.io/464564`  | Not set      |
| `BERZIBOT_ENV`        | Name of the current environment.                                                   | `production`                              | `production` |
