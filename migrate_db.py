"""Prepare the database running all available migations.

Migrations are .sql files in the resources/db_schemas directory.
Migration names must start with a number, then an underscore, then its name.
Migrations are executed in order of their number; if multiple migrations have the same
 number, relative order is undefined.
Migrations which have already been applied based on their name will not be applied
 again. Do not change a migration's name once applied.
"""

# print() should be allowed since this is a script.
# ruff: noqa: T201

import asyncio
from dataclasses import dataclass
from pathlib import Path
from typing import Final

from asyncpg import Connection, UndefinedTableError, connect

from src.config import DB_STRING

REGISTER_MIGRATION_NAME: Final[str] = "register"
MIGRATION_DIR: Final[Path] = Path("src/resources/db_schemas")
if not (MIGRATION_DIR.exists() and MIGRATION_DIR.is_dir()):
    error = f"Directory {MIGRATION_DIR} is not valid."
    raise ValueError(error)


@dataclass
class Migration:  # noqa: D101
    number: int
    name: str
    path: Path


async def run_migration(  # noqa: D103
    connection: Connection,
    migration: Migration,
) -> None:
    try:
        exists = await connection.fetch(
            "SELECT * FROM migrations_register WHERE name = $1;",
            migration.name,
        )
    except UndefinedTableError:
        exists = []

    if exists:
        print(f'Migration "{migration.name}" is already installed.')
        return

    migration_code = migration.path.read_text(encoding="utf-8")

    await connection.execute(migration_code)

    await connection.execute(
        "INSERT INTO migrations_register (name) VALUES ($1);",
        migration.name,
    )
    print(f'Installed migration "{migration.name}".')


def _migration_sort_key(m: Migration) -> int:
    return m.number


async def find_and_run_migrations() -> int:
    """Run all migrations available in the migrations directory."""
    conn = await connect(DB_STRING)

    if not (MIGRATION_DIR.exists() and MIGRATION_DIR.is_dir()):
        message = f"The migrations directory {MIGRATION_DIR} must exist."
        raise ValueError(message)

    migrations: list[Migration] = []
    for file in MIGRATION_DIR.iterdir():
        if file.is_file() and file.name.endswith(".sql"):
            num_str, _, name = file.name.removesuffix(".sql").partition("_")

            if not name:
                message = f"Migration file {file} must have a valid name."
                raise ValueError(message)

            try:
                order = int(num_str)
            except ValueError:
                message = f"Migration {file} must have a valid integer numeral."
                raise ValueError(message) from None

            migrations.append(Migration(order, name.lower(), file))

    migrations.sort(key=_migration_sort_key)

    for migration in migrations:
        try:
            await run_migration(conn, migration)
        except Exception as e:  # noqa: BLE001
            print(e)
            return 1

    return 0


def main() -> int:  # noqa: D103
    return asyncio.run(find_and_run_migrations())


if __name__ == "__main__":
    exit(code=main())
